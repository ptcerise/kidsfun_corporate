$(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

/* swap open/close side menu icons */
$('[data-toggle=collapse]').click(function(){
  	// toggle icon
  	$(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
});
});

/* FOR SMOOTH SCROLL TO TAG WITH #main */
$(function() {
  $('a[href*="#main"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 750);
        return false;
      }
    }
  });
});

/* FOOTER */
var h = 0;
$('.footer-border').each(function() {
    if ($(this).height()>h) {
        h= $(this).height();
    };
});
$('.footer-border').each(function() {
    $(this).css('height', h+'px');
});


/* auto-hiding navbar */
$(".navbar-fixed-top").autoHidingNavbar();

/* SHOWING DETAIL INFO ON CARDS */
function view_detail($id){
    $($id).removeClass("animated fadeOutDown");
    $($id).addClass("animated fadeInUp");
    $($id).css("visibility", "visible");
}

function close_detail($id){
    $($id).addClass("animated fadeOutDown");
}

// FOR GALLERY
$(document).ready(function(){
    $("#gallery").unitegallery({
        gallery_theme: "tiles"
    });
});

/* CLOSE BUTTON ON SALES PRODUCT DETAIL */
$(document).ready(function(){
    var card_height = $(".card-detail").height();
    $(".product-spec").css("height", card_height * 0.7788 +"px");
});

// SELECT TOWN IN OUTLETS & MERCHANTS
$(document).ready(function(){
    $('#reset').hide();
    $("#select-city").change(function() {
        
        var selected    = $('select :selected').val();

        $('#reset').show();
        $("#showmore").hide();
        $('article').siblings().hide();
        $('article.'+selected).show();
        // $('article').attr('class', notSelected).hide();
    });
    $("#reset").click(function(){
        location.reload();
    });
});

// SELECT TOWN IN PARTNERS
$(document).ready(function(){
    $('#reset-travel').hide();
    $('#reset-hotel').hide();
    $("#select-travel").change(function() {

        var selected    = $('select#select-travel :selected').val();
        
        $('#reset-travel').show();
        $("#showmore").hide();
        $('article.travel').siblings().hide();
        $('article.travel-'+selected).show();

    });
    $("#select-hotel").change(function() {

        var selected    = $('select#select-hotel :selected').val();
        
        $('#reset-hotel').show();
        $("#showmore2").hide();
        $('article.hotel').siblings().hide();
        $('article.hotel-'+selected).show();
    });
    $("#reset-travel").click(function(){
        location.reload();
    });
    $("#reset-hotel").click(function(){
        location.reload();
    });
});

// SHOW MORE BUTTON
$(document).ready(function(){

    var cards_count = $('div.cards article').size();
    var c = 3;
    var add = 5;

    $("div.cards article:gt("+ c +")").hide();
    $("div.cards article:lt("+ c +")").show();

    var show_cards = $("div.cards article:lt("+ c +")").size();
    var q = show_cards+1;

    // SHOW MORE
    if(q == cards_count){
        $("#showmore").hide();
    }else if(show_cards >= cards_count){
        $("#showmore").hide();
    }else{
        $("#showmore").show();
    }

    $("#showmore").click(function(){
        c = (c + add <= cards_count) ? c + add : cards_count;
        $("div.cards article:gt("+ c +")").hide();
        $("div.cards article:lt("+ c +")").show();
        
        if(add == 5){add = 4};

        show_cards = $("div.cards article:lt("+ c +")").size();
        if(show_cards >= cards_count){
            $("#showmore").hide();
        }else{
            $("#showmore").show();
        }
    }); 
});

$(document).ready(function(){
    var cards_count = $('div.cards2 article').size();
    var c = 3;
    var add = 5;

    $("div.cards2 article:gt("+ c +")").hide();
    $("div.cards2 article:lt("+ c +")").show();

    var show_cards = $("div.cards2 article:lt("+ c +")").size();
    var q = show_cards+1;

    // SHOW MORE 2
    if(q == cards_count){
        $("#showmore2").hide();
    }else if(show_cards >= cards_count){
        $("#showmore2").hide();
    }else{
        $("#showmore2").show();
    }

    $("#showmore2").click(function(){
        c = (c + add <= cards_count) ? c + add : cards_count;
        $("div.cards2 article:gt("+ c +")").hide();
        $("div.cards2 article:lt("+ c +")").show();
        
        if(add == 5){add = 4};

        show_cards = $("div.cards2 article:lt("+ c +")").size();
        if(show_cards >= cards_count){
            $("#showmore2").hide();
        }else{
            $("#showmore2").show();
        }
    });
});


/* INPUT ANIMATION */
$(window).load(function(){

  $(".input-effect input").focusout(function(){
    if($(this).val() != ""){
      $(this).addClass("has-content");
    }else{
    $(this).removeClass("has-content");
    }
  });

});
$(document).ready(function(){
    $(".col-effect textarea").val("");
    $(".input-effect textarea").focusout(function(){
      if($(this).val() != ""){
        $(this).addClass("has-content");
      }else{
      $(this).removeClass("has-content");
      }
    });

    $(".input-effect select").focusout(function(){
      if($(this).val() == "No category"){
        $(this).removeClass("has-content");
      }else{
      $(this).addClass("has-content");
      }
    });
});