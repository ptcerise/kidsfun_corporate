<section class="container">
	<div class="row">
		<header class="col-xs-12 main-header">
			<h1><?php echo $this->lang->line('nav_career'); ?></h1>
			<p class="lead description">
				<?php if($lang == "english" || $lang == ""){
                    echo !empty($career_content->general_content_en) ? $career_content->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($career_content->general_content_in) ? $career_content->general_content_in : "[ KOSONG! ]";
                } ?>
			</p>
		</header>
	</div>

	<div class="row cards">
		<?php foreach($job_list as $key => $row){ ?>
			<article class="col-xs-12 col-sm-6">
				<div class="panel card">
					<div class="panel-body">
						<header>
							<h2>
								<?php if($lang == "english" || $lang == ""){
				                    echo !empty($row->career_title_en) ? $row->career_title_en : "[ EMPTY! ]";
				                }else{
				                    echo !empty($row->career_title_in) ? $row->career_title_in : "[ KOSONG! ]";
				                } ?>
							</h2>
						</header>
						<div class="fixed-content-big">
						<p>
							<?php if($lang == "english" || $lang == ""){
			                    echo !empty($row->career_desc_en) ? $row->career_desc_en : "[ EMPTY! ]";
			                }else{
			                    echo !empty($row->career_desc_in) ? $row->career_desc_in : "[ KOSONG! ]";
			                } ?>
						</p>
						</div>
					</div>
				</div>
			</article>
		<?php } ?>

		<!-- SHOW MORE BUTTON -->
		<aside class="col-xs-12 text-center btn-show-more">
			<button id="showmore" class="btn btn-default btn-raised"><?php echo $this->lang->line("btn_show_more"); ?></button>
		</aside>
	</div>
</section>