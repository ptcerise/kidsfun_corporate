<section class="container-fluid bg-white heading">
	<div class="container">

		<div class="row">
			<header class="col-xs-12 main-header">
				<h1><?php echo $this->lang->line('nav_outlets'); ?></h1>
				<p class="lead description">
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($outlets_content->general_title_en) ? $outlets_content->general_title_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($outlets_content->general_title_in) ? $outlets_content->general_title_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</header>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<p>
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($outlets_content->general_content_en) ? $outlets_content->general_content_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($outlets_content->general_content_in) ? $outlets_content->general_content_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</div>

			<figure class="col-xs-12 col-sm-6 text-center">
				<img alt="kidsfun outlets" class="img-responsive" src="<?php echo !empty($outlets_image) ? base_url('assets/upload/outlets').'/'.$outlets_image : base_url('assets/img/card-image-rect.jpg'); ?>">
			</figure>
		</div>

	</div>
</section>

<section class="container">
	<aside class="row dropdown-no-header">
		<div class="col-xs-12 col-sm-3">
			<h3><?php echo $this->lang->line('find_one'); ?></h3>
			<form>
				<select id="select-city">
					<option value="" selected="" disabled=""><?php echo $this->lang->line('select_town'); ?></option>
					<?php foreach($outlets_city as $city): ?>
					<option value="<?php echo $city->outlets_city; ?>"><?php echo $city->outlets_city; ?></option>
					<?php endforeach ?>
				</select>
			</form>
			<div class="row text-center">
				<button class="btn btn-xs btn-raised btn-reset" id="reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
			</div>
		</div>
	</aside>
	
	<div class="row cards">
	<?php foreach($outlets_centers as $center): ?>	
		<article class="col-xs-12 col-sm-6 col-md-3 outlet <?php echo $center->outlets_city; ?>">
			<div class="panel card">
				<figure class="img-cards">
					<img alt="kidsfun outlet centers" class="img-responsive" src="<?php echo !empty($center->outlets_image) ? base_url('assets/upload/outlets/content').'/'.$center->outlets_image : base_url('assets/img/card-image-srect.jpg'); ?>">
				</figure>
				<div class="panel-body">
					<div class="fixed-content-small">
						<header>
							<h3>
								<?php echo $center->outlets_name; ?>							
							</h3>
						</header>
						
						<div class="address">
							<!-- PHONE -->
							<div class="row">
								<span class="col-xs-1 icon">
									<i class="fa fa-phone" aria-hidden="true"></i>
								</span>
								<span class="col-xs-11 content">
									
									<?php
	                                    // echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
	                                    $x = str_replace(array('<p>','</p>'), '',substr($center->outlets_phone, 1));
	                                    $phone = explode('-',$x);
	                                    $phone_number = $phone[0];
	                                    foreach($phone as $key=>$val){
	                                        if(count($phone) == $key+1){break;}
	                                        $phone_number = $phone_number.$phone[$key+1];
	                                    }
	                                    $link = "tel:+62".$phone_number;
	                                ?>
                                    <a href="<?php echo $link; ?>">
                                        <?php
                                            echo "0".$phone[0]." ";
                                            foreach($phone as $key=>$val){
                                                if(count($phone) == $key+1){break;}
                                                echo $phone[$key+1]." ";
                                            }
                                        ?>
                                    </a>
								</span>
							</div>
							<!-- ADDRESS -->
							<div class="row">
								<span class="col-xs-1 icon">
									<i class="fa fa-map-marker" aria-hidden="true"></i>
								</span>
								<span class="col-xs-11 content">
									<?php echo $center->outlets_address."<br>".$center->outlets_city; ?>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	<?php endforeach; ?>
	</div>
	<!-- SHOW MORE BUTTON -->
	<aside class="row text-center btn-show-more">
		<button id="showmore" class="btn btn-default btn-raised"><?php echo strtoupper($this->lang->line('btn_show_more')); ?></button>
	</aside>
</section>

<section class="bg-white">
	<div class="container">
		<div class="row">
			<header class="col-xs-12 on-section">
				<h2><?php echo $this->lang->line('gallery');?></h2>
			</header>
		</div>
		
		<div id="gallery">
			<?php foreach ($gallery as $key => $row) { ?>
	  		<img alt="<?php echo $row->media_url; ?>" src="<?php echo base_url('assets/upload/gallery/'.$row->media_url); ?>"
	  		data-image="<?php echo base_url('assets/upload/gallery/'.$row->media_url); ?>"
            data-description="<?php 
                if($lang == "english" || $lang == ""){
                    echo $row->media_content_en;
                }else{
                    echo $row->media_content_in;
                }
            ?>">
	  		<?php }; ?>
		</div>
	</div>	
</section>