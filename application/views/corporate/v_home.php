<section class="container heading">
	<!-- INTRO -->
	<div class="row">
		<header class="col-xs-12 main-header">
			<h1>
				<?php if($lang == "english" || $lang == ""){
                    echo !empty($home_content->general_title_en) ? $home_content->general_title_en : "[ EMPTY! ]";
                }else{
                    echo !empty($home_content->general_title_in) ? $home_content->general_title_in : "[ KOSONG! ]";
                } ?>
			</h1>
			<p class="lead description">
				<?php if($lang == "english" || $lang == ""){
                    echo !empty($home_content->general_content_en) ? $home_content->general_content_en : "[ EMPTY! ]";
                }else{
                    echo !empty($home_content->general_content_in) ? $home_content->general_content_in : "[ KOSONG! ]";
                } ?>
			</p>
		</header>
	</div>

	<!-- CARDS -->
	<nav class="row">
		<?php foreach ($menu as $cards): ?>
		
		<div class="col-xs-12 col-sm-6 col-md-3">
		<?php if($cards->media_title_en=='Tickets') { ?>
			<a class="link-card" href="https://kidsfun.co.id/tickets">
		<?php } else { ?>
			<a class="link-card" href="<?php echo base_url().strtolower(str_replace(' ','_',$cards->media_title_en)); ?>">
		<?php } ?>
				<div class="panel card">
					<figure class="img-cards">
						<img alt="kidsfun <?php echo strtolower($cards->media_title_en); ?>" class="img-responsive" src="<?php echo !empty($cards->media_url) ? base_url('assets/upload/home').'/'.$cards->media_url : base_url('assets/img/card-image.jpg'); ?>">
					</figure>
					<div class="panel-body text-center">
						<header>
							<h4>
								<?php if($lang == "english" || $lang == ""){
				                    echo strtoupper($cards->media_title_en);
				                }else{
				                    echo strtoupper($cards->media_title_in);
				                } ?>
							</h4>
						</header>
					</div>
				</div>
			</a>
		</div>

		<?php endforeach ?>
	</nav>
</section>

<!-- ABOUT US -->
<div class="bg-white heading">
	<div class="container">
		<article class="row">
			<header class="col-xs-12">
				<h2><?php echo $this->lang->line('about_us'); ?></h2>
			</header>
			
			<div class="col-xs-12 col-sm-6">
				<p>
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($about_us_content->general_content_en) ? $about_us_content->general_content_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($about_us_content->general_content_in) ? $about_us_content->general_content_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</div>

			<figure class="col-xs-12 col-sm-6">
				<img alt="about kidsfun" class="img-responsive" src="<?php echo !empty($about_us_image) ? base_url('assets/upload/home').'/'.$about_us_image : base_url('assets/img/card-image-rect.jpg'); ?>">
			</figure>
		</article>
	</div>
</div>
