<section class="container-fluid bg-white heading">
    <div class="container">
        <div class="row">
            <header class=" col-xs-12 main-header">
                <h1><?php echo $this->lang->line('nav_contact'); ?></h1>
            
                <p class="lead description">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($contact_content->general_content_en) ? $contact_content->general_content_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($contact_content->general_content_in) ? $contact_content->general_content_in : "[ KOSONG! ]";
                    } ?>
                </p>
            </header>
        </div>
    </div>
</section>

<!-- THE CONTENT -->
<section class="container">
    <div class="row" id="form">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 padding-clear">
            <!-- CONTACT FORM -->
            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="panel panel-default padding-clear card">
                
                <!-- THE FORM -->
                <form method="POST" id="contact_form" name="contact_form" action="<?php echo base_url('contact_us/send_message');?>">
                    <?php echo $this->session->flashdata('info_send'); ?>
                    <div class="panel-body">

                        <div class="col-effect input-effect">
                            <input class="effect-label" placeholder="" type="text" id="contact_name" name="Name" required>
                            <label><?php echo $this->lang->line("contact_name"); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <input class="effect-label" placeholder="" value="" type="email" name="Email" required>
                            <label><?php echo $this->lang->line("contact_email"); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <input class="effect-label" placeholder="" value="" type="text" name="Phone" required>
                            <label><?php echo $this->lang->line("contact_phone"); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <input class="effect-label" placeholder="" value="" type="text" name="Subject" required>
                            <label><?php echo $this->lang->line("contact_subject"); ?></label>
                            <span class="focus-border"></span>
                        </div>

                        <div class="col-effect input-effect">
                            <textarea class="effect-label" placeholder="" rows="4" name="Message" required></textarea>
                            <label><?php echo $this->lang->line("contact_message"); ?></label>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-sm-offset-6">
                            <input class="btn btn-bg-red btn-raised" id="button_send" name="button" type="submit" value="<?php echo $this->lang->line("btn_send"); ?>">
                        </div>
                    </div>
                </form>
                </div>
            </div>
            
            <!-- CONTACT INFO -->
            <div class="col-xs-12 col-sm-6 padding-clear">
                <div class="row">
                    <div class="col-xs-12 padding-clear">
                        <div class="panel panel-default card contact-card">
                            <div class="panel-body text-center">
                                <?php
                                    // echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
                                    $x = str_replace(array('<p>','</p>'), '',substr($phone->general_content_en, 1));
                                    $phone = explode('-',$x);
                                    $phone_number = $phone[0];
                                    foreach($phone as $key=>$val){
                                        if(count($phone) == $key+1){break;}
                                        $phone_number = $phone_number.$phone[$key+1];
                                    }
                                    $link = "tel:+62".$phone_number;
                                ?>
                                    <a href="<?php echo $link; ?>">
                                        <?php
                                            echo "(0".$phone[0].") ";
                                            foreach($phone as $key=>$val){
                                                if(count($phone) == $key+1){break;}
                                                echo $phone[$key+1]." ";
                                            }
                                        ?>
                                    </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 padding-clear">
                        <div class="panel panel-default card contact-card">
                            <div class="panel-body text-center">
                                <?php echo !empty($address->general_content_en) ? $address->general_content_en:"[EMPTY!]"; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>