<section class="container-fluid bg-white heading">
	<div class="container">

		<div class="row">
			<header class="col-xs-12 main-header">
				<h1>
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($general_content->general_title_en) ? $general_content->general_title_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($general_content->general_title_in) ? $general_content->general_title_in : "[ KOSONG! ]";
	                } ?>
				</h1>
				<p class="lead">
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($general_content->general_content_en) ? $general_content->general_content_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($general_content->general_content_in) ? $general_content->general_content_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</header>
		</div>

		<div class="row">
			<header class="col-xs-12">
				<h2><?php echo $this->lang->line('nav_sales'); ?></h2>
				<p class="lead">
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($sales_content->general_title_en) ? $sales_content->general_title_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($sales_content->general_title_in) ? $sales_content->general_title_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</header>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<p>
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($sales_content->general_content_en) ? $sales_content->general_content_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($sales_content->general_content_in) ? $sales_content->general_content_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</div>

			<figure class="col-xs-12 col-sm-6 text-center">
				<img alt="Kidsfun Sales" class="img-responsive" src="<?php echo !empty($sales_img->media_url) ? base_url('assets/upload/sales').'/'.$sales_img->media_url : base_url('assets/img/card-image-rect.jpg'); ?>">
			</figure>
		</div>

	</div>
</section>

<section class="container">
	<div class="row">
		<header class="col-xs-12 col-sm-3 on-section">
			<h3><?php echo $this->lang->line('our_products'); ?></h3>
		</header>
	</div>
	
	<div class="row cards">
		<?php foreach($products_list as $key => $row) { ?>
		<article class="col-xs-12 col-sm-6 col-md-3">
			<div class="panel card">
				<figure class="img-cards">
					<img alt="Kidsfun Products" class="img-responsive" src="<?php echo !empty($row->products_image) ? base_url('assets/upload/sales/content').'/'.$row->products_image : base_url('assets/img/card-image.jpg'); ?>">
				</figure>
				<div class="panel-body">
					<div class="fixed-content-small">
						<header>
							<h4><?php echo $row->products_name; ?></h4>
						</header>
						<p class="lead">Rp. <?php echo number_format($row->products_price,0,',','.'); ?></p>
	
						<div class="panel-body btn-panel pull-right">
							<a class="card-btn" onclick="view_detail('#product-<?php echo $row->products_id;?>');"><?php echo strtoupper($this->lang->line('btn_learn_more')); ?></a>
						</div>
					</div>
				</div>

				<!-- DETAILED INFORMATION -->
				<div class="card-detail" id="product-<?php echo $row->products_id;?>">
					<div class="panel-body">
						<header>
							<h4><?php echo $this->lang->line('description'); ?></h4>
						</header>
						<div class="product-spec">
							<p><strong><?php echo $this->lang->line('dimension'); ?></strong><br>
							<?php echo $row->products_dimension; ?></p>

							<p><strong><?php echo $this->lang->line('power'); ?></strong><br>
							<?php echo $row->products_power; ?></p>

							<p><strong><?php echo $this->lang->line('delivery'); ?></strong><br>
							<?php echo $row->products_delivery_time; ?></p>
							
							<p><strong><?php echo $this->lang->line('price'); ?></strong><br>
							Rp. <?php echo number_format($row->products_price,0,',','.'); ?></p>
						</div>
						<div class="panel-body btn-panel pull-right">
							<a class="card-btn" onclick="close_detail('#product-<?php echo $row->products_id;?>');"><?php echo strtoupper($this->lang->line('btn_close')); ?></a>
						</div>
					</div>
				</div>
			</div>
		</article>
		<?php } ?>
	</div>
	<!-- SHOW MORE BUTTON -->
	<aside class="col-xs-12 text-center btn-show-more">
		<button id="showmore" class="btn btn-default btn-raised"><?php echo $this->lang->line('btn_show_more'); ?></button>
	</aside>
</section>

<section class="container-fluid bg-white heading">
	<div class="container">
		<div class="row">
			<header class="col-xs-12 col-sm-6">
				<h2><?php echo $this->lang->line('rental'); ?></h2>
			</header>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<p>
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($rental_content->general_content_en) ? $rental_content->general_content_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($rental_content->general_content_in) ? $rental_content->general_content_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</div>

			<figure class="col-xs-12 col-sm-6 text-center">
				<img alt="Kidsfun Exploitation & Rental" class="img-responsive" src="<?php echo !empty($rental_image->media_url) ? base_url('assets/upload/sales').'/'.$rental_image->media_url : base_url('assets/img/card-image-rect.jpg'); ?>">
			</figure>
		</div>
	</div>
</section>