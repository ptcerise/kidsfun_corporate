<!-- GET IP VISITORS -->
<?php
    $ip_address = $_SERVER['REMOTE_ADDR'];
    $date       = date('Y-m-d');

    if (!isset($_COOKIE["visitor"]))
    {
        setcookie("visitor", "$ip_address", time() +3600);
        // mysql_connect("localhost", "user", "password"); //sesuaikan host, user, dan password-nya !
        // mysql_select_db(“nama_db”) or die(mysql_error()); //sesuaikan nama database-nya

        $this->db->query("INSERT INTO visitors VALUES ('$ip_address', '$_SERVER[HTTP_USER_AGENT]', '$date')");
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets/icon/favicon.ico');?>">

    <title>Kidsfun Corporate</title>
    
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Font Awesome -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oleo+Script:400,700' rel='stylesheet' type='text/css'>
    <!-- UNITE GALLERY -->
    <link type='text/css' href="<?php echo base_url('assets/css/unite-gallery.css');?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link type='text/css' href="<?php echo base_url('assets/css/styles.min.css');?>" rel="stylesheet">
    <link type='text/css' href="<?php echo base_url('assets/css/kidsfun/custom_bo.css');?>" rel="stylesheet">
    
</head>
<body>
    <?php include_once("analyticstracking.php") ?>
    <?php $lang = $this->session->userdata('fe_lang'); ?>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container nav-container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand">
                    <a href="<?php echo base_url(); ?>">
                        <img src="<?php echo base_url('assets/img/logo-corporate.svg');?>" alt="logo" class="hidden-xs logo-header">
                    </a>
                </div>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse">
                <div class="row nb-second">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url(); ?>contact_us"><?php echo $this->lang->line('nav_contact'); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>find_us"><?php echo $this->lang->line('nav_find_us'); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>#"><i class="fa fa-shopping-cart fa-lg"></i></a>
                        </li>
                        <li>
                            <?php if($lang == 'indonesia'): ?>
                                <a href="<?php echo base_url('languageSwitcher/switchLang/english');?>">
                                    <img alt="EN" class="flag" src="<?php echo site_url('assets/icon/english.png'); ?>">
                                </a>
                            <?php else:?>
                                <a href="<?php echo base_url('languageSwitcher/switchLang/indonesia');?>">
                                    <img alt="ID" class="flag" src="<?php echo site_url('assets/icon/indonesia.png'); ?>">
                                </a>
                            <?php endif; ?>
                                
                        </li>
                    </ul>
                </div>
                <div class="row nb-main">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="visible-xs">
                            <a href="<?php echo base_url(); ?>" ><i class="fa fa-home" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>outlets" ><?php echo strtoupper($this->lang->line('nav_outlets')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>sales" ><?php echo strtoupper($this->lang->line('nav_sales')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>partners" ><?php echo strtoupper($this->lang->line('nav_partners')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>career" ><?php echo strtoupper($this->lang->line('nav_career')); ?></a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>merchants" ><?php echo strtoupper($this->lang->line('nav_merchants')); ?></a>
                        </li>
                        <li>
                            <a class="blue" href="https://kidsfun.co.id/tickets" ><?php echo strtoupper($this->lang->line('nav_tickets')); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <main>
        <!-- THE OPENING TIME -->
        <?php echo $content; ?>
    </main>

    <!-- Footer -->
    <footer class="container-fluid">
        <div class="row panel footer-top">
            <div class="container">
                <div class="row">
                    <?php

                    // NEWS FOOTER
                    $language = $this->session->userdata('fe_lang');
                    $news = $this->db->query("SELECT * FROM news ORDER BY news_date DESC")->row();
                    if($language == "indonesia"){
                        $title = isset($news->news_title_in)?$news->news_title_in:"[ KOSONG ]";
                        $content = isset($news->news_content_in)?$news->news_content_in:"[ KOSONG! ]";
                    }else{
                        $title = isset($news->news_title_en)?$news->news_title_en:"[ EMPTY ]";
                        $content = isset($news->news_content_en)?$news->news_content_en:"[ EMPTY! ]";
                    }

                    $link = "https://kidsfun.co.id/news/read/".$news->temp_id."-".url_title(strtolower($title))."-".date('d-m-Y', strtotime($news->news_date));

                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 footer-border">
                        <h4><?php echo $this->lang->line('latest_news'); ?></h4>
                        <article class="panel footer-content">
                            <a href="<?php echo $link; ?>" target="_blank" class="footer-news-title">
                                <h5><?php 
                                    if(strlen($title) <= 40){
                                            echo $title;
                                        }else{
                                            echo substr($title, 0, 40)."...";
                                        }
                                 ?></h5>
                            </a> 
                            <p class="news-date-footer text-grey"><?php echo date('d/m/Y', strtotime($news->news_date)); ?></p>
                            <div class="par footer-news-content">
                                <?php
                                    if(strlen($title) < 23){
                                        echo substr($content,0,140)."...";
                                    }else{
                                        echo substr($content,0,112)."...";
                                    }
                                ?>
                            </div>
                        </article>
                        <a class="card-btn btn btn-sm pull-right" target="_blank" href="<?php echo $link; ?>"><?php echo strtoupper($this->lang->line('btn_read_more')); ?></a>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 footer-border contact">
                        <h4><?php echo $this->lang->line('nav_contact'); ?></h4>
                        <div class="panel footer-content">
                            <div class="col-xs-2">
                                <span class="col-xs-12"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                <span class="col-xs-12"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                                <span class="col-xs-12"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                            </div>
                            <div class="col-xs-10">
                                <span class="col-xs-12">

                                <?php
                                    // echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
                                    $x = str_replace(array('<p>','</p>'), '',substr($phone, 1));
                                    $phone = explode('-',$x);
                                    $phone_number = $phone[0];
                                    foreach($phone as $key=>$val){
                                        if(count($phone) == $key+1){break;}
                                        $phone_number = $phone_number.$phone[$key+1];
                                    }
                                    $link = "tel:+62".$phone_number;
                                ?>
                                    <a href="<?php echo $link; ?>">
                                        <?php
                                            echo "(0".$phone[0].") ";
                                            foreach($phone as $key=>$val){
                                                if(count($phone) == $key+1){break;}
                                                echo $phone[$key+1]." ";
                                            }
                                        ?>
                                    </a>
                                </span>
                                <span class="col-xs-12">
                                    <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
                                </span>
                                <span class="col-xs-12">
                                    <?php echo $address; ?>
                                </span>
                            </div>
                        </div>
                        <a class="card-btn btn btn-sm pull-right" href="<?php echo base_url('contact_us'); ?>"><?php echo strtoupper($this->lang->line('btn_contact_us')); ?></a>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 footer-border">
                        <h4><?php echo $this->lang->line('site_map'); ?></h4>
                        <nav class="panel footer-content">
                            <div class="col-xs-6 footer-nav">
                                <span class="col-xs-12">
                                    <a href="<?php echo base_url(); ?>outlets"><?php echo $this->lang->line('nav_outlets'); ?></a>
                                </span>
                                <span class="col-xs-12">
                                    <a href="<?php echo base_url(); ?>sales"><?php echo $this->lang->line('nav_sales'); ?></a>
                                </span>
                                <span class="col-xs-12">
                                    <a href="<?php echo base_url(); ?>merchants"><?php echo $this->lang->line('nav_merchants'); ?></a>
                                </span>
                            </div>
                            <div class="col-xs-6 footer-nav">
                                <span class="col-xs-12">
                                    <a href="<?php echo base_url(); ?>partners"><?php echo $this->lang->line('nav_partners'); ?></a>
                                </span>
                                <span class="col-xs-12">
                                    <a href="<?php echo base_url(); ?>career"><?php echo $this->lang->line('nav_career'); ?></a>
                                </span>
                                <a href="https://kidsfun.co.id/tickets" class="btn btn-default btn-raised footer-ticket">
                                    <?php echo strtoupper($this->lang->line('btn_buy_tickets')); ?>
                                </a>
                            </div>
                        </nav>
                    </div>
                    
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center footer-border">
                        <h4><?php echo $this->lang->line('rec_product'); ?></h4>
                        <nav class="panel footer-content">

                            <a href="https://kidsfun.co.id" class="btn ghost-btn"> KIDSFUN </a>
                            <a href="https://yogyagokart.com" class="btn ghost-btn"> YOGYA GOKART </a>

                            <!-- VISITORS COUNTER -->
                            <h4 class="visitors">
                                <?php 
                                    echo $this->lang->line("footer_visitors"); 
                                    
                                    $visitors = $this->db->query("SELECT * FROM visitors");
                                    echo $visitors->num_rows();
                                ?>
                            </h4>

                            <h4 class="follow"><b><?php echo $this->lang->line('follow_us'); ?></b></h4>
                                <a href="<?php echo $trip_advisor; ?>" target="_blank" class="socmed-footer">
                                    <img alt="tripadvisor" src="<?php echo base_url('assets/icon/icon_tripadvisor_hover.svg'); ?>">
                                </a>  
                                <a href="<?php echo $facebook; ?>" target="_blank" class="socmed-footer">
                                    <img alt="facebook" src="<?php echo base_url('assets/icon/icon_facebook_hover.svg'); ?>">
                                </a>
                                 <a href="<?php echo $twitter; ?>" target="_blank" class="socmed-footer">
                                    <img alt="twitter" src="<?php echo base_url('assets/icon/icon_twitter_hover.svg'); ?>">
                                 </a>
                                 <a href="<?php echo $instagram; ?>" target="_blank" class="socmed-footer">
                                    <img alt="instagram" src="<?php echo base_url('assets/icon/icon_instagram_hover.svg'); ?>">
                                 </a>
                                 <a href="<?php echo $youtube; ?>" target="_blank" class="socmed-footer">
                                    <img alt="youtube" src="<?php echo base_url('assets/icon/icon_youtube_hover.svg'); ?>">
                                 </a>
                        </nav>
                    </div>

                </div>
            </div>
        </div>
        
        <div class="row panel footer-bottom bg-grey">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="footer-left">
                    <p class="copyright text-muted">&copy Kids Fun 2016 - allright reserved</p>
                </div>
                <div class="footer-right">
                    <p class="copyright text-muted">A concept by <a href="http://ptcerise.com/en/about-us.html" target="_blank"><img alt="Cerise" src="<?php echo base_url('assets/icon/logo-cerise.png');?>"></a></p>
                </div> 
            </div>
        </div>  
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <!-- <script src="../../assets/js/vendor/holder.min.js"></script> -->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url('assets/js/jquery.bootstrap-autohidingnavbar.min.js')?>"></script>
    <!-- FOR THE GALLERY -->
    <script src="<?php echo base_url('assets/js/unite-gallery.min.js')?>"></script>
    <script src='<?php echo base_url('assets/js/ug-theme-tiles.js')?>' type='text/javascript' ></script>

    <!-- CUSTOM JS -->
    <script src="<?php echo base_url('assets/js/scripts.js')?>"></script>
</body>
</html>
