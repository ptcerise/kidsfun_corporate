<section class="container-fluid bg-white heading">
	<div class="container">

		<div class="row">
			<header class="col-xs-12 main-header">
				<h1><?php echo $this->lang->line('nav_partners'); ?></h1>
				<p class="lead description">
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($partners_content->general_title_en) ? $partners_content->general_title_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($partners_content->general_title_in) ? $partners_content->general_title_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</header>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<p>
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($partners_content->general_content_en) ? $partners_content->general_content_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($partners_content->general_content_in) ? $partners_content->general_content_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</div>

			<figure class="col-xs-12 col-sm-6 text-center">
				<img alt="kidsfun partner" class="img-responsive" src="<?php echo !empty($partners_img->media_url) ? base_url('assets/upload/partners').'/'.$partners_img->media_url : base_url('assets/img/card-image-rect.jpg'); ?>">
			</figure>
		</div>

	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<header class="col-xs-12 col-sm-3 on-section">
				<h2><?php echo $this->lang->line('travel_agents'); ?></h2>
			</header>
		</div>

		<aside class="row dropdown">
			<div class="col-xs-12 col-sm-3">
				<h3><?php echo $this->lang->line('find_one'); ?></h3>
				<form>
					<select id="select-travel">
						<option value="" selected="" disabled=""><?php echo $this->lang->line('select_town'); ?></option>
						<?php foreach($travel_city as $city): ?>
						<option value="<?php echo $city->partners_city; ?>"><?php echo $city->partners_city; ?></option>
						<?php endforeach ?>
					</select>
				</form>
				<div class="row text-center">
					<button class="btn btn-xs btn-raised btn-reset" id="reset-travel"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
				</div>
			</div>
		</aside>
		
		<div class="row cards">
			<?php foreach($travel_list as $key => $row){ ?>
			<article class="col-xs-12 col-sm-6 col-md-3 travel travel-<?php echo $row->partners_city; ?>">
				<div class="panel card">
					<figure class="img-cards">
						<img alt="kidsfun travel agent partners" class="img-responsive" src="<?php echo !empty($row->partners_image) ? base_url('assets/upload/partners/content').'/'.$row->partners_image : base_url('assets/img/card-image-srect.jpg'); ?>">
					</figure>
					<div class="panel-body">
						<div class="fixed-content-small">
							<header>
								<h3>
									<?php echo $row->partners_name; ?>
								</h3>
							</header>
						
							<div class="address">
								<!-- PHONE -->
								<div class="row">
									<span class="col-xs-1 icon">
										<i class="fa fa-phone" aria-hidden="true"></i>
									</span>
									<span class="col-xs-11 content">
										
										<?php
		                                    // echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
		                                    $x = str_replace(array('<p>','</p>'), '',substr($row->partners_phone, 1));
		                                    $phone = explode('-',$x);
		                                    $phone_number = $phone[0];
		                                    foreach($phone as $key=>$val){
		                                        if(count($phone) == $key+1){break;}
		                                        $phone_number = $phone_number.$phone[$key+1];
		                                    }
		                                    $link = "tel:+62".$phone_number;
		                                ?>
	                                    <a href="<?php echo $link; ?>">
	                                        <?php
	                                            echo "0".$phone[0]." ";
	                                            foreach($phone as $key=>$val){
	                                                if(count($phone) == $key+1){break;}
	                                                echo $phone[$key+1]." ";
	                                            }
	                                        ?>
	                                    </a>
									</span>
								</div>
								<!-- ADDRESS -->
								<div class="row">
									<span class="col-xs-1 icon">
										<i class="fa fa-map-marker" aria-hidden="true"></i>
									</span>
									<span class="col-xs-11 content">
										<?php echo $row->partners_address; ?><br>
										<?php echo $row->partners_city; ?>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
			<?php } ?>
		</div>

		<!-- SHOW MORE BUTTON -->
		<aside class="col-xs-12 text-center btn-show-more">
			<button id="showmore" class="btn btn-default btn-raised"><?php echo $this->lang->line('btn_show_more'); ?></button>
		</aside>
	</div>

	<div class="container">
		<div class="row">
			<header class="col-xs-12 col-sm-3 on-section">
				<h2><?php echo $this->lang->line('hotels'); ?></h2>
			</header>
		</div>

		<aside class="row dropdown">
			<div class="col-xs-12 col-sm-3">
				<h3><?php echo $this->lang->line('find_one'); ?></h3>
				<form>
					<select id="select-hotel">
						<option value="" selected="" disabled="">Select a town</option>
						<?php foreach($hotel_city as $city): ?>
						<option value="<?php echo $city->partners_city; ?>"><?php echo $city->partners_city; ?></option>
						<?php endforeach ?>
					</select>
				</form>
				<div class="row text-center">
					<button class="btn btn-xs btn-raised btn-reset" id="reset-hotel"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
				</div>
			</div>
		</aside>
		
		<div class="row cards2">
			<?php foreach($hotel_list as $key => $row){ ?>
			<article class="col-xs-12 col-sm-6 col-md-3 hotel hotel-<?php echo $row->partners_city; ?>">
				<div class="panel card">
					<figure class="img-cards">
						<img alt="kidsfun hotel partners" class="img-responsive" src="<?php echo !empty($row->partners_image) ? base_url('assets/upload/partners/content').'/'.$row->partners_image : base_url('assets/img/card-image-srect.jpg'); ?>">
					</figure>
					<div class="panel-body">
						<div class="fixed-content-small">
							<header>
								<h3><?php echo $row->partners_name; ?></h3>
							</header>
							<div class="address">
								<!-- PHONE -->
								<div class="row">
									<span class="col-xs-1 icon">
										<i class="fa fa-phone" aria-hidden="true"></i>
									</span>
									<span class="col-xs-11 content">

										<?php
		                                    // echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
		                                    $x = str_replace(array('<p>','</p>'), '',substr($row->partners_phone, 1));
		                                    $phone = explode('-',$x);
		                                    $phone_number = $phone[0];
		                                    foreach($phone as $key=>$val){
		                                        if(count($phone) == $key+1){break;}
		                                        $phone_number = $phone_number.$phone[$key+1];
		                                    }
		                                    $link = "tel:+62".$phone_number;
		                                ?>
	                                    <a href="<?php echo $link; ?>">
	                                        <?php
	                                            echo "0".$phone[0]." ";
	                                            foreach($phone as $key=>$val){
	                                                if(count($phone) == $key+1){break;}
	                                                echo $phone[$key+1]." ";
	                                            }
	                                        ?>
	                                    </a>

									</span>
								</div>
								<!-- ADDRESS -->
								<div class="row">
									<span class="col-xs-1 icon">
										<i class="fa fa-map-marker" aria-hidden="true"></i>
									</span>
									<span class="col-xs-11 content">
										<?php echo $row->partners_address; ?><br>
										<?php echo $row->partners_city; ?>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
			<?php } ?>
		</div>
		<!-- SHOW MORE BUTTON -->
		<aside class="col-xs-12 text-center btn-show-more">
			<button id="showmore2" class="btn btn-default btn-raised"><?php echo $this->lang->line('btn_show_more'); ?></button>
		</aside>
	</div>
</section>

