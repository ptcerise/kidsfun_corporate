<section class="container-fluid bg-white heading">
	<div class="container">
		<div class="row">
			<header class=" col-xs-12 main-header">
				<h1><?php echo $this->lang->line('nav_merchants'); ?></h1>
			
				<p class="lead description">
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($merchants_content->general_title_en) ? $merchants_content->general_title_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($merchants_content->general_title_in) ? $merchants_content->general_title_in : "[ KOSONG! ]";
	                } ?>
				</p>
			</header>
		</div>

		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<p>
					<?php if($lang == "english" || $lang == ""){
	                    echo !empty($merchants_content->general_content_en) ? $merchants_content->general_content_en : "[ EMPTY! ]";
	                }else{
	                    echo !empty($merchants_content->general_content_in) ? $merchants_content->general_content_in : "[ KOSONG! ]";
	                } ?>
				</p>

				<p> If you wish to becoma a merchant please use our <a class="blue-link" href="#">contact form</a></p>
			</div>

			<figure class="col-xs-12 col-sm-6 text-center">
				<img alt="kidsfun merchant" class="img-responsive" src="<?php echo !empty($merchants_img->media_url) ? base_url('assets/upload/merchants').'/'.$merchants_img->media_url : base_url('assets/img/blank_img.png'); ?>">
			</figure>
		</div>
	</div>
</section>

<section class="container">
	<aside class="row dropdown-no-header">
		<div class="col-xs-12 col-sm-3">
			<h3><?php echo $this->lang->line('find_one'); ?></h3>
			<form>
				<select id="select-city">
					<option value="" selected="" disabled=""><?php echo $this->lang->line('select_town'); ?></option>
					<?php foreach($merchants_city as $city): ?>
					<option value="<?php echo $city->merchants_city; ?>"><?php echo $city->merchants_city; ?></option>
					<?php endforeach ?>
				</select>
			</form>
			<div class="row text-center">
				<button class="btn btn-xs btn-raised btn-reset" id="reset"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
			</div>
		</div>
	</aside>
	
	<div class="row cards">
		<?php foreach($merchants_list as $merchants){ ?>
		<article class="col-xs-12 col-sm-6 col-md-3 merchants <?php echo $merchants->merchants_city;?>">
			<div class="panel card">
				<figure class="img-cards">
					<img alt="kidsfun merchants" class="img-responsive" src="<?php echo !empty($merchants->merchants_image) ? base_url('assets/upload/merchants/content').'/'.$merchants->merchants_image : base_url('assets/img/card-image-srect.jpg'); ?>">
				</figure>
				<div class="panel-body">
					<header>
						<h3>
							<?php
								if(strlen($merchants->merchants_name)>12){
									echo substr($merchants->merchants_name,0,12)." ..";
								}else{
									echo $merchants->merchants_name;
								}
							?>		
						</h3>
					</header>
					<div class="fixed-content-small">
						<?php $char_en = strlen($merchants->merchants_desc_en); ?>
                        <?php $char_in = strlen($merchants->merchants_desc_in); ?>
						
						<p>
							<?php if($lang == "english" || $lang == ""){
								$desc_en = substr($merchants->merchants_desc_en,0,195);
			                    echo !empty($desc_en) ? $desc_en : "[ EMPTY! ]";
			                    if($char_en>195){
			                    	echo " ...";
			                    }
			                    elseif($char_en<=195){
			                    	echo "";
			                    }
			                }else{
			                	$desc_in = substr($merchants->merchants_desc_in,0,195);
			                    echo !empty($desc_in) ? $desc_in : "[ KOSONG! ]";
			                    if($char_in>195){
			                    	echo " ...";
			                    }
			                    elseif($char_in<=195){
			                    	echo "";
			                    }
			                } ?>
						</p>
					</div>

					<div class="panel-body btn-panel pull-right">
						<a class="card-btn" onclick="view_detail('#merchant-<?php echo $merchants->merchants_id;?>');"><?php echo strtoupper($this->lang->line('btn_learn_more')); ?></a>
					</div>
				</div>

				<!-- DETAILED INFORMATION -->
				<div class="card-detail" id="merchant-<?php echo $merchants->merchants_id;?>">
					<div class="panel-body">
						<header>
							<h3><?php echo $merchants->merchants_name;?></h3>
						</header>
						<!-- PHONE -->
						<div class="row">
							<div class="col-xs-2">
								<i class="fa fa-phone" aria-hidden="true"></i>
							</div>
							<div class="col-xs-10">
								<?php
                                    // echo !empty($phone->general_content_en) ? $phone->general_content_en:"[EMPTY!]";
                                    $x = str_replace(array('<p>','</p>'), '',substr($merchants->merchants_phone, 1));
                                    $phone = explode('-',$x);
                                    $phone_number = $phone[0];
                                    foreach($phone as $key=>$val){
                                        if(count($phone) == $key+1){break;}
                                        $phone_number = $phone_number.$phone[$key+1];
                                    }
                                    $link = "tel:+62".$phone_number;
                                ?>
                                <a href="<?php echo $link; ?>">
                                    <?php
                                        echo "0".$phone[0]." ";
                                        foreach($phone as $key=>$val){
                                            if(count($phone) == $key+1){break;}
                                            echo $phone[$key+1]." ";
                                        }
                                    ?>
	                            </a>
							</div>
						</div>
						<!-- ADDRESS -->
						<div class="row">
							<div class="col-xs-2">
								<i class="fa fa-map-marker" aria-hidden="true"></i>
							</div>
							<div class="col-xs-10">
								<?php echo $merchants->merchants_address;?><br>
								<?php echo $merchants->merchants_city;?>
							</div>
						</div>
						<!-- INFO -->
						<div class="row">
							<div class="col-xs-2">
								<i class="fa fa-question" aria-hidden="true"></i>
							</div>
							<div class="col-xs-10 fixed-content">
								<p>
									<?php if($lang == "english" || $lang == ""){
					                    echo !empty($merchants->merchants_desc_en) ? $merchants->merchants_desc_en : "[ EMPTY! ]";
					                }else{
					                    echo !empty($merchants->merchants_desc_in) ? $merchants->merchants_desc_in : "[ KOSONG! ]";
					                } ?>
								</p>
							</div>
						</div>
						<div class="panel-body btn-panel pull-right">
							<a class="card-btn" onclick="close_detail('#merchant-<?php echo $merchants->merchants_id;?>');"><?php echo strtoupper($this->lang->line('btn_close')); ?></a>
						</div>
					</div>
				</div>
			</div>
		</article>
		<?php } ?>
	</div>
	<!-- SHOW MORE BUTTON -->
	<div class="col-xs-12 btn-show-more text-center">
		<button id="showmore" class="btn btn-default btn-raised"><?php echo $this->lang->line('btn_show_more'); ?></button>
	</div>
</section>