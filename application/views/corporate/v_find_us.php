<section class="container-fluid bg-white heading">
    <div class="container">
        <div class="row">
            <header class=" col-xs-12 main-header">
                <h1><?php echo $this->lang->line('nav_find_us'); ?></h1>
            
                <p class="lead description">
                    <?php if($lang == "english" || $lang == ""){
                        echo !empty($find_us_content->general_content_en) ? $find_us_content->general_content_en : "[ EMPTY! ]";
                    }else{
                        echo !empty($find_us_content->general_content_in) ? $find_us_content->general_content_in : "[ KOSONG! ]";
                    } ?>
                </p>
            </header>
        </div>
    </div>
</section>

<!-- THE CONTENT -->
<section class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-9 col-sm-offset-1 padding-clear">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 padding-clear">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div class="par">
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($opening_time->general_content_en) ? $opening_time->general_content_en : "[ EMPTY! ]";
                            }else{
                                echo !empty($opening_time->general_content_in) ? $opening_time->general_content_in : "[ KOSONG! ]";
                            }
                        
                        ?>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div class="par">
                        <?php
                            if($lang == "english" || $lang == ""){
                                echo !empty($address->general_content_en) ? $address->general_content_en : "[ EMPTY! ]";
                            }else{
                                echo !empty($address->general_content_in) ? $address->general_content_in : "[ KOSONG! ]";
                            }
                        
                        ?> 
                        </div>           
                    </div>
                </div>
            </div>

            <figure class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img class="img-responsive img-thumbnail" src="<?php echo !empty($find_us_map->media_url) ? base_url('assets/upload/find_us').'/'.$find_us_map->media_url:base_url('assets/img/card-image-rect.jpg'); ?>" alt="info_map">
            </figure>

        </div>
    </div>
</section>