<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("outlets"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("outlets"); ?> </li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">
    <!-- CONTENT OUTLETS PAGE -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/outlets/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_in" rows="3" ><?php echo isset($outlets_content->general_title_in) ? $outlets_content->general_title_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_en" rows="3" ><?php echo isset($outlets_content->general_title_en) ? $outlets_content->general_title_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="outlets_content_in" rows="5" ><?php echo isset($outlets_content->general_content_in) ? $outlets_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="outlets_content_en" rows="5" ><?php echo isset($outlets_content->general_content_en) ? $outlets_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                        <img id="outlets_img" class="img-responsive img-thumbnail img-center" src="<?php echo !empty($outlets_img->media_url) ? base_url('assets/upload/outlets/thumbnail').'/'.$outlets_img->media_url : base_url('assets/img/blank_img.png'); ?>" alt="outlet">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input onchange="previewImage(this)" type="file" name="outlets_img" accept="image/gif, image/x-png, image/jpeg">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1750px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- TABLE OF OUTLETS LIST -->
        <div class="row" id="outlets_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("outlets_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_outlets"); ?>" data-toggle="modal" data-target="#add_page"><i class="fa fa-plus"></i> Outlet</a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('outlets_list');?>
                            <table class="table table-hover table-bordered outlets" id="outlets">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("name_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("phone_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("address_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("city_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("image_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($outlets_list as $key => $row):?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->outlets_name;?></td>
                                        <td><?php echo $row->outlets_phone;?></td>
                                        <td class="address"><?php echo $row->outlets_address;?></td>
                                        <td><?php echo $row->outlets_city;?></td>
                                        <td><img id="outlets_image" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/upload/outlets/content/thumbnail/'.$row->outlets_image) ;?>" alt="outlets"></td>
                                        <td><center>

                                            <a href="#" class="btn btn-warning btn-xs edit_page"
                                            data-target="#edit_page" data-toggle="modal"
                                            title="<?php echo $this->lang->line("edit_outlets"); ?>"

                                            data-id="<?php echo @$row->outlets_id; ?>"
                                            data-name="<?php echo $row->outlets_name;?>"
                                            data-phone="<?php echo $row->outlets_phone;?>"
                                            data-address="<?php echo $row->outlets_address;?>"
                                            data-city="<?php echo $row->outlets_city;?>"
                                            data-img="<?php echo base_url('assets/upload/outlets/content/thumbnail/'.$row->outlets_image); ?>"

                                            ><i class="fa fa-edit"></i> <?php echo $this->lang->line("edit_outlets"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/outlets/delete_outlets/'.@$row->outlets_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_outlets"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- TABLE OF GALLERY -->
        <div class="row" id="gallery">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("gallery"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_gallery"); ?>" data-toggle="modal" data-target="#add_gallery"><i class="fa fa-plus"></i> <?php echo $this->lang->line("gallery"); ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_gallery');?>
                            <table class="table table-hover table-bordered gallery" id="gallery">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("image_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_table_in"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_table_en"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($gallery_list as $key => $row):?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td>
                                            <img id="gallery_image" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/upload/gallery/thumbnail/'.$row->media_url) ;?>" alt="gallery">
                                        </td>
                                        <td><?php echo $row->media_content_in;?></td>
                                        <td><?php echo $row->media_content_en;?></td>
                                        <td><center>

                                            <a href="#" class="btn btn-warning btn-xs edit_gallery"
                                            data-target="#edit_gallery" data-toggle="modal"
                                            title="<?php echo $this->lang->line("edit_gallery"); ?>"

                                            data-id="<?php echo @$row->media_id; ?>"
                                            data-descin="<?php echo $row->media_content_in;?>"
                                            data-descen="<?php echo $row->media_content_en;?>"
                                            data-img="<?php echo base_url('assets/upload/gallery/thumbnail/'.$row->media_url) ;?>"

                                            ><i class="fa fa-edit"></i> <?php echo $this->lang->line("edit_gallery"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/outlets/delete_gallery/'.@$row->media_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_gallery"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- ================================ ALL MODALS ================================== -->
        <!-- MODAL ADD NEW OUTLET -->
        <div class="modal fade" id="add_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_form" action="<?php echo base_url('backend/outlets/add_outlet');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("add_outlets"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="outlets_name" id="outlets_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("phone"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="outlets_phone" id="outlets_phone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("address"); ?></label>
                                <div class="col-md-6">
                                    <textarea class="form-control textarea" rows="5" name="outlets_address"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("city"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  name="outlets_city" id="outlets_city" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-6">

                                    <img id="content_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_large.png'); ?>" alt="outlet">

                                    <input onchange="previewModal(this)" type="file" name="content_img" accept="image/gif, image/x-png, image/jpeg" required="">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1250px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL EDIT OUTLET -->
        <div class="modal fade" id="edit_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" name="edit_outlet" id="edit_form" action="<?php echo base_url('backend/outlets/edit_outlet');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("edit_outlets"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control outlets_name" name="outlets_name" id="outlets_name" required>
                                    <input type="hidden" class="form-control outlets_id" name="outlets_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("phone"); ?></label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control outlets_phone" name="outlets_phone" id="outlets_phone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("address"); ?></label>
                                <div class="col-md-7">
                                    <textarea id="outlets_address" class="form-control textarea outlets_address" rows="5" name="outlets_address"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("city"); ?></label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control outlets_city"  name="outlets_city" id="outlets_city" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-7">

                                    <img id="edit_img" class="img-responsive img-thumbnail img-center" src="" alt="outlet">

                                    <input onchange="previewModal(this)" class="edit_img" type="file" name="edit_img" accept="image/gif, image/x-png, image/jpeg">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1250px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL ADD NEW GALLERY -->
        <div class="modal fade" id="add_gallery" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_gallery_form" action="<?php echo base_url('backend/outlets/add_gallery');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("add_gallery"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-7">

                                    <img id="content_gallery_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_large.png'); ?>" alt="gallery">

                                    <input onchange="previewModal(this)" type="file" name="content_gallery_img" accept="image/gif, image/x-png, image/jpeg" required="">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_in"); ?></label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" name="gallery_caption_in" id="gallery_caption_in" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_en"); ?></label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" name="gallery_caption_en" id="gallery_caption_en" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL EDIT GALLERY -->
        <div class="modal fade" id="edit_gallery" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="edit_gallery_form" action="<?php echo base_url('backend/outlets/edit_gallery');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("edit_gallery"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-7">

                                    <img id="edit_gallery_img" class="img-responsive img-thumbnail img-center" src="" alt="gallery">

                                    <input onchange="previewModal(this)" type="file" name="edit_gallery_img" accept="image/gif, image/x-png, image/jpeg" >
                                    <input type="hidden" class="form-control media_id" name="media_id" required>
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_in"); ?></label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control gallery_caption_in" name="gallery_caption_in" id="gallery_caption_in" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("caption_en"); ?></label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control gallery_caption_en" name="gallery_caption_en" id="gallery_caption_en" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>