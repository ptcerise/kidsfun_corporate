<script type="text/javascript">
	$(document).ready(function(){
		// $('.textarea').wysihtml5({
	 //                "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
	 //                "font-styles": false,
	 //                "html": true,
	 //                "link": false,
	 //                "image": false,
  //                   parser: function(html) {
  //                       return html;
  //                   }
	 //    });
        $('table.partners').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "20%" },
            { "width": "10%" },
            { "width": "15%" },
            { "width": "10%" }
            ]
        });
	});
    $(document).on( "click", '.edit_partners',function(e) {

        var id = $(this).data('id');
        var name = $(this).data('name');
        var category = $(this).data('category');
        var phone = $(this).data('phone');
        var address = $(this).data('address');
        var city = $(this).data('city');
        var img = $(this).data('img');

        $(".partners_id").val(id);
        $(".partners_name").val(name);
        $(".partners_category").val(category);
        $(".partners_phone").val(phone);
        $(".partners_city").val(city);
        $('#edit_img').attr('src', img);
        tinyMCE.get('partners_address').setContent(address, {format : 'text'});
        // var editorObj = $("#partners_address").data('wysihtml5');
        // var editor = editorObj.editor;
        // editor.setValue(address);
    });
    
	function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#partners_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function previewModal(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#add_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
                $('#edit_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
                $('#content_gallery_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>