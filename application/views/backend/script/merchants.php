<script type="text/javascript">
	$(document).ready(function(){
		// $('.textarea').wysihtml5({
	 //                "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
	 //                "font-styles": false,
	 //                "html": true,
	 //                "link": false,
	 //                "image": false,
  //                   parser: function(html) {
  //                       return html;
  //                   }
	 //    });
	    $('table.merchants').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "10%" },
            { "width": "15%" },
            { "width": "10%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" },
            ]
        });
	});

	function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#merchants_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function previewModal(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#content_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
                $('#edit_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on( "click", '.edit_merchant',function(e) {

        var id = $(this).data('id');
        var name = $(this).data('name');
        var phone = $(this).data('phone');
        var address = $(this).data('address');
		var descin = $(this).data('descin');
        var descen = $(this).data('descen');
        var city = $(this).data('city');
        var img = $(this).data('img');

        $(".merchants_id").val(id);
        $(".merchants_name").val(name);
        $(".merchants_phone").val(phone);
        $(".merchants_city").val(city);
        $('#edit_img').attr('src', img);
        tinyMCE.get('merchants_address').setContent(address);
        tinyMCE.get('merchants_desc_in').setContent(descin);
        tinyMCE.get('merchants_desc_en').setContent(descen);
        
        // var addr = $("#merchants_address").data('wysihtml5');
        // var editor_addr = addr.editor;
        // editor_addr.setValue(address);

        // var desc_in = $("#merchants_desc_in").data('wysihtml5');
        // var editor_in = desc_in.editor;
        // editor_in.setValue(descin);

        // var desc_en = $("#merchants_desc_en").data('wysihtml5');
        // var editor_en = desc_en.editor;
        // editor_en.setValue(descen);
    });
</script>