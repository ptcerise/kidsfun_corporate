<script type="text/javascript">
	$(document).ready(function(){
		// $('.textarea').wysihtml5({
  //           "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
  //           "font-styles": false,
  //           "html": true,
  //           "link": true,
  //           "image": false,
  //           parser: function(html) {
  //                       return html;
  //                   }
  //       });
        $('table.menu').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "15%" }
            ]
        });
	});

    $(document).on( "click", '.edit_page',function(e) {

        var id = $(this).data('id');
        var titlein = $(this).data('titlein');
        var titleen = $(this).data('titleen');
        var img = $(this).data('img');

        $(".media_id").val(id);
        $(".media_title_in").val(titlein);
        $(".media_title_en").val(titleen);
        $('#edit_img').attr('src', img);
    });

	function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#about_us_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function previewModal(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#content_img')
                    .attr('src', e.target.result)
                    .width(200)
                    .height(200);
                $('#edit_img')
                    .attr('src', e.target.result)
                    .width(200)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>