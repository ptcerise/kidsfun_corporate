<script type="text/javascript">
	$(document).ready(function(){
		// $('.textarea').wysihtml5({
	 //                "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
	 //                "font-styles": false,
	 //                "html": true,
	 //                "link": true,
	 //                "image": false,
  //                   parser: function(html) {
  //                       return html;
  //                   }
	 //    });
        $('#sales').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "10%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "10%" },
            { "width": "10%" }
            ]
        });
        $('#products_price').priceFormat({
            prefix: 'Rp ',
            centsLimit: 0,
            thousandsSeparator: '.'
        });
        $('#products_edit').priceFormat({
            prefix: 'Rp ',
            centsLimit: 0,
            thousandsSeparator: '.'
        });
        $(document).on( "click", '.edit_page',function(e) {

            var id = $(this).data('id');
            var name = $(this).data('name');
            var dimension = $(this).data('dimension');
            var power = $(this).data('power');
            var delivery = $(this).data('delivery');
            var price = $(this).data('price');
            var img = $(this).data('img');

            $(".products_id").val(id);
            $(".products_name").val(name);
            $(".products_dimension").val(dimension);
            $(".products_power").val(power);
            $(".products_delivery_time").val(delivery);
            $(".products_price").val(price);
            $("#edit_img").attr('src', img);
            //$('#edit_img').attr('src', $('#outlets_image').attr('src'));
            // tinyMCE.get('outlets_address').setContent(address);
        });
	});
	function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#sales_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function previewModal(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#content_img')
                    .attr('src', e.target.result)
                    .width(180)
                    .height(180);
                $('#edit_img')
                    .attr('src', e.target.result)
                    .width(180)
                    .height(180);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function previewRental(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#rental_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>