<script type="text/javascript">
	$(document).ready(function(){
		// $('.textarea').wysihtml5({
	 //                "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
	 //                "font-styles": false,
	 //                "html": true,
	 //                "link": true,
	 //                "image": false,
  //                   parser: function(html) {
  //                       return html;
  //                   }
	 //    });
        $('table.outlets').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "20%" },
            { "width": "10%" },
            { "width": "15%" },
            { "width": "10%" }
            ]
        });
        $('table.gallery').DataTable({
            "columns": [
            { "width": "2%" },
            { "width": "10%" },
            { "width": "20%" },
            { "width": "20%" },
            { "width": "10%" }
            ]
        });
	});

    $(document).on( "click", '.edit_page',function(e) {

        var id = $(this).data('id');
        var name = $(this).data('name');
        var phone = $(this).data('phone');
        var address = $(this).data('address');
        var city = $(this).data('city');
        var img = $(this).data('img');

        $(".outlets_id").val(id);
        $(".outlets_name").val(name);
        $(".outlets_phone").val(phone);
        $(".outlets_city").val(city);
        $('#edit_img').attr('src', img);
        tinyMCE.get('outlets_address').setContent(address);
        // var editorObj = $("#outlets_address").data('wysihtml5');
        // var editor = editorObj.editor;
        // editor.setValue(address);
    });
    
    $(document).on( "click", '.edit_gallery',function(e) {

        var id = $(this).data('id');
        var descin = $(this).data('descin');
        var descen = $(this).data('descen');
        var img = $(this).data('img');

        $(".media_id").val(id);
        $(".gallery_caption_in").val(descin);
        $(".gallery_caption_en").val(descen);
        $('#edit_gallery_img').attr('src', img);
    });

	function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#outlets_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function previewModal(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#content_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
                $('#edit_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
                $('#content_gallery_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
                $('#edit_gallery_img')
                    .attr('src', e.target.result)
                    .width(250)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>