<script type="text/javascript">
	$(document).ready(function(){
        $('.view-page').click(function(){
            var contact_id = $(this).attr('id');
            var info = 'contact_id='+contact_id;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('backend/contact_us/view_message') ?>",
                data: info,
                success: function(html){
                    $('#view-page').html(html);
                }
            });
        });
		// $('.textarea').wysihtml5({
	 //                "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
	 //                "font-styles": false,
	 //                "html": true,
	 //                "link": false,
	 //                "image": false,
  //                   parser: function(html) {
  //                       return html;
  //                   }
	 //    });
	    $('table.contact').DataTable({
            "columns": [
            { "width": "3%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "20%" },
            { "width": "10%" },
            { "width": "15%" },
            { "width": "10%" }
            ]
        });
	});
</script>