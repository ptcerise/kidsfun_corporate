<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("follow_us"); ?>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i>
                <?php echo $this->lang->line("follow_us"); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line("links"); ?></div>
                    <div class="panel-body">
                    <?php echo $this->session->flashdata('info_follow_us');?>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/follow_us/trip_advisor');?>" class="form-inline" role="form">
                                    <div class="form-group" style="padding-bottom:15px">
                                        <label class="sr-only"> Trip Advisor </label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-tripadvisor" style="width:20px"></i></div>
                                            <input type="text" title="Trip Advisor" name="trip_advisor" class="form-control" placeholder="Trip advisor" value="<?php echo !empty($trip_advisor->general_url) ? $trip_advisor->general_url:''; ?>">
                                        </div>
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/follow_us/facebook');?>" class="form-inline" role="form">
                                    <div class="form-group" style="padding-bottom:15px">
                                        <label class="sr-only"> Facebook </label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-facebook-official" style="width:20px"></i></div>
                                            <input type="text" title="Facebook" class="form-control" name="facebook" placeholder="Facebook" value="<?php echo !empty($facebook->general_url) ? $facebook->general_url:''; ?>">   
                                        </div>
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/follow_us/twitter');?>" class="form-inline" role="form">
                                    <div class="form-group" style="padding-bottom:15px">
                                        <label class="sr-only"> Twitter </label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-twitter" style="width:20px"></i></div>
                                            <input type="text" title="Twitter" class="form-control" name="twitter" placeholder="Twitter" value="<?php echo !empty($twitter->general_url) ? $twitter->general_url:''; ?>">   
                                        </div>
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/follow_us/instagram');?>" class="form-inline" role="form">
                                    <div class="form-group" style="padding-bottom:15px">
                                        <label class="sr-only"> Instagram </label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-instagram" style="width:20px"></i></div>
                                            <input type="text" title="Instagram" class="form-control" name="instagram" placeholder="Instagram" value="<?php echo !empty($instagram->general_url) ? $instagram->general_url:''; ?>">   
                                        </div>
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-center">
                                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/follow_us/youtube');?>" class="form-inline" role="form">
                                    <div class="form-group">
                                        <label class="sr-only"> Youtube </label>
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-youtube-play" style="width:20px"></i></div>
                                            <input type="text" title="Youtube Channel" class="form-control" name="youtube" placeholder="Youtube channel" value="<?php echo !empty($youtube->general_url) ? $youtube->general_url:''; ?>">   
                                        </div>
                                        <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>