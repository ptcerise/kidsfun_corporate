<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("form"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("form"); ?> </li>
        </ol>
    </section>

    <section class="content">
    <!-- CONTENT UPLOAD FORM -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"><?php echo $this->lang->line("form"); ?></div>
                    <div class="panel-body">
                    <?php echo $this->session->flashdata('info_form');?>
                        <div class="well">
                            <div class="row">
                                <div class="col-md-12">
                                	<div class="form-group">
                                		<form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/form/form_travel');?>" class="form-horizontal" role="form">
	                                        <label class="col-md-4 control-label"><p>Travel agent form :</p></label>
	                                        <div class="col-sm-3">
	                                            <input type="file" name="form_travel" accept=".doc,.docx" required>
	                                            <input type="hidden" name="MAX_FILE_SIZE" value="300000">
	                                        </div>
	                                        <div class=" panel-save">
				                                <button type="submit" class="btn btn-sm btn-primary" title="<?php echo $this->lang->line("btn_upload"); ?>"><?php echo $this->lang->line("btn_upload"); ?>
				                                </button>
				                                <?php if($check_travel!='') { ?>
				                                	<i class="fa fa-check-circle" style="color:green"></i> Uploaded
				                                <?php } ?>
				                            </div>
				                        </form>
                                    </div>
                                    <div class="form-group">
                                    	<form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/form/form_hotel');?>" class="form-horizontal" role="form">
	                                        <label class="col-md-4 control-label"><p>Hotel form :</p></label>
	                                        <div class="col-sm-3">
	                                            <input type="file" name="form_hotel" accept=".doc,.docx" required>
	                                            <input type="hidden" name="MAX_FILE_SIZE" value="300000">
	                                        </div>
	                                        <div class=" panel-save">
				                                <button type="submit" class="btn btn-sm btn-primary" title="<?php echo $this->lang->line("btn_upload"); ?>"><?php echo $this->lang->line("btn_upload"); ?>
				                                </button>
				                                <?php if($check_hotel!='') { ?>
				                                	<i class="fa fa-check-circle" style="color:green"></i> Uploaded
				                                <?php } ?>
				                            </div>
				                        </form>
                                    </div>
                                    <div class="form-group">
                                    	<form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/form/form_career');?>" class="form-horizontal" role="form">
	                                        <label class="col-md-4 control-label"><p>Career form :</p></label>
	                                        <div class="col-sm-3">
	                                            <input type="file" name="form_career" accept=".doc,.docx" required>
	                                            <input type="hidden" name="MAX_FILE_SIZE" value="300000">
	                                        </div>
	                                        <div class=" panel-save">
				                                <button type="submit" class="btn btn-sm btn-primary" title="<?php echo $this->lang->line("btn_upload"); ?>"><?php echo $this->lang->line("btn_upload"); ?>
				                                </button>
				                                <?php if($check_career!='') { ?>
				                                	<i class="fa fa-check-circle" style="color:green"></i> Uploaded
				                                <?php } ?>
				                            </div>
				                        </form>
                                    </div>
                                    <div class="form-group">
                                    	<form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/form/form_merchant');?>" class="form-horizontal" role="form">
	                                        <label class="col-md-4 control-label"><p>Merchant form :</p></label>
	                                        <div class="col-sm-3">
	                                            <input type="file" name="form_merchant" accept=".doc,.docx" required>
	                                            <input type="hidden" name="MAX_FILE_SIZE" value="300000">
	                                        </div>
	                                        <div class=" panel-save">
				                                <button type="submit" class="btn btn-sm btn-primary" title="<?php echo $this->lang->line("btn_upload"); ?>"><?php echo $this->lang->line("btn_upload"); ?>
				                                </button>
				                                <?php if($check_merchant!='') { ?>
				                                	<i class="fa fa-check-circle" style="color:green"></i> Uploaded
				                                <?php } ?>
				                            </div>
				                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>