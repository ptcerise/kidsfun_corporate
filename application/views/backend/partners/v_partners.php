<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("partners"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("partners"); ?> </li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">
    	<!-- CONTENT PARTNERS PAGE -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/partners/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_in" rows="3" ><?php echo isset($partners_content->general_title_in) ? $partners_content->general_title_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_en" rows="3" ><?php echo isset($partners_content->general_title_en) ? $partners_content->general_title_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="partners_content_in" rows="5" ><?php echo isset($partners_content->general_content_in) ? $partners_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="partners_content_en" rows="5" ><?php echo isset($partners_content->general_content_en) ? $partners_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                        <img id="partners_img" class="img-responsive img-thumbnail img-center" src="<?php echo !empty($partners_img->media_url) ? base_url('assets/upload/partners/thumbnail').'/'.$partners_img->media_url : base_url('assets/img/blank_img.png'); ?>" alt="partners">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input onchange="previewImage(this)" type="file" name="partners_img" accept="image/gif, image/x-png, image/jpeg">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1750px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- TABLE OF PARTNERS LIST -->
        <div class="row" id="partners_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("partners_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_partners"); ?>" data-toggle="modal" data-target="#add_partners"><i class="fa fa-plus"></i> <?php echo $this->lang->line("partners"); ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('partners_list');?>
                            <table class="table table-hover table-bordered partners" id="partners">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("name_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("category_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("phone_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("address_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("city_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("image_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($partners_list as $key => $row):?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->partners_name;?></td>
                                        <td><?php echo $row->partners_category;?></td>
                                        <td><?php echo $row->partners_phone;?></td>
                                        <td><?php echo $row->partners_address;?></td>
                                        <td><?php echo $row->partners_city;?></td>
                                        <td><img id="content_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/upload/partners/content/thumbnail/'.$row->partners_image) ;?>" alt="partner"></td>
                                        <td><center>

                                            <a href="#" class="btn btn-warning btn-xs edit_partners"
                                            data-target="#edit_partners" data-toggle="modal"
                                            title="<?php echo $this->lang->line("edit_partners"); ?>"

                                            data-id="<?php echo @$row->partners_id; ?>"
                                            data-name="<?php echo $row->partners_name;?>"
                                            data-category="<?php echo $row->partners_category;?>"
                                            data-phone="<?php echo $row->partners_phone;?>"
                                            data-address="<?php echo $row->partners_address;?>"
                                            data-city="<?php echo $row->partners_city;?>"
                                            data-img="<?php echo base_url('assets/upload/partners/content/thumbnail/'.$row->partners_image) ;?>"

                                            ><i class="fa fa-edit"></i> <?php echo $this->lang->line("edit_partners"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/partners/delete_partner/'.@$row->partners_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_partners"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- MODAL ADD NEW PARTNER -->
        <div class="modal fade" id="add_partners" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_form" action="<?php echo base_url('backend/partners/add_partner');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("add_partners"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="partners_name" id="partners_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("category"); ?></label>
                                <div class="col-md-6">
                                    <select class="form-control" id="partners_category" name="partners_category" required>
                                        <option value=""><?php echo $this->lang->line("choose_category"); ?></option>
                                        <option>Travel Agent</option>
                                        <option>Hotel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("phone"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="partners_phone" id="partners_phone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("address"); ?></label>
                                <div class="col-md-6">
                                    <textarea class="form-control textarea" rows="5" name="partners_address"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("city"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  name="partners_city" id="partners_city" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-6">

                                    <img id="add_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_large.png'); ?>" alt="partner">

                                    <input onchange="previewModal(this)" type="file" name="add_img" accept="image/gif, image/x-png, image/jpeg" required="">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1250px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL EDIT PARTNER -->
        <div class="modal fade" id="edit_partners" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="edit_form" action="<?php echo base_url('backend/partners/edit_partner');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("edit_partners"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control partners_name" name="partners_name" id="partners_name" required>
                                    <input type="hidden" class="form-control partners_id" name="partners_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("category"); ?></label>
                                <div class="col-md-6">
                                    <select class="form-control partners_category" id="partners_category" name="partners_category" required>
                                        <option value=""><?php echo $this->lang->line("choose_category"); ?></option>
                                        <option value="Travel Agent">Travel Agent</option>
                                        <option value="Hotel">Hotel</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("phone"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control partners_phone" name="partners_phone" id="partners_phone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("address"); ?></label>
                                <div class="col-md-6">
                                    <textarea id="partners_address" class="form-control textarea partners_address" rows="5" name="partners_address"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("city"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control partners_city"  name="partners_city" id="partners_city" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-6">

                                    <img id="edit_img" class="img-responsive img-thumbnail img-center" src="" alt="partner">

                                    <input onchange="previewModal(this)" type="file" name="edit_img" accept="image/gif, image/x-png, image/jpeg" >
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1250px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>