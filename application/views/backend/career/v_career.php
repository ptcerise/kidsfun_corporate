<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("career"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("career"); ?> </li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">
    	<!-- CONTENT CAREER PAGE -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/career/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="career_content_in" rows="5" ><?php echo isset($career_content->general_content_in) ? $career_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                    	<div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="career_content_en" rows="5" ><?php echo isset($career_content->general_content_en) ? $career_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- TABLE OF CAREER LIST -->
        <div class="row" id="career_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("job_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_job"); ?>" data-toggle="modal" data-target="#add_job"><i class="fa fa-plus"></i> <?php echo $this->lang->line("job"); ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('job_list');?>
                            <table class="table table-hover table-bordered career" id="career">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("title_table_in"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("title_table_en"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_table_in"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_table_en"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($job_list as $key => $row):?>
                                        <?php $char_en = strlen($row->career_desc_en); ?>
                                        <?php $char_in = strlen($row->career_desc_in); ?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->career_title_in;?></td>
                                        <td><?php echo $row->career_title_en;?></td>
                                        <td><?php echo substr($row->career_desc_in,0,200);?>
                                            <?php if($char_in>200) {echo "(...)";} elseif($char_in<=200) {echo "";} ?>
                                        </td>
                                        <td><?php echo substr($row->career_desc_en,0,200);?>
                                            <?php if($char_en>200) {echo "(...)";} elseif($char_en<=200) {echo "";} ?>
                                        </td>
                                        <td><center>

                                            <a href="#" class="btn btn-warning btn-xs edit_job"
                                            data-target="#edit_job" data-toggle="modal"
                                            title="<?php echo $this->lang->line("edit_job"); ?>"

                                            data-id="<?php echo @$row->career_id; ?>"
                                            data-titlein="<?php echo $row->career_title_in;?>"
                                            data-titleen="<?php echo $row->career_title_en;?>"
                                            data-descin="<?php echo $row->career_desc_in;?>"
                                            data-descen="<?php echo $row->career_desc_en;?>"

                                            ><i class="fa fa-edit"></i> <?php echo $this->lang->line("edit_job"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/career/delete_job/'.@$row->career_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_job"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- MODAL ADD NEW JOB -->
        <div class="modal fade" id="add_job" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_form" action="<?php echo base_url('backend/career/add_job');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("add_job"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("title_in"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="job_title_in" id="job_title_in" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("title_en"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="job_title_en" id="job_title_en" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_in"); ?></label>
                                <div class="col-md-6">
                                    <textarea class="form-control textarea" rows="5" name="job_desc_in"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_en"); ?></label>
                                <div class="col-md-6">
                                    <textarea class="form-control textarea"  name="job_desc_en"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL EDIT JOB -->
        <div class="modal fade" id="edit_job" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="edit_form" action="<?php echo base_url('backend/career/edit_job');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("edit_job"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("title_in"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control job_title_in" name="job_title_in" id="job_title_in" required>
                                    <input type="hidden" class="form-control job_id" name="job_id" id="job_id" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("title_en"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control job_title_en" name="job_title_en" id="job_title_en" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_in"); ?></label>
                                <div class="col-md-6">
                                    <textarea id="job_desc_in" class="form-control textarea job_desc_in" rows="5" name="job_desc_in"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_en"); ?></label>
                                <div class="col-md-6">
                                    <textarea id="job_desc_en" class="form-control textarea job_desc_en" rows="5" name="job_desc_en"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>