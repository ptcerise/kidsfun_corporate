<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title"><?php echo $this->lang->line("edit_user"); ?></h4>
        </div>
        <div class="modal-body">
            <div class="col-md-12">
                <div class="row">
                    <form method="POST" action="<?php echo base_url('backend/user/save_user');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                        <div class="panel panel-info">
                            <div class="panel-body">                
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line("username"); ?></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="user_name" value="<?php echo $edit_user->user_name;?>" required readonly>
                                        <input type="hidden" class="form-control" name="user_id" value="<?php echo $edit_user->user_id;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line("fullname"); ?></label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="user_full_name" value="<?php echo $edit_user->user_full_name;?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group panel-save">
                                    <button type="button" class="btn btn-primary update_name" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="form-admin1">
                            <div class="panel panel-warning">
                                <div class="panel-heading"><?php echo $this->lang->line("admin_confirm"); ?></div>
                                <div class="panel-body"> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Admin :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="<?php echo $admin->user_name;?>" required readonly>
                                            <input type="hidden" class="form-control" name="admin_id" id="admin_id" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><?php echo $this->lang->line("password"); ?></label>
                                        <div class="col-md-6">
                                            <input type="hidden" class="form-control" name="admin_pass" id="admin_pass" value="<?php echo $admin->user_pass;?>">
                                            <input type="password" class="form-control" name="check_pass" id="check_pass" onInput="check_admin(this)" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button id="<?php echo @$edit_user->user_id;?>" type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                        <button type="button" class="btn btn-danger" id="cancel1" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <form method="POST" action="<?php echo base_url('backend/user/save_pass/'.$edit_user->user_id);?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                        <div class="panel panel-info">
                            <div class="panel-body"> 
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line("current_pass"); ?></label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="current_pass" id="current_pass" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line("new_pass"); ?></label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="new_pass" id="new_pass" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"><?php echo $this->lang->line("confirm_pass"); ?></label>
                                    <div class="col-md-6">
                                        <input type="password" class="form-control" name="confirm_pass" id="confirm_pass" onInput="check(this);" required>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="form-group panel-save">
                                    <button href="#" type="button" class="btn btn-primary update_pass" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="form-admin2">
                            <div class="panel panel-warning">
                                <div class="panel-heading"><?php echo $this->lang->line("admin_confirm"); ?></div>
                                <div class="panel-body"> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Admin :</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="<?php echo $admin->user_name;?>" required readonly>
                                            <input type="hidden" class="form-control" name="admin_id" id="admin_id" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"><?php echo $this->lang->line("password"); ?></label>
                                        <div class="col-md-6">
                                            <input type="hidden" class="form-control" name="admin_pass" id="admin_pass" value="<?php echo $admin->user_pass;?>">
                                            <input type="password" class="form-control" name="check_pass" id="check_pass" onInput="check_admin(this)" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <div class="form-group panel-save">
                                        <button href="#" id="<?php echo @$edit_user->user_id;?>" type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                        <button type="button" class="btn btn-danger" id="cancel2" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <center>
                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
            </center>
        </div>
    </div>
</div>

<script type="text/javascript">
    function check(input){
        if(input.value != $('#new_pass').val()){
            input.setCustomValidity('Password must be match.');
        }
        else{
            input.setCustomValidity('');
        }
    }   
</script>

<script type="text/javascript">
    function check_admin(input){
        if(md5(input.value) != $('#admin_pass').val()){
            input.setCustomValidity('Password must be match.');
        }
        else{
            input.setCustomValidity('');
        }
    }   
</script>

<script type="text/javascript">
    $('.form-admin1').hide();
    $('.update_name').on('click', function(){
        $('.update_name').hide();
        $('.form-admin1').fadeToggle(1000);
    });
    $('#cancel1').on('click', function(){
        $('.form-admin1').fadeToggle(1000);
        $('.update_name').show('');
        $('#check_pass').val('');
    });
</script>

<script type="text/javascript">
    $('.form-admin2').hide();
    $('.update_pass').on('click', function(){
        $('.update_pass').hide();
        $('.form-admin2').fadeToggle(1000);
    });
    $('#cancel2').on('click', function(){
        $('.form-admin2').fadeToggle(1000);
        $('.update_pass').show();
        $('#check_pass').val('');
    });
</script>