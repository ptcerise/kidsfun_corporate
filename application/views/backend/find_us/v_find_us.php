<div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("find_us"); ?>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i>
                <?php echo $this->lang->line("find_us"); ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- FIND US CONTENT -->
        <div class="row" id="content">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/find_us/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="text_content_in" rows="5" required><?php echo isset($find_us_content->general_content_in) ? $find_us_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="text_content_en" rows="5" required><?php echo isset($find_us_content->general_content_en) ? $find_us_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- INFORMATION -->
        <div class="row" id="info">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/find_us/info');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading">Info</div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_find_us');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("open_time"); ?></p></label><br>
                                                <label class="control-label">English</label>
                                                <textarea class="form-control textarea" name="open_time_en" rows="2"><?php echo isset($open_time->general_content_en) ? $open_time->general_content_en:''; ?></textarea>

                                                <label class="control-label">Indonesia</label>
                                                <textarea class="form-control textarea" name="open_time_in" rows="2"><?php echo isset($open_time->general_content_in) ? $open_time->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("address"); ?></p></label><br>
                                                <label class="control-label">English</label>
                                                <textarea class="form-control textarea" name="address_en" rows="2"><?php echo isset($address->general_content_en) ? $address->general_content_en:''; ?></textarea>

                                                <label class="control-label">Indonesia</label>
                                                <textarea class="form-control textarea" name="address_in" rows="2"><?php echo isset($address->general_content_in) ? $address->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <img id="find_us_img" class="img-responsive img-thumbnail img-center" src="<?php echo !empty($find_us_map->media_url) ? base_url('assets/upload/find_us/thumbnail').'/'.$find_us_map->media_url:base_url('assets/img/blank_img.png'); ?>" alt="info_map">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?> 
                                            <?php echo $this->lang->line("map"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input onchange="previewImage(this)" type="file" name="find_us_map" accept="image/gif, image/x-png, image/jpeg">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1000px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                                <input type="hidden" value="<?php echo isset($find_us_map->media_id) ? $find_us_map->media_id: ''; ?>" name="map_id" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>