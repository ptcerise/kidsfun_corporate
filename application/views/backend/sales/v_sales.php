<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("sales"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("sales"); ?> </li>
        </ol>
    </section>

    <!-- Main content -->

    <!-- HEAD SALES PAGE -->
    <section class="content">
        <div class="row">
        	<div class="col-md-12">
        		<form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/sales/edit_general');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("general_content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_general');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("title_in"); ?></p></label>
                                                <input type="text" class="form-control" name="general_title_in" rows="3" value="<?php echo isset($general->general_title_in) ? $general->general_title_in:''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("title_en"); ?></p></label>
                                                <input type="text" class="form-control" name="general_title_en" rows="3" value="<?php echo isset($general->general_title_en) ? $general->general_title_en:''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="general_content_in" rows="5" ><?php echo isset($general->general_content_in) ? $general->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="general_content_en" rows="5" ><?php echo isset($general->general_content_en) ? $general->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
        	</div>
        </div>

        <!-- CONTENT SALES PAGE -->
        <div class="row" id="content">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/sales/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_in" rows="3"><?php echo isset($sales_content->general_title_in) ? $sales_content->general_title_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_en" rows="3"><?php echo isset($sales_content->general_title_en) ? $sales_content->general_title_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="sales_content_in" rows="5"><?php echo isset($sales_content->general_content_in) ? $sales_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="sales_content_en" rows="5"><?php echo isset($sales_content->general_content_en) ? $sales_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                        <img id="sales_img" class="img-responsive img-thumbnail img-center" src="<?php echo !empty($sales_img->media_url) ? base_url('assets/upload/sales/thumbnail').'/'.$sales_img->media_url : base_url('assets/img/blank_img.png'); ?>" alt="sales">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input onchange="previewImage(this)" type="file" name="sales_img" accept="image/gif, image/x-png, image/jpeg" >
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1750px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- TABLE OF PRODUCT LIST -->
        <div class="row" id="products_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("products_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_product"); ?>" data-toggle="modal" data-target="#add_page"><i class="fa fa-plus"></i> <?php echo $this->lang->line("product"); ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('products_list');?>
                            <table class="table table-hover table-bordered" id="sales">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("name_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("price_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("image_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($products_list as $key => $row):?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->products_name;?></td>
                                        <td>
                                            <b><?php echo $this->lang->line("dimension"); ?></b>
                                            <br> <?php echo $row->products_dimension;?><br>
                                            <b><?php echo $this->lang->line("power"); ?></b>
                                            <br> <?php echo $row->products_power;?><br>
                                            <b><?php echo $this->lang->line("delivery_time"); ?></b><br> <?php echo $row->products_delivery_time;?>
                                        </td>
                                        <td>Rp <?php echo number_format($row->products_price,0,',','.');?></td>
                                        <td><img id="products_image" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/upload/sales/content/thumbnail/'.$row->products_image) ;?>" alt="product"></td>
                                        <td><center>

                                            <a href="#" class="btn btn-warning btn-xs edit_page"
                                            data-target="#edit_page" data-toggle="modal"
                                            title="<?php echo $this->lang->line("edit_product"); ?>"

                                            data-id="<?php echo @$row->products_id; ?>"
                                            data-name="<?php echo $row->products_name;?>"
                                            data-dimension="<?php echo $row->products_dimension;?>"
                                            data-power="<?php echo $row->products_power;?>"
                                            data-delivery="<?php echo $row->products_delivery_time;?>"
                                            data-price="Rp <?php echo number_format($row->products_price,0,',','.');?>"
                                            data-img="<?php echo base_url('assets/upload/sales/content/thumbnail/'.$row->products_image); ?>"

                                            ><i class="fa fa-edit"></i> <?php echo $this->lang->line("edit_product"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/sales/delete_product/'.@$row->products_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_product"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- CONTENT EXPLIOTATION AND RENTAL -->
        <div class="row" id="rental">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/sales/edit_rental');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("rental"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_rental');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="rental_content_in" rows="5"><?php echo isset($rental_content->general_content_in) ? $rental_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="rental_content_en" rows="5"><?php echo isset($rental_content->general_content_en) ? $rental_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                        <img id="rental_img" class="img-responsive img-thumbnail img-center" src="<?php echo !empty($rental_image->media_url) ? base_url('assets/upload/sales/thumbnail').'/'.$rental_image->media_url : base_url('assets/img/blank_img.png'); ?>" alt="rental">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input onchange="previewRental(this)" type="file" name="rental_img" accept="image/gif, image/x-png, image/jpeg">
                                                <p>
                                                    <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_1750px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div> 
        </div>

        <!-- MODAL ADD NEW PRODUCT -->
        <div class="modal fade" id="add_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_form" action="<?php echo base_url('backend/sales/add_product');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("add_product"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="products_name" id="products_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("dimension"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="products_dimension" id="products_dimension" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("power"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="products_power" id="products_power" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("delivery_time"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  name="products_delivery_time" id="products_delivery_time" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("price"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  name="products_price" id="products_price" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-6">

                                    <img id="content_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_img.png'); ?>" alt="product">

                                    <input onchange="previewModal(this)" type="file" name="content_img" accept="image/gif, image/x-png, image/jpeg" required="">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1000px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL EDIT PRODUCT -->
        <div class="modal fade" id="edit_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="edit_form" action="<?php echo base_url('backend/sales/edit_product');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("edit_product"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control products_name" name="products_name" id="products_name" required>
                                    <input type="hidden" class="form-control products_id" name="products_id" value="<?php echo $row->products_id;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("dimension"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control products_dimension" name="products_dimension" id="products_dimension" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("power"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control products_power" name="products_power" id="products_power" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("delivery_time"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control products_delivery_time"  name="products_delivery_time" id="products_delivery_time" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("price"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control products_price" name="products_price" id="products_edit" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-6">

                                    <img id="edit_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_img.png'); ?>" alt="product">

                                    <input onchange="previewModal(this)" class="edit_img" type="file" name="edit_img" accept="image/gif, image/x-png, image/jpeg">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1000px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>