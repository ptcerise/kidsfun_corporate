<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("merchants"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("merchants"); ?> </li>
        </ol>
    </section>

    <!-- Main content -->

    <section class="content">
    <!-- CONTENT MERCHAANTS PAGE -->
        <div class="row">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/merchants/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_in" rows="3" ><?php echo isset($merchants_content->general_title_in) ? $merchants_content->general_title_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("sub_title_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="sub_title_en" rows="3" ><?php echo isset($merchants_content->general_title_en) ? $merchants_content->general_title_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="merchants_content_in" rows="5" ><?php echo isset($merchants_content->general_content_in) ? $merchants_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="merchants_content_en" rows="5" ><?php echo isset($merchants_content->general_content_en) ? $merchants_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                        <img id="merchants_img" class="img-responsive img-thumbnail img-center" src="<?php echo !empty($merchants_img->media_url) ? base_url('assets/upload/merchants/thumbnail').'/'.$merchants_img->media_url : base_url('assets/img/blank_img.png'); ?>" alt="about">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input onchange="previewImage(this)" type="file" name="merchants_img" accept="image/gif, image/x-png, image/jpeg">
                                                <p><small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("image_1750px"); ?> </small>
                                                <br>
                                                <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- TABLE OF MERCHANT LIST -->
        <div class="row" id="merchants_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("merchants_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_merchant"); ?>" data-toggle="modal" data-target="#add_merchant"><i class="fa fa-plus"></i> <?php echo $this->lang->line("merchants"); ?></a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('merchants_list');?>
                            <table class="table table-hover table-bordered merchants" id="merchants">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("name_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("address_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("city_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_table_in"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("desc_table_en"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("image_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($merchants_list as $key => $row):?>
                                        <?php $char_en = strlen($row->merchants_desc_en); ?>
                                        <?php $char_in = strlen($row->merchants_desc_in); ?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->merchants_name;?></td>
                                        <td><?php echo $row->merchants_address;?></td>
                                        <td><?php echo $row->merchants_city;?></td>
                                        <td><?php echo substr($row->merchants_desc_in,0,150);?>
                                            <?php if($char_in>150) {echo "(...)";} elseif($char_in<=150) {echo "";} ?>
                                        </td>
                                        <td><?php echo substr($row->merchants_desc_en,0,150);?>
                                            <?php if($char_en>150) {echo "(...)";} elseif($char_en<=150) {echo "";} ?>
                                        </td>
                                        <td><img id="merchants_image" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/upload/merchants/content/thumbnail/'.$row->merchants_image) ;?>" alt="merchants"></td>
                                        <td><center>

                                            <a href="#" class="btn btn-warning btn-xs edit_merchant"
                                            data-target="#edit_merchant" data-toggle="modal"
                                            title="<?php echo $this->lang->line("edit_merchants"); ?>"

                                            data-id="<?php echo @$row->merchants_id; ?>"
                                            data-name="<?php echo $row->merchants_name;?>"
                                            data-address="<?php echo $row->merchants_address;?>"
                                            data-phone="<?php echo $row->merchants_phone;?>"
                                            data-city="<?php echo $row->merchants_city;?>"
                                            data-descin="<?php echo $row->merchants_desc_in;?>"
                                            data-descen="<?php echo $row->merchants_desc_en;?>"
                                            data-img="<?php echo base_url('assets/upload/merchants/content/thumbnail/'.$row->merchants_image) ;?>"

                                            ><i class="fa fa-edit"></i> <?php echo $this->lang->line("edit_merchant"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/merchants/delete_merchant/'.@$row->merchants_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_merchants"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- MODAL ADD NEW MERCHANT -->
        <div class="modal fade" id="add_merchant" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_form" action="<?php echo base_url('backend/merchants/add_merchant');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("add_merchant"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="merchants_name" id="merchants_name" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("phone"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="merchants_phone" id="merchants_phone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("address"); ?></label>
                                <div class="col-md-6">
                                    <textarea class="form-control textarea" rows="5" name="merchants_address"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("city"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control"  name="merchants_city" id="merchants_city" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_in"); ?></label>
                                <div class="col-md-6">
                                    <textarea class="form-control textarea" rows="5" name="merchants_desc_in"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_en"); ?></label>
                                <div class="col-md-6">
                                    <textarea class="form-control textarea" rows="5" name="merchants_desc_en"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-6">

                                    <img id="content_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_large.png'); ?>" alt="merchant">

                                    <input onchange="previewModal(this)" type="file" name="content_img" accept="image/gif, image/x-png, image/jpeg" required="">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1250px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL EDIT MERCHANT -->
        <div class="modal fade" id="edit_merchant" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="edit_form" action="<?php echo base_url('backend/merchants/edit_merchant');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("edit_merchant"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("name"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control merchants_name" name="merchants_name" id="merchants_name" required>
                                    <input type="hidden" class="form-control merchants_id" name="merchants_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("phone"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control merchants_phone" name="merchants_phone" id="merchants_phone" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("address"); ?></label>
                                <div class="col-md-6">
                                    <textarea id="merchants_address" class="form-control textarea merchants_address" rows="5" name="merchants_address"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("city"); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control merchants_city"  name="merchants_city" id="merchants_city" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_in"); ?></label>
                                <div class="col-md-6">
                                    <textarea id="merchants_desc_in" class="form-control textarea merchants_desc_in" rows="5" name="merchants_desc_in"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("desc_en"); ?></label>
                                <div class="col-md-6">
                                    <textarea id="merchants_desc_en" class="form-control textarea merchants_desc_en" rows="5" name="merchants_desc_en"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-6">

                                    <img id="edit_img" class="img-responsive img-thumbnail img-center" src="" alt="merchant">

                                    <input onchange="previewModal(this)" type="file" name="edit_img" accept="image/gif, image/x-png, image/jpeg">
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1250px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>