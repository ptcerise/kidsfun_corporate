<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $this->lang->line("home"); ?> </h1>
        <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php echo $this->lang->line("home"); ?> </li>
        </ol>
    </section>

    <!-- Main content -->

    <!-- CONTENT HOME -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/home/edit_content');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("content"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_content');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("title_in"); ?></p></label>
                                                <input type="text" class="form-control" name="home_title_in" rows="5" value="<?php echo isset($home_content->general_title_in) ? $home_content->general_title_in:''; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("title_en"); ?></p></label>
                                                <input type="text" class="form-control" name="home_title_en" rows="5" value="<?php echo isset($home_content->general_title_en) ? $home_content->general_title_en:''; ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="home_content_in" rows="5" required><?php echo isset($home_content->general_content_in) ? $home_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="home_content_en" rows="5" required><?php echo isset($home_content->general_content_en) ? $home_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div> 
        </div>

        <!-- TABLE OF MENU LIST -->
        <div class="row" id="menu_list">
            <div class="col-md-12">
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("menu_list"); ?>
                            <div class="caption pull-right">
                                <a href="#" class="page btn btn-success btn-sm btn-add" title="<?php echo $this->lang->line("add_menu"); ?>" data-toggle="modal" data-target="#add_page"><i class="fa fa-plus"></i> Menu</a>
                            </div>
                        </div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('menu_list');?>
                            <table class="table table-hover table-bordered menu" id="menu">
                                <thead>
                                    <tr>
                                        <th class="col-sm-2">No</th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("title_table_in"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("title_table_en"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("image_table"); ?></th>
                                        <th class="col-sm-2"><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if($menu_list!='') { ?>
                                    <?php foreach($menu_list as $key => $row):?>
                                    <tr>
                                        <td><?php echo $key+1;?></td>
                                        <td><?php echo $row->media_title_in;?></td>
                                        <td><?php echo $row->media_title_en;?></td>
                                        <td>
                                            <img id="menu_image" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/upload/home/thumbnail/'.$row->media_url) ;?>" alt="menu">
                                        </td>
                                        <td><center>

                                            <a href="#" class="btn btn-warning btn-xs edit_page"
                                            data-target="#edit_page" data-toggle="modal"
                                            title="<?php echo $this->lang->line("edit_menu"); ?>"

                                            data-id="<?php echo @$row->media_id; ?>"
                                            data-titlein="<?php echo $row->media_title_in;?>"
                                            data-titleen="<?php echo $row->media_title_en;?>"
                                            data-img="<?php echo base_url('assets/upload/home/thumbnail/'.$row->media_url); ?>"

                                            ><i class="fa fa-edit"></i> <?php echo $this->lang->line("edit_menu"); ?></a>

                                            <a onClick="return confirm('<?php echo $this->lang->line("confirm"); ?>');" href="<?php echo base_url('backend/home/delete_menu/'.@$row->media_id);?>" class="btn btn-danger btn-xs" title="<?php echo $this->lang->line("delete_menu"); ?>"><i class="fa fa-trash-o"> </i></a>
                                        </center></td>
                                    </tr>
                                    <?php endforeach;?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- CONTENT ABOUT US -->
        <div class="row" id="about_us">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" action="<?php echo base_url('backend/home/edit_about_us');?>" class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading"><?php echo $this->lang->line("about_us"); ?></div>
                        <div class="panel-body">
                        <?php echo $this->session->flashdata('info_about_us');?>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_in"); ?></p></label>
                                                <textarea class="form-control textarea" name="about_us_content_in" rows="5" required><?php echo isset($about_us_content->general_content_in) ? $about_us_content->general_content_in:''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label class="control-label"><p><?php echo $this->lang->line("content_en"); ?></p></label>
                                                <textarea class="form-control textarea" name="about_us_content_en" rows="5" required><?php echo isset($about_us_content->general_content_en) ? $about_us_content->general_content_en:''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                        <img id="about_us_img" class="img-responsive img-thumbnail img-center" src="<?php echo !empty($about_us_image->media_url) ? base_url('assets/upload/home/thumbnail').'/'.$about_us_image->media_url : base_url('assets/img/blank_img.png'); ?>" alt="about">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">
                                            <?php echo $this->lang->line("choose_image"); ?>
                                            </label>
                                            <div class="col-md-8">
                                                <input onchange="previewImage(this)" type="file" name="about_us_img" accept="image/gif, image/x-png, image/jpeg">
                                                <p>
                                                    <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("image_1750px"); ?> </small>
                                                    <br>
                                                    <small class="color-red"> <?php echo $this->lang->line("lands_square_alert"); ?> </small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group panel-save">
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("update_data"); ?>"><?php echo $this->lang->line("btn_update"); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div> 
        </div>

        <!-- MODAL ADD NEW MENU -->
        <div class="modal fade" id="add_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="add_form" action="<?php echo base_url('backend/home/add_menu');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("add_menu"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("title_in"); ?></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="media_title_in" id="media_title_in" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("title_en"); ?></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="media_title_en" id="media_title_en" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-8">

                                    <img id="content_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_img.png'); ?>" alt="menu">

                                    <input onchange="previewModal(this)" type="file" name="content_img" accept="image/gif, image/x-png, image/jpeg" required>
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1000px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- MODAL EDIT MENU -->
        <div class="modal fade" id="edit_page" tabindex="-1" role="basic" aria-hidden="true">
            <form method="POST" id="edit_form" action="<?php echo base_url('backend/home/edit_menu');?>" class="form-horizontal" enctype="multipart/form-data" role="form">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title"><?php echo $this->lang->line("edit_menu"); ?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("title_in"); ?></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control media_title_in" name="media_title_in" id="media_title_in" disabled required>
                                    <input type="hidden" class="form-control media_id" name="media_id">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("title_en"); ?></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control media_title_en" name="media_title_en" id="media_title_en" disabled required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo $this->lang->line("image"); ?></label>
                                <div class="col-md-8">

                                    <img id="edit_img" class="img-responsive img-thumbnail img-center" src="<?php echo base_url('assets/img/blank_img.png'); ?>" alt="menu">

                                    <input onchange="previewModal(this)" type="file" name="edit_img" accept="image/gif, image/x-png, image/jpeg" required>
                                    <p>
                                        <small class="color-red"> <?php echo $this->lang->line("file_image"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("image_1000px"); ?> </small>
                                        <br>
                                        <small class="color-red"> <?php echo $this->lang->line("square_alert"); ?> </small>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <center>
                                <button type="submit" class="btn btn-primary" title="<?php echo $this->lang->line("save_data"); ?>"><?php echo $this->lang->line("save"); ?></button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" title="<?php echo $this->lang->line("btn_cancel"); ?>"><?php echo $this->lang->line("cancel"); ?></button>
                            </center>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>