<?php

// MASTER

// NAV
$lang["nav_outlets"]			= "Outlet";
$lang["nav_sales"]				= "Penjualan";
$lang["nav_partners"]			= "Rekanan";
$lang["nav_career"]				= "Karir";
$lang["nav_merchants"]			= "Merchant";
$lang["nav_tickets"]			= "Tiket";
$lang["nav_contact"]			= "Kontak";
$lang["nav_find_us"]			= "Temukan Kami";

// FOOTER
$lang["latest_news"]			= "Berita terbaru";
$lang["site_map"]				= "Peta situs";
$lang["rec_product"]			= "Produk Rekreasi";
$lang["follow_us"]				= "Ikuti kami";
$lang["footer_visitors"]		= "Pengunjung : ";

// BUTTON
$lang["btn_read_more"]			= "Baca lebih lanjut";
$lang["btn_show_more"]			= "Tampilkan lebih banyak";
$lang["btn_learn_more"]			= "Pelajari lebih lanjut";
$lang["btn_contact_us"]			= "Hubungi Kami";
$lang["btn_buy_tickets"]		= "Beli Tiket";
$lang["btn_close"]				= "Keluar";
$lang["btn_send"]				= "Kirim";
$lang["follow_us"]				= "Ikuti kami";

// NOTIFICATION
$lang["send_success"]			= "Pesan Anda berhasil dikirim!";

// =====================================================

$lang["select_town"]			= "Pilih kota";

// HOME
$lang["about_us"]				= "Tentang kami";

// OUTLETS
$lang["find_one"]				= "Temukan didekat anda";
$lang["gallery"]				= "Galeri";

// SALES
$lang["rental"]					= "Exploitation and rental";
$lang["our_products"]			= "Produk kami";
$lang["description"]			= "Deskripsi";
$lang["dimension"]				= "Dimensi:";
$lang["power"]					= "Kekuatan:";
$lang["delivery"]				= "Waktu pengiriman:";
$lang["price"]					= "Harga:";

// PARTNERS
$lang["travel_agents"]			= "Agen perjalanan";
$lang["hotels"]					= "Hotel";

// CONTACT
$lang["contact_name"]			= "Nama";
$lang["contact_email"]			= "Email";
$lang["contact_phone"]			= "Nomor telepon";
$lang["contact_subject"]		= "Subyek";
$lang["contact_message"]		= "Pesan";
