<?php
// sidbar
$lang['home'] 		= 'Beranda';
$lang['outlets']	= 'Outlet';
$lang['sales']		= 'Penjualan';
$lang['partners']	= 'Rekanan';
$lang['career']		= 'Karir';
$lang['merchants']	= 'Merchant';
$lang['contact_us']	= 'Hubungi Kami';
$lang['find_us']	= 'Temukan Kami';
$lang['follow_us']	= 'Ikuti Kami';
$lang['tickets_pack']= 'Ticket & Paket';
$lang['tickets']	= 'Ticket';
$lang['cart']		= 'Keranjang Belanja';
$lang['checkout']	= 'Checkout';
$lang['form']		= 'Formulir';
$lang['user']		= 'Pengguna';

// upload requirement
$lang['image_1750px']  	= '* Ukuran gambar minimal : 1750 x 1000 px';
$lang['image_1250px']  	= '* Ukuran gambar minimal : 1250 x 1000 px';
// ------------------------------------------------------------------
$lang['file_image']  	= '* File Maksimal 2MB';
$lang['file_form']  	= '* File Maksimal 300KB';
$lang['landscape']  	= '* Gambar Harus Landscape Atau Ukuran File Terlalu besar (Maks 2MB)';
$lang['square']  		= '* Gambar Harus Square Atau Ukuran File Terlalu besar (Maks 2MB)';
$lang['portrait']  		= '* Gambar Harus Portrait Atau Ukuran File Terlalu besar (Maks 2MB)';
$lang['lands_square']	= '* Gambar Harus Landscape atau Square Atau Ukuran File Terlalu besar (Maks 2MB)';
$lang['landscape_alert']= '* Gambar Harus Landscape ';
$lang['square_alert']  	= '* Gambar Harus Square ';
$lang['portrait_alert'] = '* Gambar Harus Portrait ';
$lang['lands_square_alert']= '* Gambar Harus Landscape atau Square ';
$lang['form_allowed'] 	= '* Hanya file .doc $ .docx yang diperbolehkan ';

// Table
$lang['name_table']			= 'Nama';
$lang['category_table']		= 'Kategori';
$lang['title_table_en'] 	= 'Judul English';
$lang['title_table_in']		= 'Judul Indonesia';
$lang['content_table_en']	= 'Konten English';
$lang['content_table_in']	= 'Konten Indonesia';
$lang['desc_table_en']		= 'Deskripsi English';
$lang['desc_table_in']		= 'Deskripsi Indonesia';
$lang['desc']				= 'Deskripsi';
$lang['date']				= 'Tanggal';
$lang['username_table'] 	= 'Nama Pengguna';
$lang['fullname_table'] 	= 'Nama Lengkap';
$lang['price_table'] 		= 'Harga';
$lang['userlevel_table'] 	= 'Level User';
$lang['phone_table'] 		= 'Nomor Telepon';
$lang['address_table'] 		= 'Alamat';
$lang['city_table'] 		= 'Kota';
$lang['image_table'] 		= 'Gambar';
$lang['dimension_table'] 	= 'Dimensi';
$lang['power_table'] 		= 'Kekuatan';
$lang['delivery_table'] 	= 'Waktu pengiriman';

// header
$lang['profile']		= 'Profil';
$lang['sign_out']		= 'Keluar';

// btn
$lang['btn_edit'] 		= 'Ubah';
$lang['btn_detail'] 	= 'Detil';
$lang['btn_cancel'] 	= 'Batal';
$lang['btn_add'] 		= 'Tambah';
$lang['btn_update'] 	= 'Perbaharui';
$lang['btn_delete'] 	= 'Hapus';
$lang['btn_save'] 		= 'Simpan';
$lang['btn_close'] 		= 'Keluar';
$lang['btn_upload'] 	= 'Unggah';
$lang['edit'] 		= 'Ubah';
$lang['detail'] 	= 'Detil';
$lang['cancel'] 	= 'Batal';
$lang['add'] 		= 'Tambah';
$lang['update'] 	= 'Data Berhasil di Pembaharui';
$lang['delete'] 	= 'Data Berhasil di Hapus';
$lang['save'] 		= 'Simpan';
//hover
$lang['update_data'] 	= 'Perbarui Data';
$lang['save_data'] 		= 'Simpan Data';
$lang['view'] 			= 'Lihat Detil';

// notif
$lang['edit'] 			= 'Ubah';
$lang['detail'] 		= 'Detil';
$lang['cancel'] 		= 'Batal';
$lang['insert'] 		= 'Data Berhasil ditambah';
$lang['update'] 		= 'Data Berhasil diperbarui';
$lang['delete'] 		= 'Data Berhasil dihapus';
$lang['update_pass']	= 'Sandi Berhasil diperbarui';
$lang['wrong_pass'] 	= 'Sandi Lama Salah!';
$lang['save'] 			= 'Simpan';
$lang['user_check'] 	= 'Nama sudah ada! Silakan coba nama lain';
$lang['pass_check'] 	= 'Kata sandi harus sama!';
$lang['update_profile']	= 'Profil anda berhasil diubah!';
$lang['update_password']	= 'Sandi anda berhasil diubah. Silakan login kembali!';
$lang['confirm']		= 'Apakah anda yakin ingin menghapus data ini?';
$lang['confirm_profile']= 'Apakah anda yakin ingin memperbarui profil anda?';
$lang['upload_error']		= 'Terjadi eror saat upload file';

// content
$lang['action']			= 'Aksi';
$lang['language'] 		= 'Bahasa';
$lang['image'] 			= 'Gambar :';
$lang['content'] 		= 'Konten';
$lang['title_en'] 		= 'Judul English :';
$lang['title_in']		= 'Judul Indonesia :';
$lang['content_en'] 	= 'Konten English :';
$lang['content_in'] 	= 'Konten Indonesia :';
$lang['caption_en'] 	= 'Judul English :';
$lang['caption_in'] 	= 'Judul Indonesia :';
$lang['sub_title_en'] 	= 'Sub-judul English :';
$lang['sub_title_in'] 	= 'Sub-judul Indonesia :';
$lang['choose_file']	= 'Pilih FIle :';
$lang['choose_video']	= 'Pilih Video :';
$lang['choose_image']	= 'Pilih Gambar :';
$lang['choose_date']	= 'Pilih Tanggal :';
$lang['background']		= 'Latarbalakang';
$lang['image_caption']	= 'Judul Gambar';
$lang['caption']		= 'Judul';
$lang['divider']		= 'Pembatas';
$lang['title']			= 'Judul';
$lang['image_size']		= 'Ukuran gambar :';
$lang['category']		= 'Kategori :';
$lang['name']			= 'Nama :';
$lang['city']			= 'City :';
$lang['dividers1']    = 'Batas 1';
$lang['dividers3']    = 'Batas 2';
$lang['dividers2']    = 'Batas 3';
$lang['image1']       = 'Gambar 1';
$lang['image2']       = 'Gambar 2';
$lang['image3']       = 'Gambar 3';
$lang['general_content']= 'Konten Umum';
$lang['dimension']		= 'Dimensi :';
$lang['power']			= 'Kekuatan :';
$lang['delivery_time']	= 'Waktu Pengiriman :';
$lang['price']			= 'Harga :';
$lang['desc_in']		= 'Deskripsi Indonesia :';
$lang['desc_en']		= 'Deskripsi English :';
$lang['open_time']		= 'Waktu Buka :';

//--------------------MAIN MENU--------------------------
//Profile
$lang['account_setting'] 	= 'Pengaturan Akun';
$lang['profile'] 			= 'Profil';
$lang['pass'] 				= 'Sandi';
$lang['sign_out'] 			= 'Keluar';

// Home
$lang['about_us'] = 'Tentang Kami';
$lang['menu_list'] 	= 'Daftar Menu';
$lang['add_menu'] 	= 'Tambah Menu';
$lang['edit_menu'] 	= 'Ubah Menu';
$lang['delete_menu'] 	= 'Hapus Menu';

// Outlets
$lang['outlets_list'] = 'Daftar Outlet';
$lang['add_outlets'] = 'Tambah Outlet';
$lang['edit_outlets'] = 'Ubah Outlet';
$lang['delete_outlets'] = 'Hapus Outlet';
$lang['gallery'] = 'Galeri';
$lang['add_gallery'] = 'Tambah Galeri';
$lang['edit_gallery'] = 'Ubah Galeri';
$lang['delete_gallery'] = 'Hapus Galeri';
$lang['rental'] = 'Exploitation and rental';

// Sales
$lang['products_list']	= 'Daftar Produk';
$lang['add_product'] 	= 'Tambah Produk';
$lang['edit_product'] 	= 'Ubah Produk';
$lang['delete_product'] = 'Hapus Produk';
$lang['product'] 		= 'Produk';

// Partners
$lang['partners_list'] = 'Daftar Rekan';
$lang['add_partners'] = 'Tambah Rekan';
$lang['edit_partners'] = 'Ubah Rekan';
$lang['delete_partners'] = 'Hapus Rekan';
$lang['choose_category'] = 'Pilih kategori';

// Career
$lang['job_list']	= 'Daftar Pekerjaan';
$lang['add_job'] 	= 'Tambah Pekerjaan';
$lang['edit_job'] 	= 'Ubah Pekerjaan';
$lang['delete_job'] = 'Hapus Pekerjaan';
$lang['job']	= 'Pekerjaan';

// Merchants
$lang['merchants_list']	= 'Daftar Merchant';
$lang['add_merchant'] 	= 'Tambah Merchant';
$lang['edit_merchant'] 	= 'Ubah Merchant';
$lang['delete_merchant'] = 'Hapus Merchant';

// Contact us
$lang['address']  	= 'Alamat';
$lang['phone']		= 'Nomor Telepon';
$lang['message']  	= 'Pesan';
$lang['sender']  	= 'Pengirim';
$lang['subject']  	= 'Subyek';
$lang['detail_msg'] = 'Detil Pesan';
$lang['from']  		= 'Dari';
$lang['delete_message']  = 'Hapus Pesan';
$lang['detail_message']  = 'Detil Pesan';
$lang['message_received']  = 'Pesan diterima';
$lang['status']			= 'Status';

//follow us
$lang['links']         = 'Tautan';

// user
$lang['username'] 		= 'Nama Pengguna :';
$lang['fullname'] 		= 'Nama Lengkap :';
$lang['user_level'] 	= 'Level User :';
$lang['password'] 		= 'Kata Sandi :';
$lang['current_pass'] 	= 'Sandi Lama :';
$lang['new_pass'] 		= 'Sandi Baru :';
$lang['confirm_pass'] 	= 'Konfirmasi Sandi :';
$lang['user_list'] 		= 'Daftar Pengguna';
$lang['new_user'] 		= 'Pengguna Baru';
$lang['add_user']       = 'Tambah Pengguna';
$lang['edit_user']  	= 'Ubah Pengguna';
$lang['delete_user']  	= 'Hapus Pengguna';
$lang['choose_level']  	= 'Pilih level user';
$lang['admin_confirm']	= 'Konfirmasi Admin';