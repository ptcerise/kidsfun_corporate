<?php

//# MASTER
// NAV
$lang["nav_outlets"]			= "Outlets";
$lang["nav_sales"]				= "Sales";
$lang["nav_partners"]			= "Partners";
$lang["nav_career"]				= "Career";
$lang["nav_merchants"]			= "Merchants";
$lang["nav_tickets"]			= "Tickets";
$lang["nav_contact"]			= "Contact";
$lang["nav_find_us"]			= "Find Us";

// FOOTER
$lang["latest_news"]			= "Latest news";
$lang["site_map"]				= "Site Map";
$lang["rec_product"]			= "Produk Rekreasi";
$lang["follow_us"]				= "Follow us";
$lang["footer_visitors"]		= "Visitors : ";

// BUTTON
$lang["btn_read_more"]			= "Read more";
$lang["btn_show_more"]			= "Show more";
$lang["btn_learn_more"]			= "Learn more";
$lang["btn_contact_us"]			= "Contact Us";
$lang["btn_buy_tickets"]		= "Buy Tickets";
$lang["btn_close"]				= "Close";
$lang["btn_send"]				= "Send";
$lang["follow_us"]				= "Follow us";

// NOTIFICATION
$lang["send_success"]			= "Your message sent successfully!";

// =====================================================

$lang["select_town"]			= "Select a town";

// HOME
$lang["about_us"]				= "About us";

// OUTLETS
$lang["find_one"]				= "Find one near you";
$lang["gallery"]				= "Gallery";

// SALES
$lang["rental"]					= "Exploitation and rental";
$lang["our_products"]			= "Our products";
$lang["description"]			= "Description";
$lang["dimension"]				= "Dimension:";
$lang["power"]					= "Power:";
$lang["delivery"]				= "Delivery time:";
$lang["price"]					= "Price:";

// PARTNERS
$lang["travel_agents"]			= "Travel agents";
$lang["hotels"]					= "Hotels";

// CONTACT
$lang["contact_name"]			= "Name";
$lang["contact_email"]			= "Email";
$lang["contact_phone"]			= "Phone Number";
$lang["contact_subject"]		= "Subject";
$lang["contact_message"]		= "Message";