<?php
// sidbar
$lang['home'] 		= 'Home';
$lang['outlets']	= 'Outlets';
$lang['sales']		= 'Sales';
$lang['partners']	= 'Partners';
$lang['career']		= 'Career';
$lang['merchants']	= 'Merchants';
$lang['contact_us']	= 'Contact';
$lang['find_us']	= 'Find Us';
$lang['follow_us']	= 'Follow Us';
$lang['tickets_pack']= 'Tickets & Package';
$lang['tickets']	= 'Tickets';
$lang['cart']		= 'Shopping cart';
$lang['checkout']	= 'Checkout';
$lang['form']		= 'Form';
$lang['user']		= 'User';

// upload image
$lang['image_1750px']  	= '* Minimum Image size : 1750 x 1000 px';
$lang['image_1250px']  	= '* Minimum Image size : 1250 x 1000 px';
$lang['image_1000px']  	= '* Minimum Image size : 1000 x 1000 px';
//-----------------------------------------------------------------
$lang['file_image']  	= '* Maximum file 2MB';
$lang['file_form']  	= '* Maximum file 300KB';
$lang['landscape']  	= '* Image must be Landscape or File size to Large (Max 2MB) ';
$lang['square']  		= '* Image must be Square or File size to Large (Max 2MB) ';
$lang['portrait']  		= '* Image must be Portrait or File size to Large (Max 2MB) ';
$lang['lands_square']	= '* Image must be Landscape or Square or File size to Large (Max 2MB) ';
$lang['landscape_alert']= '* Image must be Landscape ';
$lang['square_alert']  	= '* Image must be Square ';
$lang['portrait_alert'] = '* Image must be Portrait ';
$lang['lands_square_alert']= '* Image must be Landscape or Square ';
$lang['form_allowed'] 	= '* Only .doc $ .docx files are allowed ';

// Table
$lang['name_table']			= 'Name';
$lang['category_table']		= 'Category';
$lang['title_table_en'] 	= 'Title English';
$lang['title_table_in']		= 'Title Indonesia';
$lang['content_table_en']	= 'Content English';
$lang['content_table_in']	= 'Content Indonesia';
$lang['desc_table_en']		= 'Description English';
$lang['desc_table_in']		= 'Description Indonesia';
$lang['desc']				= 'Description';
$lang['date']				= 'Date';
$lang['username_table'] 	= 'Username';
$lang['fullname_table'] 	= 'Fullname';
$lang['price_table'] 		= 'Price';
$lang['userlevel_table'] 	= 'Level User';
$lang['phone_table'] 		= 'Phone Number';
$lang['address_table'] 		= 'Address';
$lang['city_table'] 		= 'City';
$lang['image_table'] 		= 'Image';
$lang['dimension_table'] 	= 'Dimension';
$lang['power_table'] 		= 'Power';
$lang['delivery_table'] 	= 'Delivery Time';

// header
$lang['profile']		= 'Profile';
$lang['sign_out']		= 'Sign Out';

// btn
$lang['btn_edit'] 		= 'Edit';
$lang['btn_detail'] 	= 'Detail';
$lang['btn_cancel'] 	= 'Cancel';
$lang['btn_add'] 		= 'Add';
$lang['btn_update'] 	= 'Update';
$lang['btn_delete'] 	= 'Delete';
$lang['btn_save'] 		= 'Save';
$lang['btn_close'] 		= 'Close';
$lang['btn_upload'] 	= 'Upload';
$lang['edit'] 		= 'Edit';
$lang['detail'] 	= 'Detail';
$lang['cancel'] 	= 'Cancel';
$lang['add'] 		= 'Add';
$lang['update'] 	= 'Update Successfully ';
$lang['delete'] 	= 'Delete Successfully';
$lang['save'] 		= 'Save';
//hover
$lang['update_data'] 	= 'Update Data';
$lang['save_data'] 		= 'Save Data';
$lang['view'] 			= 'View Detail';

//notif
$lang['edit'] 			= 'Edit';
$lang['detail'] 		= 'Detail';
$lang['cancel'] 		= 'Cancel';
$lang['insert'] 		= 'Insert Data Successful';
$lang['update'] 		= 'Update Data Successful';
$lang['delete'] 		= 'Delete Data Successful';
$lang['update_pass']	= 'Update Password Successful';
$lang['wrong_pass'] 	= 'Wrong Current Password!';
$lang['save'] 			= 'Save';
$lang['user_check'] 	= 'Username already exist! Please try another username';
$lang['pass_check'] 	= 'Password must be match!';
$lang['update_profile']	= 'Your profile has been changed!';
$lang['update_password']	= 'Your profile has been changed. Please re-login!';
$lang['confirm']		= 'Are you sure to delete this?';
$lang['confirm_profile']= 'Are you sure to update your profile?';
$lang['upload_error']		= 'An error occurs when uploading files';

// content
$lang['action']			= 'Action';
$lang['language'] 		= 'Language';
$lang['image'] 			= 'Image :';
$lang['content'] 		= 'Content';
$lang['title_en'] 		= 'Title English :';
$lang['title_in']		= 'Title Indonesia :';
$lang['content_en'] 	= 'Content English :';
$lang['content_in'] 	= 'Content Indonesia :';
$lang['caption_en'] 	= 'Caption English :';
$lang['caption_in'] 	= 'Caption Indonesia :';
$lang['sub_title_en'] 	= 'Sub-title English :';
$lang['sub_title_in'] 	= 'Sub-title Indonesia :';
$lang['choose_file']	= 'Choose File :';
$lang['choose_video']	= 'Choose Video :';
$lang['choose_image'] 	= 'Choose Picture :';
$lang['choose_date']	= 'Choose Date :';
$lang['background']		= 'Background';
$lang['image_caption']	= 'Image Caption';
$lang['caption']		= 'Caption';
$lang['divider']		= 'Divider';
$lang['title']			= 'Title';
$lang['image_size']		= 'Image size :';
$lang['category']		= 'Category :';
$lang['name']			= 'Name :';
$lang['city']			= 'City :';
$lang['dividers1']   = 'Divider 1';
$lang['dividers3']   = 'Divider 2';
$lang['dividers2']   = 'Divider 3';
$lang['image1']       = 'Image 1';
$lang['image2']       = 'Image 2';
$lang['image3']       = 'Image 3';
$lang['general_content']= 'General Content';
$lang['dimension']		= 'Dimension :';
$lang['power']			= 'Power :';
$lang['delivery_time']	= 'Delivery Time :';
$lang['price']			= 'Price :';
$lang['desc_in']		= 'Description Indonesia :';
$lang['desc_en']		= 'Description English :';
$lang['open_time']		= 'Opening Time :';


//--------------------MAIN MENU--------------------------
//Profile
$lang['account_setting'] 	= 'Account Setting';
$lang['profile'] 			= 'Profile';
$lang['pass'] 				= 'Password';
$lang['sign_out'] 			= 'Sign Out';

// Home
$lang['about_us'] 	= 'About Us';
$lang['menu_list'] 	= 'List of Menu';
$lang['add_menu'] 	= 'Add Menu';
$lang['edit_menu'] 	= 'Edit Menu';
$lang['delete_menu'] 	= 'Delete Menu';


// Outlets
$lang['outlets_list'] = 'List of Outlets';
$lang['add_outlets'] = 'Add Outlet';
$lang['edit_outlets'] = 'Edit Outlet';
$lang['delete_outlets'] = 'Delete Outlet';
$lang['gallery'] = 'Gallery';
$lang['add_gallery'] = 'Add Gallery';
$lang['edit_gallery'] = 'Edit Gallery';
$lang['delete_gallery'] = 'Delete Gallery';
$lang['rental'] = 'Exploitation and rental';

// Sales
$lang['products_list'] = 'List of Products';
$lang['add_product'] = 'Add Product';
$lang['edit_product'] = 'Edit Product';
$lang['delete_product'] = 'Delete Product';
$lang['product'] = 'Product';

// Partners
$lang['partners_list'] = 'List of Partners';
$lang['add_partners'] = 'Add Partners';
$lang['edit_partners'] = 'Edit Partners';
$lang['delete_partners'] = 'Delete Partners';
$lang['choose_category'] = 'Choose category';

// Career
$lang['job_list']	= 'List of Jobs';
$lang['add_job'] 	= 'Add Job';
$lang['edit_job'] 	= 'Edit Job';
$lang['delete_job'] = 'Delete Job';
$lang['job']	= 'Job';

// Merchants
$lang['merchants_list']	= 'List of Merchants';
$lang['add_merchant'] 	= 'Add Merchant';
$lang['edit_merchant'] 	= 'Edit Merchant';
$lang['delete_merchant'] = 'Delete Merchant';

// Contact us
$lang['address']  	= 'Address :';
$lang['phone']  	= 'Phone Number :';
$lang['message']  	= 'Message';
$lang['sender']  	= 'Sender';
$lang['subject']  	= 'Subject';
$lang['detail_msg'] = 'Message Detail';
$lang['from']  		= 'From';
$lang['delete_message']  = 'Delete Message';
$lang['detail_message']  = 'Message Detail';
$lang['message_received']  = 'Message(s) received';
$lang['status']			= 'Status';

//follow us
$lang['links']         = 'Links';

// user
$lang['username'] 		= 'Username :';
$lang['fullname'] 		= 'Fullname :';
$lang['user_level'] 	= 'User Level :';
$lang['password'] 		= 'Password :';
$lang['current_pass'] 	= 'Current Password :';
$lang['new_pass'] 		= 'New Password :';
$lang['confirm_pass'] 	= 'Confirm Password :';
$lang['user_list'] 		= 'List of Users';
$lang['new_user'] 		= 'New User';
$lang['add_user']       = 'Add User';
$lang['edit_user']  	= 'Edit User';
$lang['delete_user']  	= 'Delete User';
$lang['choose_level']  	= 'Choose user level';
$lang['admin_confirm']	= 'Admin Confirmation';