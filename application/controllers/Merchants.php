<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchants extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('kidsfun_frontend',$fe_lang);
        } else {
            $this->lang->load('kidsfun_frontend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['merchants_content'] = $this->Access->readtable('general','',array('general_page'=>'Merchants', 'general_section'=>'merchants_content'))->row();
        $data['merchants_img'] = $this->Access->readtable('media','',array('media_page'=>'Merchants', 'media_section'=>'merchants_image'))->row();
        $data['merchants_list'] = $this->Access->readtable('merchants','')->result();
        $data['merchants_city'] = $this->db->query("SELECT merchants_city FROM merchants GROUP BY merchants_city")->result();
        $view['content']   = $this->load->view('corporate/v_merchants',$data,TRUE);

        // LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        // CONTACT US ON FOOTER
        $view['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'Contact_us'))->row()->general_content_en;
        $view['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Contact_us'))->row()->general_content_en;
        $view['email'] = $this->Access->readtable('general','',array('general_section'=>'email','general_page'=>'Contact_us'))->row()->general_content_en;

        $this->load->view('corporate/v_master',$view);
    }

    // function cek(){
    //     session_destroy();
    // }
}
