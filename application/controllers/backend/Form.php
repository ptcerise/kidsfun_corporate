<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['current'] = "form";
        $data['check_travel'] = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_travel'))->row();
        $data['check_hotel'] = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_hotel'))->row();
        $data['check_career'] = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_career'))->row();
        $data['check_merchant'] = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_merchant'))->row();

        $view['content'] = $this->load->view('backend/form/v_form',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // function form_travel()
    // {
    //     if(isset($_FILES['form_travel'])){
    //         $allowedExt = array("doc","docx");
    //         $allowedMime = array('application/msword');

    //         $form_travel = $_FILES['form_travel']['name'];
    //         $file_size = $_FILES['form_travel']['size'];
    //         $file_type = $_FILES['form_travel']['type'];

    //         $break = explode('.', $form_travel);
    //         $ext = $break[count($break) - 1];
    //         $date = date('dmYHis');
    //         $name_url = 'form_travel'.'.'.$ext;
    //         $path = './assets/upload/form_travel';

    //         if(!file_exists($path))
    //         {
    //             $create = mkdir($path, 0777, TRUE);
    //             if(!$create)
    //                 return;
    //         }

    //         if (300000 < $file_size) {
    //             die( 'Please provide a smaller file' );
    //         }

    //         if (!(in_array($ext, $allowedExt ))){
    //             die('Please provide another file type');
    //         }

    //         if (in_array($file_type, $allowedMime)) 
    //         {      
    //             move_uploaded_file($name_url, "./assets/upload/form_travel");
    //             echo "success"; 
    //         }
    //         else
    //         {
    //             die('Please provide another file.');
    //         }
    //     }
    // }

    function form_travel()
    {
        if(array_key_exists('form_travel', $_FILES)) {
            
            $file       = $_FILES['form_travel']['name'];
            $file_size  = $_FILES['form_travel']['size'];
            $file_error = $_FILES['form_travel']['error'];
            $tmp_name   = $_FILES['form_travel']['tmp_name'];

            $break      = explode('.', $file);
            $ext        = $break[count($break) - 1];
            $date       = date('dmYHis');

            $path       = 'assets/upload';
            $target_file= $path .'/'. basename($file);
            $file_type  = pathinfo($target_file,PATHINFO_EXTENSION);
            $file_name  = 'travel_form'.'.'.$ext;

            // Validation file upload
            if($file_size > 300000)
            {
                $error = $this->lang->line("file_form");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_type !== "doc" && $file_type !== "docx")
            {
                $error = $this->lang->line("form_allowed");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_error !== UPLOAD_ERR_OK)
            {
                $error = "Syntax error!";
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else
            { 
                // Proses membuat folder jika belum ada
                if(!file_exists($path)) mkdir('form');
                // Memindahkan file ke direktori tujuan
                if(move_uploaded_file($tmp_name,'assets/upload/form/' . $file_name)) {

                    $check = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_travel'))->row();
                    if($check=='')
                    {
                        $insert = array(
                            'general_title_in' => 'Form Travel agent',
                            'general_page' => 'Form',
                            'general_section' => 'form_travel',
                            'general_url' => $file_name
                        );

                        $this->Access->inserttable('general',$insert);
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                    }
                    else
                    {
                        $old_file = $this->Access->readtable('general','general_url',array('general_section'=>'form_travel','general_page'=>'Form'))->row()->general_url;
                        unlink( realpath( APPPATH.'../assets/upload/form/'.$old_file ));

                        $update = array(
                            'general_url' => $file_name
                        );
                        $this->Access->updatetable('general',$update,array('general_section' => 'form_travel'));
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
                else
                {
                    $error = $this->lang->line("upload_error");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }
        $_SESSION['info_form'] = $notif;
        $this->session->mark_as_flash('info_form');
        redirect('backend/form');
    }

    function form_hotel()
    {
        if(array_key_exists('form_hotel', $_FILES)) {
            
            $file       = $_FILES['form_hotel']['name'];
            $file_size  = $_FILES['form_hotel']['size'];
            $file_error = $_FILES['form_hotel']['error'];
            $tmp_name   = $_FILES['form_hotel']['tmp_name'];

            $break      = explode('.', $file);
            $ext        = $break[count($break) - 1];
            $date       = date('dmYHis');

            $path       = 'assets/upload';
            $target_file= $path .'/'. basename($file);
            $file_type  = pathinfo($target_file,PATHINFO_EXTENSION);
            $file_name  = 'hotel_form'.'.'.$ext;

            // Validation file upload
            if($file_size > 300000)
            {
                $error = $this->lang->line("file_form");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_type !== "doc" && $file_type !== "docx")
            {
                $error = $this->lang->line("form_allowed");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_error !== UPLOAD_ERR_OK)
            {
                $error = "Syntax error!";
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else
            { 
                // Proses membuat folder jika belum ada
                if(!file_exists($path)) mkdir('form');
                // Memindahkan file ke direktori tujuan
                if(move_uploaded_file($tmp_name,'assets/upload/form/' . $file_name)) {

                    $check = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_hotel'))->row();
                    if($check=='')
                    {
                        $insert = array(
                            'general_title_in' => 'Form Hotel',
                            'general_page' => 'Form',
                            'general_section' => 'form_hotel',
                            'general_url' => $file_name
                        );

                        $this->Access->inserttable('general',$insert);
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                    }
                    else
                    {
                        $old_file = $this->Access->readtable('general','general_url',array('general_section'=>'form_hotel','general_page'=>'Form'))->row()->general_url;
                        unlink( realpath( APPPATH.'../assets/upload/form/'.$old_file ));

                        $update = array(
                            'general_url' => $file_name
                        );

                        $this->Access->updatetable('general',$update,array('general_section' => 'form_hotel'));
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
                else
                {
                    $error = $this->lang->line("upload_error");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }
        $_SESSION['info_form'] = $notif;
        $this->session->mark_as_flash('info_form');
        redirect('backend/form');
    }

    function form_career()
    {
        if(array_key_exists('form_career', $_FILES)) {
            
            $file       = $_FILES['form_career']['name'];
            $file_size  = $_FILES['form_career']['size'];
            $file_error = $_FILES['form_career']['error'];
            $tmp_name   = $_FILES['form_career']['tmp_name'];

            $break      = explode('.', $file);
            $ext        = $break[count($break) - 1];
            $date       = date('dmYHis');

            $path       = 'assets/upload';
            $target_file= $path .'/'. basename($file);
            $file_type  = pathinfo($target_file,PATHINFO_EXTENSION);
            $file_name  = 'career_form'.'.'.$ext;

            // Validation file upload
            if($file_size > 300000)
            {
                $error = $this->lang->line("file_form");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_type !== "doc" && $file_type !== "docx")
            {
                $error = $this->lang->line("form_allowed");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_error !== UPLOAD_ERR_OK)
            {
                $error = "Syntax error!";
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else
            { 
                // Proses membuat folder jika belum ada
                if(!file_exists($path)) mkdir('form');
                // Memindahkan file ke direktori tujuan
                if(move_uploaded_file($tmp_name,'assets/upload/form/' . $file_name)) {

                    $check = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_career'))->row();
                    if($check=='')
                    {
                        $insert = array(
                            'general_title_in' => 'Form Career',
                            'general_page' => 'Form',
                            'general_section' => 'form_career',
                            'general_url' => $file_name
                        );

                        $this->Access->inserttable('general',$insert);
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                    }
                    else
                    {
                        $old_file = $this->Access->readtable('general','general_url',array('general_section'=>'form_career','general_page'=>'Form'))->row()->general_url;
                        unlink( realpath( APPPATH.'../assets/upload/form/'.$old_file ));

                        $update = array(
                            'general_url' => $file_name
                        );

                        $this->Access->updatetable('general',$update,array('general_section' => 'form_career'));
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
                else
                {
                    $error = $this->lang->line("upload_error");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }
        $_SESSION['info_form'] = $notif;
        $this->session->mark_as_flash('info_form');
        redirect('backend/form');
    }

    function form_merchant()
    {
        if(array_key_exists('form_merchant', $_FILES)) {
            
            $file       = $_FILES['form_merchant']['name'];
            $file_size  = $_FILES['form_merchant']['size'];
            $file_error = $_FILES['form_merchant']['error'];
            $tmp_name   = $_FILES['form_merchant']['tmp_name'];

            $break      = explode('.', $file);
            $ext        = $break[count($break) - 1];
            $date       = date('dmYHis');

            $path       = 'assets/upload';
            $target_file= $path .'/'. basename($file);
            $file_type  = pathinfo($target_file,PATHINFO_EXTENSION);
            $file_name  = 'merchant_form'.'.'.$ext;

            // Validation file upload
            if($file_size > 300000)
            {
                $error = $this->lang->line("file_form");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_type !== "doc" && $file_type !== "docx")
            {
                $error = $this->lang->line("form_allowed");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else if($file_error !== UPLOAD_ERR_OK)
            {
                $error = "Syntax error!";
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
            }
            else
            { 
                // Proses membuat folder jika belum ada
                if(!file_exists($path)) mkdir('form');
                // Memindahkan file ke direktori tujuan
                if(move_uploaded_file($tmp_name,'assets/upload/form/' . $file_name)) {

                    $check = $this->Access->readtable('general','',array('general_page'=>'Form', 'general_section'=>'form_merchant'))->row();
                    if($check=='')
                    {
                        $insert = array(
                            'general_title_in' => 'Form Merchant',
                            'general_page' => 'Form',
                            'general_section' => 'form_merchant',
                            'general_url' => $file_name
                        );

                        $this->Access->inserttable('general',$insert);
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                    }
                    else
                    {
                        $old_file = $this->Access->readtable('general','general_url',array('general_section'=>'form_merchant','general_page'=>'Form'))->row()->general_url;
                        unlink( realpath( APPPATH.'../assets/upload/form/'.$old_file ));

                        $update = array(
                            'general_url' => $file_name
                        );

                        $this->Access->updatetable('general',$update,array('general_section' => 'form_merchant'));
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
                else
                {
                    $error = $this->lang->line("upload_error");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';
                }
            }
        }
        $_SESSION['info_form'] = $notif;
        $this->session->mark_as_flash('info_form');
        redirect('backend/form');
    }
}