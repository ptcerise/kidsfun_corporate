<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['partners_content'] = $this->Access->readtable('general','',array('general_page'=>'Partners', 'general_section'=>'partners_content'))->row();
        $data['partners_img'] = $this->Access->readtable('media','',array('media_page'=>'Partners', 'media_section'=>'partners_image'))->row();
        $data['partners_list'] = $this->Access->readtable('partners','')->result();

        $data['current'] = "partners";
        $view['script']  = $this->load->view('backend/script/partners','',TRUE);
        $view['content'] = $this->load->view('backend/partners/v_partners',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // UPDATE PARTNERS CONTENT
    function edit_content()
    {
        $partners_img = $_FILES['partners_img']['name'];
        if($partners_img == '')
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Partners', 'general_section'=>'partners_content'))->row();
            if($check == '')
            {
                // INSERT TO DATABASE IF DATA IS NOT SET 
                $add_content = array(
                                    'general_page'=>'Partners',
                                    'general_section'=>'partners_content',
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('partners_content_in'),
                                    'general_content_en' => $this->input->post('partners_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->inserttable('general',$add_content);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("insert");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
            else
            {
                // UPDATE DATABASE IF DATA IS SET
                $edit_content = array(
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('partners_content_in'),
                                    'general_content_en' => $this->input->post('partners_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->updatetable('general',$edit_content,array('general_page'=>'Partners', 'general_section'=>'partners_content'));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
        }
        else
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Partners', 'general_section'=>'partners_content'))->row();
            if($check == '')
            {
                // UPDATE IMAGE + INSERT CONTENT
                $partners_img = $_FILES['partners_img']['name'];
                $break = explode('.', $partners_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'partners_img_'.$date.'.'.$ext;
                $path = './assets/upload/partners';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('partners_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/partners/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/partners/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/partners/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/partners/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'partners_image','media_page'=>'Partners'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/partners/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/cropped/'.$old_image ));

                            $add_content = array(
                                                'general_page'=>'Partners',
                                                'general_section'=>'partners_content',
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('partners_content_in'),
                                                'general_content_en' => $this->input->post('partners_content_en')
                                            );

                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'partners_image','media_page'=>'Partners'));
                            $this->Access->inserttable('general',$add_content);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
            else
            {
                // UPDATE IMAGE + UPDATE CONTENT
                $partners_img = $_FILES['partners_img']['name'];
                $break = explode('.', $partners_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'partners_img_'.$date.'.'.$ext;
                $path = './assets/upload/partners';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('partners_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/partners/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/partners/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/partners/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/partners/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'partners_image','media_page'=>'Partners'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/partners/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/cropped/'.$old_image ));

                            $edit_content = array(
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('partners_content_in'),
                                                'general_content_en' => $this->input->post('partners_content_en')
                                            );

                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'partners_image','media_page'=>'Partners'));
                            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Partners', 'general_section'=>'partners_content'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/partners');
    }

    function add_partner()
    {
        $add_img = $_FILES['add_img']['name'];
        $break = explode('.', $add_img);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'partner_'.$date.'.'.$ext;
        $path = './assets/upload/partners/content';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('add_img') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1250 || $height < 1000 )
            {
                unlink( realpath( APPPATH.'../assets/upload/partners/content/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/partners/content/thumbnail/'.$name_url ));
                $image_1250px = $this->lang->line('image_1250px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1250px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/partners/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/partners/content/thumbnail/'.$name_url ));
                    $lands_square = $this->lang->line('lands_square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1250, 1000);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }

                    $add_partner = array(
                                        'partners_name' => $this->input->post('partners_name'),
                                        'partners_category' => $this->input->post('partners_category'),
                                        'partners_phone' => $this->input->post('partners_phone'),
                                        'partners_address' => $this->input->post('partners_address'),
                                        'partners_city' => $this->input->post('partners_city'),
                                        'partners_image' => $name_url
                                        );
                    $this->db->trans_begin();
                    $this->Access->inserttable('partners',$add_partner);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        $_SESSION['partners_list'] = $notif;
        $this->session->mark_as_flash('partners_list');
        redirect('backend/partners#partners_list');
    }

    function edit_partner()
    {
        $edit_img = $_FILES['edit_img']['name'];
        if($edit_img=='')
        {
            $edit_partner = array(
                            'partners_name' => $this->input->post('partners_name'),
                            'partners_category' => $this->input->post('partners_category'),
                            'partners_phone' => $this->input->post('partners_phone'),
                            'partners_address' => $this->input->post('partners_address'),
                            'partners_city' => $this->input->post('partners_city')
                            );
            $this->db->trans_begin();
            $partners_id = $this->input->post('partners_id');
            $this->Access->updatetable('partners',$edit_partner,array('partners_id'=>$partners_id));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        else
        {
            $break = explode('.', $edit_img);
            $ext = $break[count($break) - 1];
            $date = date('dmYHis');
            $name_url = 'partners_'.$date.'.'.$ext;
            $path = './assets/upload/partners/content';

            if( ! file_exists( $path ) )
            {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
            }
            $this->piclib->get_config($name_url, $path);
            if( $this->upload->do_upload('edit_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1250 || $height < 1000 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/partners/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/partners/content/thumbnail/'.$name_url ));
                    $image_1250px = $this->lang->line('image_1250px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1250px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/partners/content/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/partners/content/thumbnail/'.$name_url ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1250, 1000);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                        }

                        $partners_id = $this->input->post('partners_id');
                        $old_image = $this->Access->readtable('partners','partners_image',array('partners_id'=>$partners_id))->row()->partners_image;       
                            unlink( realpath( APPPATH.'../assets/upload/partners/content/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/partners/content/thumbnail/'.$old_image ));

                        $edit_partner = array(
                                            'partners_name' => $this->input->post('partners_name'),
                                            'partners_category' => $this->input->post('partners_category'),
                                            'partners_phone' => $this->input->post('partners_phone'),
                                            'partners_address' => $this->input->post('partners_address'),
                                            'partners_city' => $this->input->post('partners_city'),
                                            'partners_image' => $name_url
                                            );
                        $this->db->trans_begin();
                        $this->Access->updatetable('partners',$edit_partner,array('partners_id'=>$partners_id));
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }
                        else {
                            $success = $this->lang->line("update");
                            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                        }
                    }
                }
            }
        }
        $_SESSION['partners_list'] = $notif;
        $this->session->mark_as_flash('partners_list');
        redirect('backend/partners#partners_list');
    }

    function delete_partner($partners_id)
    {
        $image = $this->Access->readtable('partners','partners_image',array('partners_id'=>$partners_id))->row()->partners_image;
        unlink( realpath( APPPATH.'../assets/upload/partners/content/'.$image ));
        unlink( realpath( APPPATH.'../assets/upload/partners/content/thumbnail/'.$image ));

        $this->db->trans_begin();
        $this->Access->deletetable('partners',array('partners_id'=>$partners_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['partners_list'] = $notif;
        $this->session->mark_as_flash('partners_list');
        redirect('backend/partners#partners_list');
    }
}