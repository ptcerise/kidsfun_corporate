<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['career_content'] = $this->Access->readtable('general','',array('general_page'=>'Career', 'general_section'=>'career_content'))->row();
        $data['job_list'] = $this->Access->readtable('career','')->result();

        $data['current'] = "career";
        $view['script']  = $this->load->view('backend/script/career','',TRUE);
        $view['content'] = $this->load->view('backend/career/v_career',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // UPDATE HOME TOP CONTENT 
    function edit_content()
    {
        $check = $this->Access->readtable('general','',array('general_page'=>'Career', 'general_section'=>'career_content'))->row();

        // INSERT TO DATABASE IF DATA IS NOT SET
        if($check=='')
        {
            $add_content = array(
                                'general_page'=>'Career',
                                'general_section'=>'career_content',
                                'general_content_in' => $this->input->post('career_content_in'),
                                'general_content_en' => $this->input->post('career_content_en')
                            );
            $this->db->trans_begin();
            $this->Access->inserttable('general',$add_content);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("insert");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        else
        {
            // UPDATE DATABASE IF DATA IS SET
            $edit_content = array(
                                'general_content_in' => $this->input->post('career_content_in'),
                                'general_content_en' => $this->input->post('career_content_en')
                                );
            $this->db->trans_begin();
            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Career', 'general_section'=>'career_content'));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/career');
    }

    function add_job()
    {
        $add_job = array(
                            'career_title_in' => $this->input->post('job_title_in'),
                            'career_title_en' => $this->input->post('job_title_en'),
                            'career_desc_in' => $this->input->post('job_desc_in'),
                            'career_desc_en' => $this->input->post('job_desc_en')
                        );
        $this->db->trans_begin();
        $this->Access->inserttable('career',$add_job);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("insert");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['job_list'] = $notif;
        $this->session->mark_as_flash('job_list');
        redirect('backend/career#job_list');
    }

    function edit_job()
    {
        $edit_job = array(
                            'career_title_in' => $this->input->post('job_title_in'),
                            'career_title_en' => $this->input->post('job_title_en'),
                            'career_desc_in' => $this->input->post('job_desc_in'),
                            'career_desc_en' => $this->input->post('job_desc_en')
                        );
        $this->db->trans_begin();
        $job_id = $this->input->post('job_id');
        $this->Access->updatetable('career',$edit_job,array('career_id'=>$job_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['job_list'] = $notif;
        $this->session->mark_as_flash('job_list');
        redirect('backend/career#job_list');
    }

    function delete_job($career_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('career',array('career_id'=>$career_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['job_list'] = $notif;
        $this->session->mark_as_flash('job_list');
        redirect('backend/career#job_list');
    }
}