<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Follow_us extends CI_Controller {

	public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index() 
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['trip_advisor'] = $this->Access->readtable('general','',array('general_section'=>'trip_advisor','general_page'=>'Follow_us'))->row();
        $data['facebook'] = $this->Access->readtable('general','',array('general_section'=>'facebook','general_page'=>'Follow_us'))->row();
        $data['twitter'] = $this->Access->readtable('general','',array('general_section'=>'twitter','general_page'=>'Follow_us'))->row();
        $data['instagram'] = $this->Access->readtable('general','',array('general_section'=>'instagram','general_page'=>'Follow_us'))->row();
        $data['youtube'] = $this->Access->readtable('general','',array('general_section'=>'youtube','general_page'=>'Follow_us'))->row();

        $data['current'] = "follow_us";
        $view['content'] = $this->load->view('backend/follow_us/v_follow_us',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    function trip_advisor()
    {
    	$links = array(
    					'general_url' => $this->input->post('trip_advisor')
    					);
    	$cek = 	$this->Access->readtable('general','',array('general_section'=>'trip_advisor','general_page'=>'Follow_us'))->row();

    	if($cek=='')
    	{
    		$add = array(
    					'general_section'=>'trip_advisor',
    					'general_page'=>'Follow_us',
    					'general_url' => $this->input->post('trip_advisor')
    					);
    		$this->db->trans_begin();
    		$this->Access->inserttable('general',$add);
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	else
    	{
    		$this->db->trans_begin();
    		$this->Access->updatetable('general',$links,array('general_section'=>'trip_advisor','general_page'=>'Follow_us'));
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	$_SESSION['info_follow_us'] = $notif;
        $this->session->mark_as_flash('info_follow_us');
        redirect('backend/follow_us');
    }

    function facebook()
    {
    	$links = array(
    					'general_url' => $this->input->post('facebook')
    					);
    	$cek = 	$this->Access->readtable('general','',array('general_section'=>'facebook','general_page'=>'Follow_us'))->row();

    	if($cek=='')
    	{
    		$add = array(
    					'general_section'=>'facebook',
    					'general_page'=>'Follow_us',
    					'general_url' => $this->input->post('facebook')
    					);
    		$this->db->trans_begin();
    		$this->Access->inserttable('general',$add);
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	else
    	{
    		$this->db->trans_begin();
    		$this->Access->updatetable('general',$links,array('general_section'=>'facebook','general_page'=>'Follow_us'));
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	$_SESSION['info_follow_us'] = $notif;
        $this->session->mark_as_flash('info_follow_us');
        redirect('backend/follow_us');
    }

    function twitter()
    {
    	$links = array(
    					'general_url' => $this->input->post('twitter')
    					);
    	$cek = 	$this->Access->readtable('general','',array('general_section'=>'twitter','general_page'=>'Follow_us'))->row();

    	if($cek=='')
    	{
    		$add = array(
    					'general_section'=>'twitter',
    					'general_page'=>'Follow_us',
    					'general_url' => $this->input->post('twitter')
    					);
    		$this->db->trans_begin();
    		$this->Access->inserttable('general',$add);
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	else
    	{
    		$this->db->trans_begin();
    		$this->Access->updatetable('general',$links,array('general_section'=>'twitter','general_page'=>'Follow_us'));
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	$_SESSION['info_follow_us'] = $notif;
        $this->session->mark_as_flash('info_follow_us');
        redirect('backend/follow_us');
    }

    function instagram()
    {
    	$links = array(
    					'general_url' => $this->input->post('instagram')
    					);
    	$cek = 	$this->Access->readtable('general','',array('general_section'=>'instagram','general_page'=>'Follow_us'))->row();

    	if($cek=='')
    	{
    		$add = array(
    					'general_section'=>'instagram',
    					'general_page'=>'Follow_us',
    					'general_url' => $this->input->post('instagram')
    					);
    		$this->db->trans_begin();
    		$this->Access->inserttable('general',$add);
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	else
    	{
    		$this->db->trans_begin();
    		$this->Access->updatetable('general',$links,array('general_section'=>'instagram','general_page'=>'Follow_us'));
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	$_SESSION['info_follow_us'] = $notif;
        $this->session->mark_as_flash('info_follow_us');
        redirect('backend/follow_us');
    }

    function youtube()
    {
    	$links = array(
    					'general_url' => $this->input->post('youtube')
    					);
    	$cek = 	$this->Access->readtable('general','',array('general_section'=>'youtube','general_page'=>'Follow_us'))->row();

    	if($cek=='')
    	{
    		$add = array(
    					'general_section'=>'youtube',
    					'general_page'=>'Follow_us',
    					'general_url' => $this->input->post('youtube')
    					);
    		$this->db->trans_begin();
    		$this->Access->inserttable('general',$add);
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	else
    	{
    		$this->db->trans_begin();
    		$this->Access->updatetable('general',$links,array('general_section'=>'youtube','general_page'=>'Follow_us'));
    		$this->db->trans_complete();

	        if ($this->db->trans_status() === FALSE)
	        {
	            $this->db->trans_rollback();
	        }
	        else {
	            $success = $this->lang->line("update");
	            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
	        }
    	}
    	$_SESSION['info_follow_us'] = $notif;
        $this->session->mark_as_flash('info_follow_us');
        redirect('backend/follow_us');
    }
}