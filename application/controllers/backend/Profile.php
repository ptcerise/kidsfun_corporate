<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('Access');
      	$be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }     
    }
	
	function index()
	{
		$data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
		$user_id = $this->session->userdata('user_id');
		$data['user'] = $this->Access->readtable('user','',array('user_id'=>$user_id))->row();
		$data['current'] = "Profile";
		$view['content'] = $this->load->view('backend/profile/v_account',$data,TRUE);
		$this->load->view('backend/v_master',$view);
	}

	// UPDATE USER PROFILE
	function account($user_id)
	{
		$user = array(
					'user_name' => $this->input->post('user_name'),
					'user_full_name' => $this->input->post('user_full_name')
					);
		$this->db->where('user_id', $user_id);
		$this->db->update('user', $user);
		$this->session->set_userdata($user);
		$success = $this->lang->line('update_profile');
		$this->session->set_flashdata('account',$success);
		redirect('backend/profile');
	}

	// UPDATE USER PASSWORD
	function password($user_id)
	{
		$current = md5($this->input->post('current'));
		$new = md5($this->input->post('new'));
		$check = $this->Access->readtable('user','user_pass',array('user_id'=>$user_id))->row()->user_pass;
		
		if($check == $current){
			$success = $this->lang->line('update_password');
			$this->Access->updatetable('user',array('user_pass'=>$new),array('user_id'=>$user_id));
			$this->session->set_flashdata('success',$success);
			redirect('backend/profile');
		} else {
			$wrong = $this->lang->line('wrong_pass');
			$this->session->set_flashdata('failed',$wrong);
			redirect('backend/profile');
		}
	}
}