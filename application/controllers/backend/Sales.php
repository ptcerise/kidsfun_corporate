<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['general'] = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'general_content'))->row();
        $data['sales_content'] = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'sales_content'))->row();
        $data['sales_img'] = $this->Access->readtable('media','',array('media_page'=>'Sales', 'media_section'=>'sales_image'))->row();
        $data['products_list'] = $this->Access->readtable('products','')->result();
        $data['rental_content'] = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'rental_content'))->row();
        $data['rental_image'] = $this->Access->readtable('media','',array('media_page'=>'Sales', 'media_section'=>'rental_image'))->row();

        $data['current'] = "sales";
        $view['script']  = $this->load->view('backend/script/sales','',TRUE);
        $view['content'] = $this->load->view('backend/sales/v_sales',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    function edit_general()
    {
        $check = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'general_content'))->row();

        if($check=='')
        {
            $add_general = array(
                            'general_title_in' => $this->input->post('general_title_in'),
                            'general_title_en' => $this->input->post('general_title_en'),
                            'general_content_in' => $this->input->post('general_content_in'),
                            'general_content_en' => $this->input->post('general_content_en'),
                            'general_page' => 'Sales',
                            'general_section'=>'general_content'
                            );
            $this->db->trans_begin();
            $this->Access->inserttable('general',$add_general);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        else
        {
            $edit_general = array(
                            'general_title_in' => $this->input->post('general_title_in'),
                            'general_title_en' => $this->input->post('general_title_en'),
                            'general_content_in' => $this->input->post('general_content_in'),
                            'general_content_en' => $this->input->post('general_content_en')
                            );
            $this->db->trans_begin();
            $this->Access->updatetable('general',$edit_general,array('general_page'=>'Sales', 'general_section'=>'general_content'));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        $_SESSION['info_general'] = $notif;
        $this->session->mark_as_flash('info_general');
        redirect('backend/sales');
    }

    function edit_content()
    {
        $sales_img = $_FILES['sales_img']['name'];
        if($sales_img=='')
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'sales_content'))->row();
            if($check=='')
            {
                $add_content = array(
                                    'general_page'=>'Sales',
                                    'general_section'=>'sales_content',
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('sales_content_in'),
                                    'general_content_en' => $this->input->post('sales_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->inserttable('general',$add_content);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
            else
            {
                $edit_content = array(
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('sales_content_in'),
                                    'general_content_en' => $this->input->post('sales_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->updatetable('general',$edit_content,array('general_page'=>'Sales', 'general_section'=>'sales_content'));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
        }
        else
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'sales_content'))->row();
            if($check == '')
            {
                // UPDATE IMAGE + INSERT CONTENT (CONTENT NOT SET)
                $sales_img = $_FILES['sales_img']['name'];
                $break = explode('.', $sales_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'sales_img_'.$date.'.'.$ext;
                $path = './assets/upload/sales';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('sales_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/sales/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'sales_image','media_page'=>'Sales'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/cropped/'.$old_image ));

                            $add_content = array(
                                                'general_page'=>'Sales',
                                                'general_section'=>'sales_content',
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('sales_content_in'),
                                                'general_content_en' => $this->input->post('sales_content_en')
                                            );

                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'sales_image','media_page'=>'Sales'));
                            $this->Access->inserttable('general',$add_content);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
            else
            {
                // UPDATE IMAGE + UPDATE CONTENT (CONTENT IS SET)
                $sales_img = $_FILES['sales_img']['name'];
                $break = explode('.', $sales_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'sales_img_'.$date.'.'.$ext;
                $path = './assets/upload/sales';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('sales_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/sales/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'sales_image','media_page'=>'Sales'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/cropped/'.$old_image ));

                            $edit_content = array(
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('sales_content_in'),
                                                'general_content_en' => $this->input->post('sales_content_en')
                                            );
                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'sales_image','media_page'=>'Sales'));
                            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Sales', 'general_section'=>'sales_content'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/sales#content');
    }

    function add_product()
    {
        $content_img = $_FILES['content_img']['name'];
        $break = explode('.', $content_img);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'product_'.$date.'.'.$ext;
        $path = './assets/upload/sales/content';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('content_img') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1000 || $height < 1000 )
            {
                unlink( realpath( APPPATH.'../assets/upload/sales/content/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/sales/content/thumbnail/'.$name_url ));
                $image_1000px = $this->lang->line('image_1000px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1000px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'landscape' || $orientation == 'portrait' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/sales/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/sales/content/thumbnail/'.$name_url ));
                    $square = $this->lang->line('square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1000, 1000);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }
                    $raw_price = $this->input->post('products_price');
                    $add_product = array(
                                        'products_name' => $this->input->post('products_name'),
                                        'products_dimension' => $this->input->post('products_dimension'),
                                        'products_power' => $this->input->post('products_power'),
                                        'products_delivery_time' => $this->input->post('products_delivery_time'),
                                        'products_price' => str_replace(array("Rp ","."),"",$raw_price),
                                        'products_image' => $name_url
                                        );
                    $this->db->trans_begin();
                    $this->Access->inserttable('products',$add_product);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        $_SESSION['products_list'] = $notif;
        $this->session->mark_as_flash('products_list');
        redirect('backend/sales#products_list');
    }

    function edit_product()
    {
        $edit_img = $_FILES['edit_img']['name'];
        if($edit_img=='')
        {
            $raw_price = $this->input->post('products_price');
            $edit_product = array(
                            'products_name' => $this->input->post('products_name'),
                            'products_dimension' => $this->input->post('products_dimension'),
                            'products_power' => $this->input->post('products_power'),
                            'products_delivery_time' => $this->input->post('products_delivery_time'),
                            'products_price' => str_replace(array("Rp ","."),"",$raw_price)
                            );
            $this->db->trans_begin();
            $products_id = $this->input->post('products_id');
            $this->Access->updatetable('products',$edit_product,array('products_id'=>$products_id));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        else
        {

            $break = explode('.', $edit_img);
            $ext = $break[count($break) - 1];
            $date = date('dmYHis');
            $name_url = 'product_'.$date.'.'.$ext;
            $path = './assets/upload/sales/content';

            if( ! file_exists( $path ) )
            {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
            }
            $this->piclib->get_config($name_url, $path);
            if( $this->upload->do_upload('edit_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1000 || $height < 1000 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/sales/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/sales/content/thumbnail/'.$name_url ));
                    $image_1000px = $this->lang->line('image_1000px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1000px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'landscape' || $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/sales/content/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/sales/content/thumbnail/'.$name_url ));
                        $square = $this->lang->line('square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1000, 1000);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                        }

                        $products_id = $this->input->post('products_id');
                        $raw_price = $this->input->post('products_price');
                        $old_image = $this->Access->readtable('products','products_image',array('products_id'=>$products_id))->row()->products_image;       
                            unlink( realpath( APPPATH.'../assets/upload/sales/content/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/content/thumbnail/'.$old_image ));

                        $edit_product = array(
                                            'products_name' => $this->input->post('products_name'),
                                            'products_dimension' => $this->input->post('products_dimension'),
                                            'products_power' => $this->input->post('products_power'),
                                            'products_delivery_time' => $this->input->post('products_delivery_time'),
                                            'products_price' => str_replace(array("Rp ","."),"",$raw_price),
                                            'products_image' => $name_url
                                            );
                        $this->db->trans_begin();
                        $this->Access->updatetable('products',$edit_product,array('products_id'=>$products_id));
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }
                        else {
                            $success = $this->lang->line("update");
                            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                        }
                    }
                }
            }
        }
        $_SESSION['products_list'] = $notif;
        $this->session->mark_as_flash('products_list');
        redirect('backend/sales#products_list');
    }

    function delete_product($products_id)
    {
        $image = $this->Access->readtable('products','products_image',array('products_id'=>$products_id))->row()->products_image;
        unlink( realpath( APPPATH.'../assets/upload/sales/content/'.$image ));
        unlink( realpath( APPPATH.'../assets/upload/sales/content/thumbnail/'.$image ));

        $this->db->trans_begin();
        $this->Access->deletetable('products',array('products_id'=>$products_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['products_list'] = $notif;
        $this->session->mark_as_flash('products_list');
        redirect('backend/sales#products_list');
    }

    // UPDATE EXPLITATION AND RENTAL
    function edit_rental()
    {
        $rental_img = $_FILES['rental_img']['name'];
        if($rental_img == '')
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'rental_content'))->row();
            if($check == '')
            {
                // INSERT TO DATABASE IF DATA IS NOT SET
                $add_content = array(
                                'general_page'=>'Sales',
                                'general_section'=>'rental_content',
                                'general_content_in' => $this->input->post('rental_content_in'),
                                'general_content_en' => $this->input->post('rental_content_en')
                            );
                $this->db->trans_begin();
                $this->Access->inserttable('general',$add_content);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                } 
            }
            else
            {
                // UPDATE DATABASE IF DATA IS SET
                $edit_content = array(
                                'general_content_in' => $this->input->post('rental_content_in'),
                                'general_content_en' => $this->input->post('rental_content_en')
                            );
                $this->db->trans_begin();
                $this->Access->updatetable('general',$edit_content,array('general_page'=>'Sales', 'general_section'=>'rental_content'));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
        }
        else
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Sales', 'general_section'=>'rental_content'))->row();
            if($check == '')
            {
                // UPDATE IMAGE + INSERT CONTENT
                $rental_img = $_FILES['rental_img']['name'];
                $break = explode('.', $rental_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'rental_'.$date.'.'.$ext;
                $path = './assets/upload/sales';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createThumb )
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('rental_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'rental_image','media_page'=>'Sales'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$old_image ));

                            $add_content = array(
                                                'general_page'=>'Sales',
                                                'general_section'=>'rental_content',
                                                'general_content_in' => $this->input->post('rental_content_in'),
                                                'general_content_en' => $this->input->post('rental_content_en')
                                                );
                            $content_img = array(
                                                'media_url' => $name_url
                                            );
                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'rental_image','media_page'=>'Sales'));
                            $this->Access->inserttable('general',$add_content);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
            else
            {
                // UPDATE IMAGE + UPDATE CONTENT
                $rental_img = $_FILES['rental_img']['name'];
                $break = explode('.', $rental_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'rental_'.$date.'.'.$ext;
                $path = './assets/upload/sales';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createThumb )
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('rental_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'rental_image','media_page'=>'Sales'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/sales/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/sales/thumbnail/'.$old_image ));

                            $edit_content = array(
                                                'general_content_in' => $this->input->post('rental_content_in'),
                                                'general_content_en' => $this->input->post('rental_content_en')
                                                );
                            $content_img = array(
                                                'media_url' => $name_url
                                                );
                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'rental_image','media_page'=>'Sales'));
                            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Sales', 'general_section'=>'rental_content'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
        }
        $_SESSION['info_rental'] = $notif;
        $this->session->mark_as_flash('info_rental');
        redirect('backend/sales#rental');
    }
}