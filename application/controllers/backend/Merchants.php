<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchants extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['merchants_content'] = $this->Access->readtable('general','',array('general_page'=>'Merchants', 'general_section'=>'merchants_content'))->row();
        $data['merchants_img'] = $this->Access->readtable('media','',array('media_page'=>'Merchants', 'media_section'=>'merchants_image'))->row();
        $data['merchants_list'] = $this->Access->readtable('merchants','')->result();

        $data['current'] = "merchants";
        $view['script']  = $this->load->view('backend/script/merchants','',TRUE);
        $view['content'] = $this->load->view('backend/merchants/v_merchants',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // UPDATE OUTLET CONTENT
    function edit_content()
    {
        $merchants_img = $_FILES['merchants_img']['name'];
        if($merchants_img == '')
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Merchants', 'general_section'=>'merchants_content'))->row();
            if($check == '')
            {
                // INSERT TO DATABASE IF DATA IS NOT SET 
                $add_content = array(
                                    'general_page'=>'Merchants',
                                    'general_section'=>'merchants_content',
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('merchants_content_in'),
                                    'general_content_en' => $this->input->post('merchants_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->inserttable('general',$add_content);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("insert");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
            else
            {
                // UPDATE DATABASE IF DATA IS SET
                $edit_content = array(
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('merchants_content_in'),
                                    'general_content_en' => $this->input->post('merchants_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->updatetable('general',$edit_content,array('general_page'=>'Merchants', 'general_section'=>'merchants_content'));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
        }
        else
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Merchants', 'general_section'=>'merchants_content'))->row();
            if($check == '')
            {
                // UPDATE IMAGE + INSERT CONTENT
                $merchants_img = $_FILES['merchants_img']['name'];
                $break = explode('.', $merchants_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'merchant_img_'.$date.'.'.$ext;
                $path = './assets/upload/merchants';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('merchants_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/merchants/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/merchants/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/merchants/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/merchants/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'merchants_image','media_page'=>'Merchants'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/merchants/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/cropped/'.$old_image ));

                            $add_content = array(
                                                'general_page'=>'Merchants',
                                                'general_section'=>'merchants_content',
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('merchants_content_in'),
                                                'general_content_en' => $this->input->post('merchants_content_en')
                                            );

                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'merchants_image','media_page'=>'Merchants'));
                            $this->Access->inserttable('general',$add_content);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
            else
            {
                // UPDATE IMAGE + UPDATE CONTENT
                $merchants_img = $_FILES['merchants_img']['name'];
                $break = explode('.', $merchants_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'merchant_img_'.$date.'.'.$ext;
                $path = './assets/upload/merchants';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('merchants_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/merchants/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/merchants/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/merchants/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/merchants/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'merchants_image','media_page'=>'Merchants'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/merchants/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/cropped/'.$old_image ));

                            $edit_content = array(
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('merchants_content_in'),
                                                'general_content_en' => $this->input->post('merchants_content_en')
                                            );

                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'merchants_image','media_page'=>'Merchants'));
                            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Merchants', 'general_section'=>'merchants_content'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/merchants');
    }

    function add_merchant()
    {
        $content_img = $_FILES['content_img']['name'];
        $break = explode('.', $content_img);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'merchant_'.$date.'.'.$ext;
        $path = './assets/upload/merchants/content';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('content_img') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1250 || $height < 1000 )
            {
                unlink( realpath( APPPATH.'../assets/upload/merchants/content'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/merchants/content/thumbnail/'.$name_url ));
                $image_1250px = $this->lang->line('image_1250px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1250px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/merchants/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/merchants/content/thumbnail/'.$name_url ));
                    $lands_square = $this->lang->line('lands_square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1250, 1000);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }

                    $add_merchant = array(
                                        'merchants_name' => $this->input->post('merchants_name'),
                                        'merchants_phone' => $this->input->post('merchants_phone'),
                                        'merchants_address' => $this->input->post('merchants_address'),
                                        'merchants_city' => $this->input->post('merchants_city'),
                                        'merchants_desc_in' => $this->input->post('merchants_desc_in'),
                                        'merchants_desc_en' => $this->input->post('merchants_desc_en'),
                                        'merchants_image' => $name_url
                                        );
                    $this->db->trans_begin();
                    $this->Access->inserttable('merchants',$add_merchant);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        $_SESSION['merchants_list'] = $notif;
        $this->session->mark_as_flash('merchants_list');
        redirect('backend/merchants#merchants_list');
    }

    function edit_merchant()
    {
        $edit_img = $_FILES['edit_img']['name'];
        if($edit_img=='')
        {
            $edit_merchant = array(
                            'merchants_name' => $this->input->post('merchants_name'),
                            'merchants_phone' => $this->input->post('merchants_phone'),
                            'merchants_address' => $this->input->post('merchants_address'),
                            'merchants_city' => $this->input->post('merchants_city'),
                            'merchants_desc_in' => $this->input->post('merchants_desc_in'),
                            'merchants_desc_en' => $this->input->post('merchants_desc_en')
                            );
            $this->db->trans_begin();
            $merchants_id = $this->input->post('merchants_id');
            $this->Access->updatetable('merchants',$edit_merchant,array('merchants_id'=>$merchants_id));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        else
        {

            $break = explode('.', $edit_img);
            $ext = $break[count($break) - 1];
            $date = date('dmYHis');
            $name_url = 'merchant_'.$date.'.'.$ext;
            $path = './assets/upload/merchants/content';

            if( ! file_exists( $path ) )
            {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
            }
            $this->piclib->get_config($name_url, $path);
            if( $this->upload->do_upload('edit_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1250 || $height < 1000 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/merchants/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/merchants/content/thumbnail/'.$name_url ));
                    $image_1250px = $this->lang->line('image_1250px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$image_1250px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/merchants/content/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/merchants/content/thumbnail/'.$name_url ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1250, 1000);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                        }

                        $merchants_id = $this->input->post('merchants_id');
                        $old_image = $this->Access->readtable('merchants','merchants_image',array('merchants_id'=>$merchants_id))->row()->merchants_image;       
                            unlink( realpath( APPPATH.'../assets/upload/merchants/content/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/merchants/content/thumbnail/'.$old_image ));

                        $edit_merchant = array(
                                            'merchants_name' => $this->input->post('merchants_name'),
                                            'merchants_phone' => $this->input->post('merchants_phone'),
                                            'merchants_address' => $this->input->post('merchants_address'),
                                            'merchants_city' => $this->input->post('merchants_city'),
                                            'merchants_desc_in' => $this->input->post('merchants_desc_in'),
                                            'merchants_desc_en' => $this->input->post('merchants_desc_en'),
                                            'merchants_image' => $name_url
                                            );
                        $this->db->trans_begin();
                        $this->Access->updatetable('merchants',$edit_merchant,array('merchants_id'=>$merchants_id));
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }
                        else {
                            $success = $this->lang->line("update");
                            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                        }
                    }
                }
            }
        }
        $_SESSION['merchants_list'] = $notif;
        $this->session->mark_as_flash('merchants_list');
        redirect('backend/merchants#merchants_list');
    }

    function delete_merchant($merchants_id)
    {
        $image = $this->Access->readtable('merchants','merchants_image',array('merchants_id'=>$merchants_id))->row()->merchants_image;
        unlink( realpath( APPPATH.'../assets/upload/merchants/content/'.$image ));
        unlink( realpath( APPPATH.'../assets/upload/merchants/content/thumbnail/'.$image ));

        $this->db->trans_begin();
        $this->Access->deletetable('merchants',array('merchants_id'=>$merchants_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['merchants_list'] = $notif;
        $this->session->mark_as_flash('merchants_list');
        redirect('backend/merchants#merchants_list');
    }
}