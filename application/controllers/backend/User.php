<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
            $this->load->model('Access');     
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['user_list'] = $this->db->query("select * from user")->result();
        $view['script']  = $this->load->view('backend/script/user','',TRUE);

        $data['current'] = "user";
        $view['content'] = $this->load->view('backend/user/v_user',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // ------------------- check same username --------------------
    function check()
    {
        $user_name = $this->input->post('user_name');
        $check = $this->access->readtable('user', '', array('user_name' => $user_name))->num_rows();
        if( $check > 0 )
        {
            $status = 1;
        } else {
            $status = 0;
        }

        $data = array('status' => $status);
        echo json_encode($data);
    }

    // -------------------- save new user ------------------------
    function save_new()
    {
        $user_level = $this->input->post('user_level');
        if ($user_level == 'Admin'){
            $level_id = 1;
        }elseif($user_level == 'Operator'){
            $level_id = 2;
        }
        
        $user = array(
                        'user_full_name'=>$this->input->post('user_full_name'),
                        'user_level'=>$level_id,
                        'user_name'=>$this->input->post('user_name'),
                        'user_pass'=>md5($this->input->post('user_pass'))
                    );
        $this->db->trans_begin();
        $this->Access->inserttable('user',$user);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("insert");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';  
        }
        $_SESSION['info_user'] = $notif;
        $this->session->mark_as_flash('info_user');
        redirect('backend/user');
    }

    // ------------------- form edit user ------------------
    function edit_form()
    {
        $user_id = $this->input->post('user_id');
        $admin_id = $this->session->userdata('user_id');
        
        $data['edit_user'] = $this->Access->readtable('user','',array('user_id'=>$user_id))->row();
        $data['admin'] = $this->Access->readtable('user','',array('user_id'=>$admin_id))->row();

        $this->load->view('backend/user/v_user_edit',$data);
    }

    // ------------------ edit username --------------------
    function save_user()
    {
        $admin_id = $this->session->userdata('user_id');
        $admin_pass = $this->Access->readtable('user','user_pass',array('user_id'=>$admin_id))->row()->user_pass;
        $check_pass = $this->input->post('check_pass');

        if($admin_pass != $check_pass)
        {

        }

        $user_id = $this->input->post('user_id');

        $new_user = array(
                        'user_name'=>$this->input->post('user_name'),
                        'user_full_name'=>$this->input->post('user_full_name'),
                        );
        $this->db->trans_begin();
        $this->Access->updatetable('user',$new_user,array('user_id'=>$user_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_user'] = $notif;
        $this->session->mark_as_flash('info_user');
        redirect('backend/user');
    }

    // ----------------- edit password -------------------
    function save_pass($user_id)
    {
        $current_pass = md5($this->input->post('current_pass'));
        $new_pass = md5($this->input->post('new_pass'));

        $check = $this->Access->readtable('user','user_pass',array('user_id'=>$user_id))->row()->user_pass;

        if($check == $current_pass){
            $this->Access->updatetable('user',array('user_pass'=>$new_pass),array('user_id'=>$user_id));

            $success = $this->lang->line("update_pass");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            
            $_SESSION['info_user'] = $notif;
            $this->session->mark_as_flash('info_user');
            redirect('backend/user');
        }else {
            $error = $this->lang->line("wrong_pass");
            $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$error.'</div>';

            $_SESSION['password'] = $notif;
            $this->session->mark_as_flash('password');
            redirect('backend/user');
        }
    }

    // ------------------------ delete user -------------------------
    function delete_user($user_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('user', array('user_id'=> $user_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['info_user'] = $notif;
        $this->session->mark_as_flash('info_user');
        redirect('backend/user');
    }
}