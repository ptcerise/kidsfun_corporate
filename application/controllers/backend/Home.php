<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
    	$data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['home_content'] = $this->Access->readtable('general','',array('general_page'=>'Home', 'general_section'=>'home_content'))->row();
        $data['about_us_content'] = $this->Access->readtable('general','',array('general_page'=>'Home', 'general_section'=>'about_us_content'))->row();
        $data['about_us_image'] = $this->Access->readtable('media','',array('media_page'=>'Home', 'media_section'=>'about_us_image'))->row();
        $data['menu_list'] = $this->Access->readtable('media','',array('media_page'=>'Home', 'media_section'=>'menu'))->result();

        $data['current'] = "home";
        $view['script']  = $this->load->view('backend/script/home','',TRUE);
    	$view['content'] = $this->load->view('backend/home/v_home',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // UPDATE HOME TOP CONTENT 
    function edit_content()
    {
        $check = $this->Access->readtable('general','',array('general_page'=>'Home', 'general_section'=>'home_content'))->row();

        // INSERT TO DATABASE IF DATA IS NOT SET
        if($check=='')
        {
            $add_home = array(
                                'general_page'=>'Home',
                                'general_section'=>'home_content',
                                'general_title_in' => $this->input->post('home_title_in'),
                                'general_title_en' => $this->input->post('home_title_en'),
                                'general_content_in' => $this->input->post('home_content_in'),
                                'general_content_en' => $this->input->post('home_content_en')
                            );
            $this->db->trans_begin();
            $this->Access->inserttable('general',$add_home);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        else
        {
            // UPDATE DATABASE IF DATA IS SET
            $edit_home = array(
                                'general_title_in' => $this->input->post('home_title_in'),
                                'general_title_en' => $this->input->post('home_title_en'),
                                'general_content_in' => $this->input->post('home_content_in'),
                                'general_content_en' => $this->input->post('home_content_en')
                            );
            $this->db->trans_begin();
            $this->Access->updatetable('general',$edit_home,array('general_page'=>'Home', 'general_section'=>'home_content'));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/home');
    }

    // UPDATE ABOUT US ON HOME PAGE
    function edit_about_us()
    {
        $about_us_img = $_FILES['about_us_img']['name'];
        if($about_us_img == '')
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Home', 'general_section'=>'about_us_content'))->row();
            if($check == '')
            {
                // INSERT TO DATABASE IF DATA IS NOT SET
                $add_content = array(
                                'general_page'=>'Home',
                                'general_section'=>'about_us_content',
                                'general_content_in' => $this->input->post('about_us_content_in'),
                                'general_content_en' => $this->input->post('about_us_content_en')
                            );
                $this->db->trans_begin();
                $this->Access->inserttable('general',$add_content);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                } 
            }
            else
            {
                // UPDATE DATABASE IF DATA IS SET
                $edit_content = array(
                                'general_content_in' => $this->input->post('about_us_content_in'),
                                'general_content_en' => $this->input->post('about_us_content_en')
                            );
                $this->db->trans_begin();
                $this->Access->updatetable('general',$edit_content,array('general_page'=>'Home', 'general_section'=>'about_us_content'));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
        }
        else
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Home', 'general_section'=>'about_us_content'))->row();
            if($check == '')
            {
                // UPDATE IMAGE + INSERT CONTENT
                $about_us_img = $_FILES['about_us_img']['name'];
                $break = explode('.', $about_us_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'about_us_'.$date.'.'.$ext;
                $path = './assets/upload/home';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createThumb )
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('about_us_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'about_us_image','media_page'=>'Home'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/home/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$old_image ));

                            $add_content = array(
                                                'general_page'=>'Home',
                                                'general_section'=>'about_us_content',
                                                'general_content_in' => $this->input->post('about_us_content_in'),
                                                'general_content_en' => $this->input->post('about_us_content_en')
                                                );
                            $content_img = array(
                                                'media_url' => $name_url
                                            );
                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'about_us_image','media_page'=>'Home'));
                            $this->Access->inserttable('general',$add_content);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
            else
            {
                // UPDATE IMAGE + UPDATE CONTENT
                $about_us_img = $_FILES['about_us_img']['name'];
                $break = explode('.', $about_us_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'about_us_'.$date.'.'.$ext;
                $path = './assets/upload/home';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    if( ! $create || ! $createThumb )
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('about_us_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'about_us_image','media_page'=>'Home'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/home/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$old_image ));

                            $edit_content = array(
                                                'general_content_in' => $this->input->post('about_us_content_in'),
                                                'general_content_en' => $this->input->post('about_us_content_en')
                                                );
                            $content_img = array(
                                                'media_url' => $name_url
                                                );
                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'about_us_image','media_page'=>'Home'));
                            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Home', 'general_section'=>'about_us_content'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
        }
        $_SESSION['info_about_us'] = $notif;
        $this->session->mark_as_flash('info_about_us');
        redirect('backend/home#about_us');
    }

    function add_menu()
    {
        $content_img = $_FILES['content_img']['name'];
        $break = explode('.', $content_img);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'menu_'.$date.'.'.$ext;
        $path = './assets/upload/home';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('content_img') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1000 || $height < 1000 )
            {
                unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                $image_1000px = $this->lang->line('image_1000px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1000px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'landscape' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                    $square = $this->lang->line('square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1000, 1000);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }

                    $add_menu = array(
                                        'media_title_in' => $this->input->post('media_title_in'),
                                        'media_title_en' => $this->input->post('media_title_en'),
                                        'media_url' => $name_url,
                                        'media_page' => 'Home',
                                        'media_section' => 'menu',
                                        );
                    $this->db->trans_begin();
                    $this->Access->inserttable('media',$add_menu);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        $_SESSION['menu_list'] = $notif;
        $this->session->mark_as_flash('menu_list');
        redirect('backend/home#menu_list');
    }

    function edit_menu()
    {
        $edit_img = $_FILES['edit_img']['name'];
        $break = explode('.', $edit_img);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'menu_'.$date.'.'.$ext;
        $path = './assets/upload/home';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('edit_img') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1000 || $height < 1000 )
            {
                unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                $image_1000px = $this->lang->line('image_1000px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1000px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' || $orientation == 'landscape' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/home/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$name_url ));
                    $square = $this->lang->line('square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1000, 1000);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 200, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }

                    $media_id = $this->input->post('media_id');
                    $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$media_id))->row()->media_url;       
                        unlink( realpath( APPPATH.'../assets/upload/home/'.$old_image ));
                        unlink( realpath( APPPATH.'../assets/upload/home/thumbnail/'.$old_image ));

                    $edit_menu = array(
                                        'media_url' => $name_url
                                        );
                    $this->db->trans_begin();
                    $this->Access->updatetable('media',$edit_menu,array('media_id'=>$media_id));
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("update");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        $_SESSION['menu_list'] = $notif;
        $this->session->mark_as_flash('menu_list');
        redirect('backend/home#menu_list');
    }
}