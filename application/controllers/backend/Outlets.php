<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Outlets extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('kidsfun_backend',$be_lang);
        } else {
            $this->lang->load('kidsfun_backend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $outlets_id = $this->input->post('outlets_id');
        $data['outlets_content'] = $this->Access->readtable('general','',array('general_page'=>'Outlets', 'general_section'=>'outlets_content'))->row();
        $data['outlets_img'] = $this->Access->readtable('media','',array('media_page'=>'Outlets', 'media_section'=>'outlets_image'))->row();
        $data['outlets_list'] = $this->Access->readtable('outlets','')->result();
        $data['outlets_edit'] = $this->Access->readtable('outlets','',array('outlets_id'=>$outlets_id))->row();
        $data['gallery_list'] = $this->Access->readtable('media','',array('media_page'=>'Outlets', 'media_section'=>'gallery'))->result();

        $data['current'] = "outlets";
        $view['script']  = $this->load->view('backend/script/outlets','',TRUE);
        $view['content'] = $this->load->view('backend/outlets/v_outlets',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // UPDATE OUTLET CONTENT
    function edit_content()
    {
        $outlets_img = $_FILES['outlets_img']['name'];
        if($outlets_img == '')
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Outlets', 'general_section'=>'outlets_content'))->row();
            if($check == '')
            {
                // INSERT TO DATABASE IF DATA IS NOT SET 
                $add_content = array(
                                    'general_page'=>'Outlets',
                                    'general_section'=>'outlets_content',
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('outlets_content_in'),
                                    'general_content_en' => $this->input->post('outlets_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->inserttable('general',$add_content);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("insert");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
            else
            {
                // UPDATE DATABASE IF DATA IS SET
                $edit_content = array(
                                    'general_title_in' => $this->input->post('sub_title_in'),
                                    'general_title_en' => $this->input->post('sub_title_en'),
                                    'general_content_in' => $this->input->post('outlets_content_in'),
                                    'general_content_en' => $this->input->post('outlets_content_en')
                                );
                $this->db->trans_begin();
                $this->Access->updatetable('general',$edit_content,array('general_page'=>'Outlets', 'general_section'=>'outlets_content'));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
                }
            }
        }
        else
        {
            $check = $this->Access->readtable('general','',array('general_page'=>'Outlets', 'general_section'=>'outlets_content'))->row();
            if($check == '')
            {
                // UPDATE IMAGE + INSERT CONTENT
                $outlets_img = $_FILES['outlets_img']['name'];
                $break = explode('.', $outlets_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'outlet_img_'.$date.'.'.$ext;
                $path = './assets/upload/outlets';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('outlets_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/outlets/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/outlets/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/outlets/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/outlets/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'outlets_image','media_page'=>'Outlets'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/outlets/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/cropped/'.$old_image ));

                            $add_content = array(
                                                'general_page'=>'Outlets',
                                                'general_section'=>'outlets_content',
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('outlets_content_in'),
                                                'general_content_en' => $this->input->post('outlets_content_en')
                                            );

                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'outlets_image','media_page'=>'Outlets'));
                            $this->Access->inserttable('general',$add_content);
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
            else
            {
                // UPDATE IMAGE + UPDATE CONTENT
                $outlets_img = $_FILES['outlets_img']['name'];
                $break = explode('.', $outlets_img);
                $ext = $break[count($break) - 1];
                $date = date('dmYHis');
                $name_url = 'outlet_img_'.$date.'.'.$ext;
                $path = './assets/upload/outlets';

                if( ! file_exists( $path ) )
                {
                    $create = mkdir($path, 0777, TRUE);
                    $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                    $createCrop = mkdir($path.'/cropped', 0777, TRUE);
                    if( ! $create || ! $createThumb || ! $createCrop)
                        return;
                }
                
                $this->piclib->get_config($name_url, $path);
                if( $this->upload->do_upload('outlets_img') )
                {
                    $image = array('upload_data' => $this->upload->data());
                    $source_path = $image['upload_data']['full_path'];
                    $width = $image['upload_data']['image_width'];
                    $height = $image['upload_data']['image_height'];
                    
                    if( $width < 1750 || $height < 1000 )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/outlets/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/outlets/thumbnail/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/outlets/cropped/'.$name_url ));
                        $image_1750px = $this->lang->line('image_1750px');
                        $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1750px.'</div>';
                    }else{
                        $orientation = $this->piclib->orientation($source_path);
                        if( $orientation == 'portrait' )
                        {
                            unlink( realpath( APPPATH.'../assets/upload/outlets/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/thumbnail/'.$name_url ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/cropped/'.$name_url ));
                            $lands_square = $this->lang->line('lands_square');
                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                        }
                        else
                        {
                            $this->piclib->resize_image($source_path, $width, $height, 1750, 1000);
                            if( $this->image_lib->resize() )
                            {
                                $this->image_lib->clear();
                                $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/thumbnail');
                                $this->image_lib->resize();
                                $crop = $this->piclib->resize_image($source_path, $width, $height, 300, 300, $path.'/cropped');
                                $this->piclib->crop_image($crop, 1000, 1000);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                            }
                            $old_image = $this->Access->readtable('media','media_url',array('media_section'=>'outlets_image','media_page'=>'Outlets'))->row()->media_url;       
                            unlink( realpath( APPPATH.'../assets/upload/outlets/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/thumbnail/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/cropped/'.$old_image ));

                            $edit_content = array(
                                                'general_title_in' => $this->input->post('sub_title_in'),
                                                'general_title_en' => $this->input->post('sub_title_en'),
                                                'general_content_in' => $this->input->post('outlets_content_in'),
                                                'general_content_en' => $this->input->post('outlets_content_en')
                                            );

                            $content_img = array(
                                                'media_url' => $name_url
                                            );

                            $this->db->trans_begin();
                            $this->db->set('media_date', 'NOW()', FALSE);
                            $this->Access->updatetable('media',$content_img,array('media_section'=>'outlets_image','media_page'=>'Outlets'));
                            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Outlets', 'general_section'=>'outlets_content'));
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE)
                            {
                                $this->db->trans_rollback();
                            }
                            else {
                                $success = $this->lang->line("update");
                                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                            }
                        }
                    }
                }
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/outlets');
    }

    function add_outlet()
    {
        $content_img = $_FILES['content_img']['name'];
        $break = explode('.', $content_img);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'outlet_'.$date.'.'.$ext;
        $path = './assets/upload/outlets/content';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('content_img') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            if( $width < 1250 || $height < 1000 )
            {
                unlink( realpath( APPPATH.'../assets/upload/outlets/content/'.$name_url ));
                unlink( realpath( APPPATH.'../assets/upload/outlets/content/thumbnail/'.$name_url ));
                $image_1250px = $this->lang->line('image_1250px');
                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1250px.'</div>';
            }else{
                $orientation = $this->piclib->orientation($source_path);
                if( $orientation == 'portrait' )
                {
                    unlink( realpath( APPPATH.'../assets/upload/outlets/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/outlets/content/thumbnail/'.$name_url ));
                    $lands_square = $this->lang->line('lands_square');
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                }
                else
                {
                    $this->piclib->resize_image($source_path, $width, $height, 1250, 1000);
                    if( $this->image_lib->resize() )
                    {
                        $this->image_lib->clear();
                        $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
                        $this->image_lib->resize();
                    }

                    $add_outlet = array(
                                        'outlets_name' => $this->input->post('outlets_name'),
                                        'outlets_phone' => $this->input->post('outlets_phone'),
                                        'outlets_address' => $this->input->post('outlets_address'),
                                        'outlets_city' => $this->input->post('outlets_city'),
                                        'outlets_image' => $name_url
                                        );
                    $this->db->trans_begin();
                    $this->Access->inserttable('outlets',$add_outlet);
                    $this->db->trans_complete();

                    if ($this->db->trans_status() === FALSE)
                    {
                        $this->db->trans_rollback();
                    }
                    else {
                        $success = $this->lang->line("insert");
                        $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                    }
                }
            }
        }
        $_SESSION['outlets_list'] = $notif;
        $this->session->mark_as_flash('outlets_list');
        redirect('backend/outlets#outlets_list');
    }

    function edit_outlet()
    {
        $edit_img = $_FILES['edit_img']['name'];
        if($edit_img=='')
        {
            $edit_outlet = array(
                            'outlets_name' => $this->input->post('outlets_name'),
                            'outlets_phone' => $this->input->post('outlets_phone'),
                            'outlets_address' => $this->input->post('outlets_address'),
                            'outlets_city' => $this->input->post('outlets_city')
                            );
            $this->db->trans_begin();
            $outlets_id = $this->input->post('outlets_id');
            $this->Access->updatetable('outlets',$edit_outlet,array('outlets_id'=>$outlets_id));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        else
        {

            $break = explode('.', $edit_img);
            $ext = $break[count($break) - 1];
            $date = date('dmYHis');
            $name_url = 'outlet_'.$date.'.'.$ext;
            $path = './assets/upload/outlets/content';

            if( ! file_exists( $path ) )
            {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
            }
            $this->piclib->get_config($name_url, $path);
            if( $this->upload->do_upload('edit_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];
                
                if( $width < 1250 || $height < 1000 )
                {
                    unlink( realpath( APPPATH.'../assets/upload/outlets/content/'.$name_url ));
                    unlink( realpath( APPPATH.'../assets/upload/outlets/content/thumbnail/'.$name_url ));
                    $image_1250px = $this->lang->line('image_1250px');
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'.$image_1250px.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' )
                    {
                        unlink( realpath( APPPATH.'../assets/upload/outlets/content/'.$name_url ));
                        unlink( realpath( APPPATH.'../assets/upload/outlets/content/thumbnail/'.$name_url ));
                        $lands_square = $this->lang->line('lands_square');
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
                    }
                    else
                    {
                        $this->piclib->resize_image($source_path, $width, $height, 1250, 1000);
                        if( $this->image_lib->resize() )
                        {
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
                            $this->image_lib->resize();
                        }

                        $outlets_id = $this->input->post('outlets_id');
                        $old_image = $this->Access->readtable('outlets','outlets_image',array('outlets_id'=>$outlets_id))->row()->outlets_image;       
                            unlink( realpath( APPPATH.'../assets/upload/outlets/content/'.$old_image ));
                            unlink( realpath( APPPATH.'../assets/upload/outlets/content/thumbnail/'.$old_image ));

                        $edit_outlet = array(
                                            'outlets_name' => $this->input->post('outlets_name'),
                                            'outlets_phone' => $this->input->post('outlets_phone'),
                                            'outlets_address' => $this->input->post('outlets_address'),
                                            'outlets_city' => $this->input->post('outlets_city'),
                                            'outlets_image' => $name_url
                                            );
                        $this->db->trans_begin();
                        $this->Access->updatetable('outlets',$edit_outlet,array('outlets_id'=>$outlets_id));
                        $this->db->trans_complete();

                        if ($this->db->trans_status() === FALSE)
                        {
                            $this->db->trans_rollback();
                        }
                        else {
                            $success = $this->lang->line("update");
                            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                        }
                    }
                }
            }
        }
        $_SESSION['outlets_list'] = $notif;
        $this->session->mark_as_flash('outlets_list');
        redirect('backend/outlets#outlets_list');
    }

    function delete_outlets($outlets_id)
    {
        $image = $this->Access->readtable('outlets','outlets_image',array('outlets_id'=>$outlets_id))->row()->outlets_image;
        unlink( realpath( APPPATH.'../assets/upload/outlets/content/'.$image ));
        unlink( realpath( APPPATH.'../assets/upload/outlets/content/thumbnail/'.$image ));

        $this->db->trans_begin();
        $this->Access->deletetable('outlets',array('outlets_id'=>$outlets_id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("delete");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
        }
        $_SESSION['outlets_list'] = $notif;
        $this->session->mark_as_flash('outlets_list');
        redirect('backend/outlets#outlets_list');
    }

    function add_gallery()
    {
        $content_gallery_img = $_FILES['content_gallery_img']['name'];
        $break = explode('.', $content_gallery_img);
        $ext = $break[count($break) - 1];
        $date = date('dmYHis');
        $name_url = 'gallery_'.$date.'.'.$ext;
        $path = './assets/upload/gallery';

        if( ! file_exists( $path ) )
        {
            $create = mkdir($path, 0777, TRUE);
            $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
            if( ! $create || ! $createThumb )
                return;
        }
        $this->piclib->get_config($name_url, $path);
        if( $this->upload->do_upload('content_gallery_img') )
        {
            $image = array('upload_data' => $this->upload->data());
            $source_path = $image['upload_data']['full_path'];
            $width = $image['upload_data']['image_width'];
            $height = $image['upload_data']['image_height'];
            
            $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
            $this->image_lib->resize();

            $add_gallery = array(
                                'media_page' => 'Outlets',
                                'media_section' => 'gallery',
                                'media_content_in' => $this->input->post('gallery_caption_in'),
                                'media_content_en' => $this->input->post('gallery_caption_en'),
                                'media_url' => $name_url
                                );
            $this->db->trans_begin();
            $this->db->set('media_date', 'NOW()', FALSE);
            $this->Access->inserttable('media',$add_gallery);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("insert");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        $_SESSION['info_gallery'] = $notif;
        $this->session->mark_as_flash('info_gallery');
        redirect('backend/outlets#gallery');
    }

    function edit_gallery()
    {
        $edit_gallery_img = $_FILES['edit_gallery_img']['name'];
        if($edit_gallery_img=='')
        {
            $edit_gallery = array(
                            'media_content_in' => $this->input->post('gallery_caption_in'),
                            'media_content_en' => $this->input->post('gallery_caption_en')
                            );
            $this->db->trans_begin();
            $media_id = $this->input->post('media_id');
            $this->Access->updatetable('media',$edit_gallery,array('media_id'=>$media_id));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
            }
        }
        else
        {
            $break = explode('.', $edit_gallery_img);
            $ext = $break[count($break) - 1];
            $date = date('dmYHis');
            $name_url = 'gallery_'.$date.'.'.$ext;
            $path = './assets/upload/gallery';

            if( ! file_exists( $path ) )
            {
                $create = mkdir($path, 0777, TRUE);
                $createThumb = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createThumb )
                    return;
            }
            $this->piclib->get_config($name_url, $path);
            if( $this->upload->do_upload('edit_gallery_img') )
            {
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];

                $this->piclib->resize_image($source_path, $width, $height, 250, 200, $path.'/thumbnail');
                $this->image_lib->resize();

                $media_id = $this->input->post('media_id');
                $old_image = $this->Access->readtable('media','media_url',array('media_id'=>$media_id))->row()->media_url;       
                    unlink( realpath( APPPATH.'../assets/upload/gallery/'.$old_image ));
                    unlink( realpath( APPPATH.'../assets/upload/gallery/thumbnail/'.$old_image ));

                $edit_gallery = array(
                                'media_content_in' => $this->input->post('gallery_caption_in'),
                                'media_content_en' => $this->input->post('gallery_caption_en'),
                                'media_url' => $name_url
                                );
                $this->db->trans_begin();
                $this->Access->updatetable('media',$edit_gallery,array('media_id'=>$media_id));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                }
                else {
                    $success = $this->lang->line("update");
                    $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>'; 
                }
            }
        }
        $_SESSION['info_gallery'] = $notif;
        $this->session->mark_as_flash('info_gallery');
        redirect('backend/outlets#gallery');
    }
}