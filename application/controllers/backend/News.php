<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Access'); 
    }
 
    public function save_news() {
        $token = $this->input->post('token_key');
        if($token == "kidsfun2016"){
        	$news_title_in = $this->input->post('news_title_in');
        	$news_title_en = $this->input->post('news_title_en');
        	$news_content_in = $this->input->post('news_content_in');
        	$news_content_en = $this->input->post('news_content_en');
        	$temp_id = $this->input->post('temp_id');

        	$data = array(
        			'news_title_in'=>$news_title_in,
        			'news_title_en'=>$news_title_en,
        			'news_content_in'=>$news_content_in,
        			'news_content_en'=>$news_content_en,
        			'temp_id'=>$temp_id,
        		);
        	$this->db->set('news_date', 'NOW()', FALSE);
        	$this->Access->inserttable('news',$data);
        }
    }

    public function update_news() {
        $token = $this->input->post('token_key');
        if($token == "kidsfun2016"){
        	$news_title_in = $this->input->post('news_title_in');
        	$news_title_en = $this->input->post('news_title_en');
        	$news_content_in = $this->input->post('news_content_in');
        	$news_content_en = $this->input->post('news_content_en');
        	$temp_id = $this->input->post('temp_id');

        	$data = array(
        			'news_title_in'=>$news_title_in,
        			'news_title_en'=>$news_title_en,
        			'news_content_in'=>$news_content_in,
        			'news_content_en'=>$news_content_en,
        		);
        	$this->db->set('news_date', 'NOW()', FALSE);
        	$this->Access->updatetable('news',$data,array('temp_id'=>$temp_id));
        }
    }

    public function delete_news() {
        $token = $this->input->post('token_key');
        if($token == "kidsfun2016"){
        	$temp_id = $this->input->post('temp_id');

        	$this->Access->deletetable('news',array('temp_id'=>$temp_id));
        }
    }
}