<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $be_lang = $this->session->userdata('be_lang');
            if ($be_lang) {
                $this->lang->load('kidsfun_backend',$be_lang);
            } else {
                $this->lang->load('kidsfun_backend','english');
            }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('be_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }

        $data['contact_content'] = $this->Access->readtable('general','',array('general_page'=>'Contact_us', 'general_section'=>'contact_content'))->row();
        $data['message'] = $this->db->query('select * from contact order by contact_date DESC')->result();
        $data['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'Contact_us'))->row();
        $data['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Contact_us'))->row();
        $data['email'] = $this->Access->readtable('general','',array('general_section'=>'email','general_page'=>'Contact_us'))->row();

        $data['current'] = "contact_us";
        $view['script']  = $this->load->view('backend/script/contact_us','',TRUE);
        $view['content'] = $this->load->view('backend/contact_us/v_contact_us',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    // UPDATE CONTACT_US CONTENT 
    function edit_content()
    {
        $check = $this->Access->readtable('general','',array('general_page'=>'Contact_us', 'general_section'=>'contact_content'))->row();

        // INSERT TO DATABASE IF DATA IS NOT SET
        if($check=='')
        {
            $add_content = array(
                                'general_page'=>'Contact_us',
                                'general_section'=>'contact_content',
                                'general_content_in' => $this->input->post('content_in'),
                                'general_content_en' => $this->input->post('content_en')
                            );
            $this->db->trans_begin();
            $this->Access->inserttable('general',$add_content);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        else
        {
            // UPDATE DATABASE IF DATA IS SET
            $edit_content = array(
                                'general_content_in' => $this->input->post('content_in'),
                                'general_content_en' => $this->input->post('content_en')
                            );
            $this->db->trans_begin();
            $this->Access->updatetable('general',$edit_content,array('general_page'=>'Contact_us', 'general_section'=>'contact_content'));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else {
                $success = $this->lang->line("update");
                $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
            }
        }
        $_SESSION['info_content'] = $notif;
        $this->session->mark_as_flash('info_content');
        redirect('backend/contact_us');
    }

    // DETAIL MESSAGE
    function view_message()
    {
        $contact_id = $this->input->post('contact_id');
        $status = array(
                'contact_status' => '1',
            );
        $this->Access->updatetable('contact',$status,array('contact_id'=>$contact_id));

        $data['view_message'] = $this->Access->readtable('contact','',array('contact_id'=>$contact_id))->row();

        $this->load->view('backend/contact_us/v_contact_msg',$data);
    }

    // DELETE MESSAGE
    function delete_message($contact_id)
    {
        $this->db->trans_begin();
        $this->Access->deletetable('contact',array('contact_id'=>$contact_id));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $success    = $this->lang->line("delete");
            $notif      = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_message'] = $notif;
        $this->session->mark_as_flash('info_message');
        redirect('backend/contact_us#message');
    }

    // UPDATE INFORMATION
    function info()
    {
        $phone_info = array(
                'general_content_en' => $this->input->post('phone')
            );
        $address_info = array(
                'general_content_en' => $this->input->post('address')
            );
        $email_info = array(
                'general_content_en' => $this->input->post('email')
            );

        $this->db->trans_begin();
        $this->Access->updatetable('general',$phone_info,array('general_section' => 'phone_number','general_page'=>'Contact_us'));
        $this->Access->updatetable('general',$address_info,array('general_section' => 'address','general_page'=>'Contact_us'));
        $this->Access->updatetable('general',$email_info,array('general_section' => 'email','general_page'=>'Contact_us'));
        $this->db->trans_complete();
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else {
            $success = $this->lang->line("update");
            $notif = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$success.'</div>';
        }
        $_SESSION['info_contact'] = $notif;
        $this->session->mark_as_flash('info_contact');
        redirect('backend/contact_us#info');
    }
}