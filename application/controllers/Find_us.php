<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Find_us extends CI_Controller {

    public function __construct() {
        parent::__construct();
            $this->load->model('Access');       
            $fe_lang = $this->session->userdata('fe_lang');
            if ($fe_lang) {
                $this->lang->load('kidsfun_frontend',$fe_lang);
            } else {
                $this->lang->load('kidsfun_frontend','english');
            }
    }
    
    public function index()
    {
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['find_us_content'] = $this->Access->readtable('general','',array('general_page'=>'Find_us', 'general_section'=>'find_us_content'))->row();
        $data['opening_time'] = $this->Access->readtable('general','',array('general_section'=>'opening_time','general_page'=>'Find_us'))->row();
        $data['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Find_us'))->row();
        $data['find_us_map'] = $this->Access->readtable('media','',array('media_section'=>'map','media_page'=>'Find_us'))->row();

        $view['content'] = $this->load->view('corporate/v_find_us',$data,TRUE);

        //  //LATEST NEWS ON FOOTER
        // $today = date('Y-m-d H:i:s');
        // $view['latest_news'] = $this->db->query('SELECT * FROM news ORDER BY news_date DESC')->result();

        // //NEXT EVENT
        // $check = $this->db->query('SELECT * FROM events WHERE events_date > "'.$today.'"')->result();
        // $view['check'] = count($check);
        // if(count($check) == 0){
        //     $view['next_event'] = $this->db->query('SELECT * FROM events ORDER BY events_date DESC')->result();
        // }else{
        //     $view['next_event'] = $check;
        // }

        //LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        // CONTACT US ON FOOTER
        $view['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'Contact_us'))->row()->general_content_en;
        $view['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Contact_us'))->row()->general_content_en;
        $view['email'] = $this->Access->readtable('general','',array('general_section'=>'email','general_page'=>'Contact_us'))->row()->general_content_en;
        
        $this->load->view('corporate/v_master', $view);
    }
}
