<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partners extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');      
        $fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('kidsfun_frontend',$fe_lang);
        } else {
            $this->lang->load('kidsfun_frontend','english');
        }
    }

    function index()
    {
        $data['lang']    = $this->session->userdata('fe_lang');
        if ($data['lang'] == 'english'||$data['lang']==null){
            $id = 2;   
        }else{
            $id = 1;
        }
        $data['partners_content'] = $this->Access->readtable('general','',array('general_page'=>'Partners', 'general_section'=>'partners_content'))->row();
        $data['partners_img'] = $this->Access->readtable('media','',array('media_page'=>'Partners', 'media_section'=>'partners_image'))->row();
        $data['travel_city'] = $this->db->query("SELECT partners_city FROM partners WHERE partners_category='Travel Agent' GROUP BY partners_city")->result();
        $data['travel_list'] = $this->Access->readtable('partners','',array('partners_category'=>'Travel Agent'))->result();
        $data['hotel_city'] = $this->db->query("SELECT partners_city FROM partners WHERE partners_category='Hotel' GROUP BY partners_city")->result();
        $data['hotel_list'] = $this->Access->readtable('partners','',array('partners_category'=>'Hotel'))->result();

        $view['content']   = $this->load->view('corporate/v_partners',$data,TRUE);

        // LINKS FOLLOW US ON FOOTER
        $view['trip_advisor'] = $this->Access->readtable('general','general_url',array('general_section'=>'trip_advisor', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['facebook'] = $this->Access->readtable('general','general_url',array('general_section'=>'facebook', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['twitter'] = $this->Access->readtable('general','general_url',array('general_section'=>'twitter', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['instagram'] = $this->Access->readtable('general','general_url',array('general_section'=>'instagram', 'general_page'=>'Follow_us'))->row()->general_url;
        $view['youtube'] = $this->Access->readtable('general','general_url',array('general_section'=>'youtube', 'general_page'=>'Follow_us'))->row()->general_url;

        // CONTACT US ON FOOTER
        $view['phone'] = $this->Access->readtable('general','',array('general_section'=>'phone_number','general_page'=>'Contact_us'))->row()->general_content_en;
        $view['address'] = $this->Access->readtable('general','',array('general_section'=>'address','general_page'=>'Contact_us'))->row()->general_content_en;
        $view['email'] = $this->Access->readtable('general','',array('general_section'=>'email','general_page'=>'Contact_us'))->row()->general_content_en;
        
        $this->load->view('corporate/v_master',$view);
    }

    // function cek(){
    //     session_destroy();
    // }
}
